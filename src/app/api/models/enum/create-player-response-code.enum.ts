export enum CreatePlayerResponseCode {
  Success = 'Success',
  WorldNotFound = 'WorldNotFound',
  UserAlreadyHasPlayerOnWorld = 'UserAlreadyHasPlayerOnWorld',
  WorldIsFull = 'WorldIsFull',
}
