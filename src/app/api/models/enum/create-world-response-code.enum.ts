export enum CreateWorldResponseCode {
  Success = 'Success',
  NameAlreadyExists = 'NameAlreadyExists',
}
