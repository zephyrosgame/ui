export enum ResponseCode {
  Success = 'Success',
  NotFound = 'NotFound',
  AccessForbidden = 'AccessForbidden',
}
