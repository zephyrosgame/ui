import {Authority} from './authority';

export class User {
  constructor(public readonly id: string,
              public readonly username: string,
              public readonly email: string,
              public readonly avatar: string | null,
              public readonly authorities: Authority[]) {
  }
}
