export class City {
  id: string;
  name: string;
  capital: boolean;
}
