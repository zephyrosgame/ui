export class ResourceDiscoveryInformation {
  resource: string;
  order: number;
  discovered: boolean;
}
