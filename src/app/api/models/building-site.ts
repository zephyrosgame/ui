import {Building} from './enum';
import {Construction} from '@api/models/construction';

export class BuildingSite {
  id: string;
  building: Building;
  site: number;
  builtAt: Date;
  construction: Construction | null;
}
