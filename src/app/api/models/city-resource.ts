import {Resource} from './resource';

export class CityResource {
  id: string;
  resource: Resource;
  amount: number;
}
