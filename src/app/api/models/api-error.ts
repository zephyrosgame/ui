import {TimeoutError} from 'rxjs';

export class ApiErrorResponse {
  error: ApiError;
  status: number;
}

export class ApiError {
  timestamp: Date;
  errorName: string;
  developerMessage: string;
  data?: any;
}

export type ApiCommunicationErrors = ApiError | TimeoutError;
