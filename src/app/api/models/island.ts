import {IslandForm} from './island-form';

export class Island {
  id: string;
  x: number;
  y: number;
  form: IslandForm;
}
