import {File} from '@api/models/file';

export class WorldMapChunk {
  constructor(public readonly x: number,
              public readonly y: number,
              public readonly image: File) {
  }
}
