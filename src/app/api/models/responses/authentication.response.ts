export class AuthenticationResponse {

  constructor(public readonly username: string,
              public readonly authorities: { authority: string }[],
              public readonly token: string,
              public readonly type: string) {
  }

}
