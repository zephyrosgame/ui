import {CreateWorldResponseCode} from '../enum';
import {World} from '../world';

export interface CreateWorldResponse {
  readonly code: CreateWorldResponseCode;
  readonly world?: World;
}
