import {CreatePlayerResponseCode} from '../enum';
import {Player} from '../player';

export interface CreatePlayerResponse {
  readonly code: CreatePlayerResponseCode;
  readonly player?: Player;
}
