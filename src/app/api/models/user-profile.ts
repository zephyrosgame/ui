export class UserProfile {
  constructor(public readonly id: string,
              public readonly username: string,
              public readonly avatar: string | null) {
  }
}
