export class World {
  id: string;
  name: string;
  playerAmount: number;
  maxPlayers: number;
}
