import {WorldMapChunk} from '@api/models/world-map-chunk';
import {File} from '@api/models/file';

export class WorldMapInfo {
  constructor(public readonly width: number,
              public readonly height: number,
              public readonly chunkWidth: number,
              public readonly chunkHeight: number,
              public readonly miniMap: File,
              public readonly chunks: WorldMapChunk[]) {
  }
}
