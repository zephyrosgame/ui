export interface Extension {
  id: string;
  title: string;
  price: string;
  description: string;
  variation: number;
}
