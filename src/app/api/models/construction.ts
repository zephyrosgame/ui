export interface Construction {
  id: string;
  workers: number;
  workRequired: number;
  timeNeeded: number;
}
