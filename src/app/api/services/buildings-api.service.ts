import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {apiTimeoutToken, apiUrlToken} from '../api-tokens';
import {Observable} from 'rxjs';
import {timeout} from 'rxjs/operators';
import {Building} from '@api/models';

@Injectable({
  providedIn: 'root'
})
export class BuildingsApiService {

  constructor(private http: HttpClient,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  getBuildings(): Observable<Building[]> {
    return this.http.get<Building[]>(`${this.apiUrl}/buildings`).pipe(
      timeout(this.apiTimeout)
    );
  }

}
