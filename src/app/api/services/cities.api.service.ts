import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RandomCityNameResponse, UpdateCityDto} from '@api/models';
import {City} from '../models';
import {Observable} from 'rxjs';
import {apiTimeoutToken, apiUrlToken} from '../api-tokens';
import {timeout} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CitiesApiService {

  constructor(private http: HttpClient,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  getCitiesOfPlayer(playerId: string): Observable<City[]> {
    return this.http.get<City[]>(`${this.apiUrl}/players/${playerId}/cities`).pipe(
      timeout(this.apiTimeout)
    );
  }

  updateCity(id: string, dto: UpdateCityDto): Observable<City> {
    return this.http.put<City>(`${this.apiUrl}/cities/${id}`, dto).pipe(
      timeout(this.apiTimeout)
    );
  }

  getRandomCityName(): Observable<RandomCityNameResponse> {
    return this.http.get<RandomCityNameResponse>(`${this.apiUrl}/random-city-name`).pipe(
      timeout(this.apiTimeout)
    );
  }

}
