import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CreatePlayerDto, Player} from '../models';
import {Observable} from 'rxjs';
import {timeout} from 'rxjs/operators';
import {apiTimeoutToken, apiUrlToken} from '../api-tokens';

@Injectable({
  providedIn: 'root'
})
export class PlayersApiService {

  constructor(private http: HttpClient,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  getPlayerOnWorld(worldId: string): Observable<Player> {
    return this.http.get<Player>(`${this.apiUrl}/worlds/${worldId}/player`).pipe(
      timeout(this.apiTimeout)
    );
  }

  getPlayersOfUser(userId: string): Observable<Player[]> {
    return this.http.get<Player[]>(`${this.apiUrl}/users/${userId}/players`).pipe(
      timeout(this.apiTimeout)
    );
  }

  createPlayer(dto: CreatePlayerDto): Observable<void> {
    return this.http.post<void>(`${this.apiUrl}/players`, dto).pipe(
      timeout(this.apiTimeout)
    );
  }

}
