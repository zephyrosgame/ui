import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CityResource} from '../models';
import {Observable} from 'rxjs';
import {apiTimeoutToken, apiUrlToken} from '../api-tokens';
import {timeout} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CityResourcesApiService {

  constructor(private http: HttpClient,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  getResourcesOfCity(cityId: string): Observable<CityResource[]> {
    return this.http.get<CityResource[]>(`${this.apiUrl}/cities/${cityId}/resources`).pipe(
      timeout(this.apiTimeout)
    );
  }

}
