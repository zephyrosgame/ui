import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NGXLogger} from 'ngx-logger';
import {CreateWorldDto, CreateWorldResponse, Player, World, WorldNameResponse} from '@api/models';
import {Observable} from 'rxjs';
import {tap, timeout} from 'rxjs/operators';
import {apiTimeoutToken, apiUrlToken} from '../api-tokens';

@Injectable({
  providedIn: 'root'
})
export class WorldsApiService {

  constructor(private http: HttpClient,
              private logger: NGXLogger,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  getWorldsToJoin(): Observable<World[]> {
    this.logger.trace('requesting worlds to join');
    return this.http.get<World[]>(`${this.apiUrl}/join-worlds`).pipe(
      timeout(this.apiTimeout),
      tap((worlds: World[]) => this.logger.info('worlds to join retrieved', worlds))
    );
  }

  getWorld(id: string): Observable<World> {
    this.logger.debug(`requesting world with id '${id}'`);
    return this.http.get<World>(`${this.apiUrl}/worlds/${id}`).pipe(
      timeout(this.apiTimeout)
    );
  }

  getWorldOfPlayer(player: Player): Observable<World> {
    return this.http.get<World>(`${this.apiUrl}/players/${player.id}/world`).pipe(
      timeout(this.apiTimeout)
    );
  }

  createWorld(dto: CreateWorldDto): Promise<CreateWorldResponse> {
    return this.http.post<CreateWorldResponse>(`${this.apiUrl}/worlds`, dto).pipe(
      timeout(this.apiTimeout)
    ).toPromise();
  }

  getRandomWorldName(): Promise<WorldNameResponse> {
    return this.http.get<WorldNameResponse>(`${this.apiUrl}/random-world-name`).pipe(
      timeout(this.apiTimeout)
    ).toPromise();
  }

}
