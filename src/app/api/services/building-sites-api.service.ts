import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BuildingSite} from '../models';
import {apiTimeoutToken, apiUrlToken} from '../api-tokens';
import {timeout} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BuildingSitesApiService {

  constructor(private http: HttpClient,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  getBuildingSitesOfCity(cityId: string): Observable<BuildingSite[]> {
    return this.http.get<BuildingSite[]>(`${this.apiUrl}/cities/${cityId}/building-sites`).pipe(
      timeout(this.apiTimeout)
    );
  }

  getBuildingSite(cityId: string, site: number): Observable<BuildingSite> {
    return this.http.get<BuildingSite>(`${this.apiUrl}/cities/${cityId}/building-sites/${site}`).pipe(
      timeout(this.apiTimeout)
    );
  }

  createBuildingSite(cityId: string, site: number, buildingId: string): Observable<void> {
    return this.http.post<void>(`${this.apiUrl}/cities/${cityId}/building-sites/${site}`, {
      buildingId: buildingId
    }).pipe(
      timeout(this.apiTimeout)
    );
  }

}
