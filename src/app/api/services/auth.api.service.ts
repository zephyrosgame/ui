import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LoginDto, User} from '../models';
import {NGXLogger} from 'ngx-logger';
import {AuthenticationResponse} from '@api/models';
import {map, tap, timeout} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {apiTimeoutToken, apiUrlToken} from '../api-tokens';

@Injectable({
  providedIn: 'root'
})
export class AuthApiService {

  constructor(private http: HttpClient,
              private logger: NGXLogger,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  authenticate(dto: LoginDto): Observable<AuthenticationResponse> {
    return this.http.post<AuthenticationResponse>(`${this.apiUrl}/auth/token`, dto).pipe(
      timeout(this.apiTimeout),
      tap((response: AuthenticationResponse) => this.logger.debug('login response', response))
    );
  }

  getAuthenticatedUser(): Observable<User> {
    return this.http.get<User>(`${this.apiUrl}/auth/userinfo`).pipe(
      timeout(this.apiTimeout),
      map((user: User) => {
        const avatarUrl = user.avatar !== null ? new URL(user.avatar, this.apiUrl).href : null;
        return new User(user.id, user.username, user.email, avatarUrl, user.authorities);
      })
    );
  }

}
