import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {apiTimeoutToken, apiUrlToken} from '@api/api-tokens';
import {Observable} from 'rxjs';
import {ResourceDiscoveryInformation} from '@api/models';
import {timeout} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResourcesApiService {

  constructor(private http: HttpClient,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  getDiscoveredResourcesOfPlayer(playerId: string): Observable<ResourceDiscoveryInformation[]> {
    return this.http.get<ResourceDiscoveryInformation[]>(`${this.apiUrl}/players/${playerId}/discovered-resources`).pipe(
      timeout(this.apiTimeout)
    );
  }

}
