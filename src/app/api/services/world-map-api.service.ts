import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {apiTimeoutToken, apiUrlToken} from '@api/api-tokens';
import {File, WorldMapChunk, WorldMapInfo} from '@api/models';
import {map, timeout} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorldMapApiService {

  constructor(private http: HttpClient,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  getMapInfo(worldId: string): Observable<WorldMapInfo> {
    return this.http.get<WorldMapInfo>(`${this.apiUrl}/worlds/${worldId}/map`).pipe(
      timeout(this.apiTimeout),
      map((info: WorldMapInfo) => {
        const miniMapUrl = new URL(info.miniMap.url, this.apiUrl);

        const chunks = info.chunks.map((chunk: WorldMapChunk) => {
          const imageUrl = new URL(chunk.image.url, this.apiUrl);
          return new WorldMapChunk(chunk.x, chunk.y, new File(imageUrl.href));
        });

        return new WorldMapInfo(info.width, info.height, info.chunkWidth, info.chunkHeight, new File(miniMapUrl.href), chunks);
      })
    );
  }

}
