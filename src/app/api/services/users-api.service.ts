import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RegisterDto} from '@api/models';
import {Observable} from 'rxjs';
import {UserProfile} from '../models';
import {apiTimeoutToken, apiUrlToken} from '../api-tokens';
import {map, timeout} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersApiService {

  constructor(private http: HttpClient,
              @Inject(apiUrlToken) private apiUrl: string,
              @Inject(apiTimeoutToken) private apiTimeout: number) {
  }

  createUser(dto: RegisterDto): Observable<void> {
    return this.http.post<void>(`${this.apiUrl}/users`, dto).pipe(
      timeout(this.apiTimeout)
    );
  }

  getUserProfile(id: string): Observable<UserProfile> {
    return this.http.get<UserProfile>(`${this.apiUrl}/users/${id}/profile`).pipe(
      timeout(this.apiTimeout),
      map((profile: UserProfile) => {
        const avatarUrl = profile.avatar !== null ? new URL(profile.avatar, this.apiUrl).href : null;
        return new UserProfile(profile.id, profile.username, avatarUrl);
      })
    );
  }

  uploadAvatar(avatar: File): Observable<void> {
    const formData = new FormData();
    formData.append('avatar', avatar);

    return this.http.post<void>(`${this.apiUrl}/avatars`, formData).pipe(
      timeout(this.apiTimeout)
    );
  }

}
