import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from '../interceptors';
import {apiTimeoutToken, apiUrlToken} from './api-tokens';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ]
})
export class ZephyrosApiModule {

  static forRoot(apiUrl: string, timeout: number): ModuleWithProviders {
    return {
      ngModule: ZephyrosApiModule,
      providers: [
        {provide: apiUrlToken, useValue: apiUrl},
        {provide: apiTimeoutToken, useValue: timeout},
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
      ]
    };
  }

}
