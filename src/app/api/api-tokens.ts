import {InjectionToken} from '@angular/core';

export const apiUrlToken = new InjectionToken<string>('apiUrlToken');

export const apiTimeoutToken = new InjectionToken<string>('apiTimeoutToken');
