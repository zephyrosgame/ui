import {ErrorHandler, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClient} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {AppComponent} from './app.component';
import {ZephyrosApiModule} from '@api/zephyros-api.module';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {DialogModule} from './modules/dialog/dialog.module';
import {CookiesNotificationComponent} from './components/cookies-notification/cookies-notification.component';
import {GlobalErrorHandler} from './handlers/global-error-handler';
import {AppStoreModule} from './store/app-store.module';
import {NumeralModule} from 'ngx-numeral';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {LoadingScreenComponent} from './components/loading-screen/loading-screen.component';
import {WidgetsModule} from './modules/widgets/widgets.module';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LoggerModule.forRoot({level: NgxLoggerLevel.TRACE}),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ZephyrosApiModule.forRoot(environment.apiUrl, 1000),
    AppRoutingModule,
    AppStoreModule,
    DialogModule,
    NumeralModule.forRoot(),
    DragDropModule,
    WidgetsModule
  ],
  declarations: [
    AppComponent,
    CookiesNotificationComponent,
    LoadingScreenComponent
  ],
  providers: [
    {provide: ErrorHandler, useClass: GlobalErrorHandler}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
