import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {RoutingService} from '../services';
import {World, WorldsApiService} from '../api';
import {EMPTY, Observable} from 'rxjs';
import {catchError, first, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WorldResolver implements Resolve<World> {

  constructor(private logger: NGXLogger,
              private worldsApiService: WorldsApiService,
              private route: RoutingService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<World> | Observable<never> {
    const worldId = route.paramMap.get('worldId');

    if (!worldId) {
      this.logger.warn('no world id given');
      this.route.toWorldsPage();
      return EMPTY;
    }

    this.logger.debug(`trying to resolve world with id '${worldId}'`);

    return this.worldsApiService.getWorld(worldId).pipe(
      first(),
      tap((world: World) => this.logger.debug(`world with id '${worldId}' retrieved:`, world)),
      catchError(() => {
        this.route.toWorldsPage();
        return EMPTY;
      })
    );
  }

}
