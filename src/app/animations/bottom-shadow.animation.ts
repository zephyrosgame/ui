import {animate, state, style, transition, trigger} from '@angular/animations';
import {App} from '../models/enum';

export const bottomShadowAnimation = trigger('bottomShadowAnimation', [
  state(App.Ingame, style({opacity: 1})),
  state('*', style({opacity: 0})),
  transition(`${App.Main} => ${App.Ingame}`, [
    style({opacity: 0}),
    animate('200ms 100ms', style({opacity: 1}))
  ]),
  transition(`${App.Ingame} => ${App.Main}`, [
    style({opacity: 1}),
    animate('150ms', style({opacity: 0}))
  ])
]);
