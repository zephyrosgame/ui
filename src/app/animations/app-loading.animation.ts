import {animate, style, transition, trigger} from '@angular/animations';

export const appLoadingAnimation = trigger('appLoadingAnimation', [
  transition(':leave', [
    animate('250ms 250ms', style({opacity: 0}))
  ])
]);
