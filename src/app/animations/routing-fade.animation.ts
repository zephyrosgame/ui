import {animate, group, query, sequence, style, transition, trigger} from '@angular/animations';

export const routingFadeAnimation = trigger('routingFadeAnimation', [
  transition('* => *', [
    query(':enter, :leave', style({position: 'absolute', width: '100%', height: '100%'}), {optional: true}),
    sequence([
      group([
        query(':enter', [
          style({opacity: 0, zIndex: 2}),
          animate('250ms', style({opacity: 1}))
        ], {optional: true}),
        query('router-outlet ~ *', [style({}), animate(1, style({}))], {optional: true})
      ])
    ])
  ])
]);
