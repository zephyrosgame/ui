import {animate, style, transition, trigger} from '@angular/animations';

export const cookiesNotificationAnimation = trigger('cookiesNotificationAnimation', [
  transition(':leave', [
    animate('300ms', style({height: 0}))
  ])
]);
