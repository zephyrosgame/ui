import {animate, group, query, sequence, style, transition, trigger} from '@angular/animations';

export const appRoutingAnimation = trigger('appRoutingAnimation', [
  transition('Main => Ingame', [
    query(':enter, :leave', style({position: 'absolute', width: '100%', height: '100%'})),
    sequence([
      group([
        query(':enter', [
          style({opacity: 0, transform: 'scale(0.8)', zIndex: 2}),
          animate('250ms', style({opacity: 1, transform: 'scale(1)'}))
        ]),
        query('router-outlet ~ *', [style({}), animate(1, style({}))], {optional: true})
      ])
    ])
  ]),
  transition('Ingame => Main', [
    query(':enter, :leave', style({position: 'absolute', width: '100%', height: '100%'}), {optional: true}),
    sequence([
      group([
        query(':leave', [
          style({opacity: 1, transform: 'scale(1)', zIndex: 2}),
          animate('250ms', style({opacity: 0, transform: 'scale(0.8)'}))
        ]),
        query('router-outlet ~ *', [style({}), animate(1, style({}))], {optional: true})
      ])
    ])
  ])
]);
