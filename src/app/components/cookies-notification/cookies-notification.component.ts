import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'zephyros-cookies-notification',
  templateUrl: './cookies-notification.component.html',
  styleUrls: ['./cookies-notification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CookiesNotificationComponent {

  @Output() accept: EventEmitter<void> = new EventEmitter();

}
