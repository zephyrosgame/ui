import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {CookiesNotificationComponent} from './cookies-notification.component';

describe('CookiesNotificationComponent', () => {
  let component: CookiesNotificationComponent;
  let fixture: ComponentFixture<CookiesNotificationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CookiesNotificationComponent],
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations({})
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CookiesNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have a title', () => {
    const element = fixture.debugElement.query(By.css('.cookies-notification__title'));
    expect(element).toBeTruthy();
  });

  it('should have a paragraph', () => {
    const element = fixture.debugElement.query(By.css('.cookies-notification__paragraph'));
    expect(element).toBeTruthy();
  });

  it('should have a link to the privacy policy page', () => {
    const element = fixture.debugElement.query(By.css('a')).nativeElement;
    expect(element.getAttribute('href')).toBe('/policies/cookies-policy');
  });

  it('should have a button to accept', () => {
    const element = fixture.debugElement.query(By.css('.cookies-notification__button'));
    expect(element).toBeTruthy();
  });

  it('should emit accept event once when button is clicked', () => {
    spyOn(component.accept, 'emit');

    const nativeElement = fixture.nativeElement;
    const button = nativeElement.querySelector('.cookies-notification__button');
    button.dispatchEvent(new Event('click'));

    expect(component.accept.emit).toHaveBeenCalledTimes(1);
  });
});
