import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Icon} from '../../modules/widgets/components/icon-widget/icon.enum';

@Component({
  selector: 'zephyros-loading-screen',
  templateUrl: './loading-screen.component.html',
  styleUrls: ['./loading-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingScreenComponent {

  readonly Icon = Icon;

}
