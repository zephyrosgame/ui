import {Injectable} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  private readonly appTitle: string = 'Zephyros';

  constructor(private readonly title: Title) {
  }

  setTitle(pageTitle: string): void {
    if (pageTitle.length > 0) {
      this.title.setTitle(`${pageTitle} - ${this.appTitle}`);
    } else {
      this.title.setTitle(this.appTitle);
    }
  }

}
