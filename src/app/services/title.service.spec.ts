import {TitleService} from './title.service';
import {Title} from '@angular/platform-browser';

const titleMock = {
  setTitle: (newTitle: string) => {}
} as Title;

describe('TitleService', () => {
  let service: TitleService;

  beforeEach(() => {
    service = new TitleService(titleMock);
  });

  it('should use only app title if empty page title is given', () => {
    spyOn(titleMock, 'setTitle');

    service.setTitle('');

    expect(titleMock.setTitle).toHaveBeenCalledWith('Zephyros');
    expect(titleMock.setTitle).toHaveBeenCalledTimes(1);
  });
});
