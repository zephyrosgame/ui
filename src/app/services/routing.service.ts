import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NGXLogger} from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  constructor(private logger: NGXLogger,
              private router: Router) {
  }

  toHomepage() {
    this.logger.debug('redirecting to homepage');
    this.router.navigate(['/']);
  }

  toWorldsPage() {
    this.logger.debug('redirecting to worlds page');
    this.router.navigate(['/worlds']);
  }

  toJoinPage(worldId?: string) {
    this.logger.debug('redirecting to join page');
    this.router.navigate(['join'], {
      queryParams: {
        world: worldId
      }
    });
  }

  toJoinWorldPage(worldId: string) {
    this.logger.debug('redirecting to join world page');
    this.router.navigate(['join-world', worldId]);
  }

  toWorld(worldId: string) {
    this.logger.debug('redirecting to world');
    this.router.navigate(['world', worldId]);
  }

}
