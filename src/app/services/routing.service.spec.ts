import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Location} from '@angular/common';
import {Component, NgZone} from '@angular/core';
import {Router, Routes} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {RoutingService} from './routing.service';
import {NGXLogger} from 'ngx-logger';
import {NGXLoggerMock} from 'ngx-logger/testing';

const routes: Routes = [
  {path: '', component: Component},
  {path: 'worlds', component: Component},
  {path: 'join', component: Component},
  {path: 'join-world/:id', component: Component},
  {path: 'world/:id', component: Component}
];

describe('RoutingService', () => {
  let service: RoutingService;
  let ngZone: NgZone;
  let location: Location;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        Location,
        {provide: NGXLogger, useClass: NGXLoggerMock}
      ]
    });

    service = TestBed.get(RoutingService);
    ngZone = TestBed.get(NgZone);
    location = TestBed.get(Location);
    router = TestBed.get(Router);
    ngZone.run(() => router.initialNavigation());
  });

  it('should route to homepage if toHomepage called', fakeAsync(() => {
    ngZone.run(() => service.toHomepage());
    tick();
    expect(location.path()).toBe('');
  }));

  it('should route to worlds page if toWorldsPage called', fakeAsync(() => {
    ngZone.run(() => service.toWorldsPage());
    tick();
    expect(location.path()).toBe('/worlds');
  }));

  it('should route to join page if toJoinPage called', fakeAsync(() => {
    ngZone.run(() => service.toJoinPage());
    tick();
    expect(location.path()).toBe('/join');
  }));

  it('should route to correct join world page if toJoinWorldPage called', fakeAsync(() => {
    ngZone.run(() => service.toJoinWorldPage('worldId'));
    tick();
    expect(location.path()).toBe('/join-world/worldId');
  }));

  it('should route to correct world if toWorld called', fakeAsync(() => {
    ngZone.run(() => service.toWorld('someWorld'));
    tick();
    expect(location.path()).toBe('/world/someWorld');
  }));
});
