import {Injectable} from '@angular/core';
import {StorageService} from './storage.service';
import {StorageKeys} from '../models/enum/storage-keys.enum';

@Injectable({
  providedIn: 'root'
})
export class CookiesNotificationService {

  constructor(private storageService: StorageService) {
  }

  get hasBeenAccepted(): boolean {
    return this.storageService.getItem(StorageKeys.CookiesAccepted) === 'true';
  }

  accept(): void {
    this.storageService.setItem(StorageKeys.CookiesAccepted, 'true');
  }

}
