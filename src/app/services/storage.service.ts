import {Injectable} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {StorageKeys} from '../models/enum/storage-keys.enum';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private logger: NGXLogger) {
  }

  getItem(key: StorageKeys): string | null {
    this.logger.trace(`get item with key '${key}'`);
    return localStorage.getItem(key);
  }

  setItem(key: StorageKeys, value: string): void {
    this.logger.trace(`set item with key '${key}' to value '${value}'`);
    return localStorage.setItem(key, value);
  }

  removeItem(key: StorageKeys): void {
    this.logger.trace(`remove item with key '${key}'`);
    return localStorage.removeItem(key);
  }

}
