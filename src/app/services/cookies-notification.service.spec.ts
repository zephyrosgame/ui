import {CookiesNotificationService} from './cookies-notification.service';
import {StorageService} from './storage.service';

const storageServiceMock = {
  getItem: (key: string): string | null => null,
  setItem: (key: string, value: string) => {}
};

describe('CookiesNotificationService', () => {
  let service: CookiesNotificationService;

  beforeEach(() => service = new CookiesNotificationService(storageServiceMock as StorageService));

  it('should return not accepted if no value stored in local storage', () => {
    spyOn(storageServiceMock, 'getItem').and.returnValue(null);
    expect(service.hasBeenAccepted).toBeFalsy();
  });

  it(`should return accepted if 'true' stored in local storage`, () => {
    spyOn(storageServiceMock, 'getItem').and.returnValue('true');
    expect(service.hasBeenAccepted).toBeTruthy();
  });

  it(`should store 'true' in local storage if accepted`, () => {
    spyOn(storageServiceMock, 'setItem');
    service.accept();
    expect(storageServiceMock.setItem).toHaveBeenCalledWith('cookiesAccepted', 'true');
  });
});
