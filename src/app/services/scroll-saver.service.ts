import {Injectable} from '@angular/core';
import {NGXLogger} from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class ScrollSaverService {

  private scrollStates: any = {};

  constructor(private logger: NGXLogger) {
  }

  getScrollState(id: string): number | undefined {
    this.logger.debug(`looking up scroll state for id '${id}'`);
    const scrollState = this.scrollStates[id];
    if (scrollState !== undefined) {
      this.logger.debug(`scroll state for id '${id}' is set to '${scrollState}'`);
    } else {
      this.logger.debug(`no scroll state for id '${id}' saved`);
    }
    return scrollState;
  }

  saveScrollState(id: string, scrollState: number) {
    this.logger.trace(`setting scroll state for id '${id}' to '${scrollState}'`);
    this.scrollStates[id] = scrollState;
  }

}
