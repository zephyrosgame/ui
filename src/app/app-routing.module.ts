import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AcpGuard, AuthGuard} from './guards';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/main/main.module').then(m => m.MainModule),
    data: {
      app: 'Main'
    }
  },
  {
    path: 'world/:worldId',
    loadChildren: () => import('./modules/ingame/ingame.module').then(m => m.IngameModule),
    canActivate: [AuthGuard],
    data: {
      app: 'Ingame'
    }
  },
  {
    path: 'acp',
    loadChildren: () => import('./modules/acp/acp.module').then(m => m.AcpModule),
    canActivate: [AcpGuard],
    data: {
      app: 'Acp'
    }
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
