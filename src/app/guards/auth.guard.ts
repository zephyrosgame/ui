import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {Observable} from 'rxjs';
import {first, map, tap} from 'rxjs/operators';
import {RoutingService} from '../services';
import {Store} from '@ngrx/store';
import {AuthState, selectAuth} from '../store/auth';
import {AppState} from '../store/app.state';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private route: RoutingService,
              private logger: NGXLogger,
              private store: Store<AppState>) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    this.logger.trace('checking if authenticated');

    return this.store.select(selectAuth).pipe(
      first((authState: AuthState) => !authState.pending),
      map((authState: AuthState) => authState.user !== null),
      tap((isLoggedIn: boolean) => {
        if (isLoggedIn) {
          this.logger.debug('access allowed, user logged in');
        } else {
          this.logger.warn('access forbidden, user not logged in');
          this.route.toJoinPage(route.url[1].path);
        }
      })
    );
  }

}
