import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {Observable} from 'rxjs';
import {first, map, tap} from 'rxjs/operators';
import {RoutingService} from '../services';
import {UserRole} from '../api';
import {Store} from '@ngrx/store';
import {AuthState, selectAuth} from '../store/auth';
import {AppState} from '../store/app.state';

@Injectable({
  providedIn: 'root'
})
export class AcpGuard implements CanActivate {

  constructor(private route: RoutingService,
              private logger: NGXLogger,
              private store: Store<AppState>) {
  }

  canActivate(): Observable<boolean> {
    this.logger.trace('checking if authorized to access ACP');

    return this.store.select(selectAuth).pipe(
      first((state: AuthState) => !state.pending),
      map((state: AuthState) => {
        return state.user !== null && state.user.authorities.find(authority => authority.authority === UserRole.Admin) !== undefined;
      }),
      tap((canActivate: boolean) => {
        if (canActivate) {
          this.logger.debug('access allowed, user has admin role');
        } else {
          this.logger.warn('access forbidden, user does not have admin role');
          this.route.toHomepage();
        }
      })
    );
  }

}
