import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {Observable} from 'rxjs';
import {first, map, tap} from 'rxjs/operators';
import {RoutingService} from '../services';
import {Store} from '@ngrx/store';
import {AuthState, selectAuth} from '../store/auth';
import {AppState} from '../store/app.state';

@Injectable({
  providedIn: 'root'
})
export class GuestGuard implements CanActivate {

  constructor(private route: RoutingService,
              private logger: NGXLogger,
              private store: Store<AppState>) {
  }

  canActivate(): Observable<boolean> {
    this.logger.trace('checking if unauthenticated');

    return this.store.select(selectAuth).pipe(
      first((state: AuthState) => !state.pending),
      map((state: AuthState) => state.user === null),
      tap((isGuest: boolean) => {
        if (isGuest) {
          this.logger.debug('access allowed, user not logged in');
        } else {
          this.logger.warn('access forbidden, user is authenticated');
          this.route.toWorldsPage();
        }
      })
    );
  }

}
