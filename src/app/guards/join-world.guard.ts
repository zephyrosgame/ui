import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {RoutingService} from '../services';
import {catchError, first, map} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {PlayersApiService} from '@api/services';
import {ApiErrorResponse, Player} from '@api/models';

@Injectable({
  providedIn: 'root'
})
export class JoinWorldGuard implements CanActivate {

  constructor(private logger: NGXLogger,
              private route: RoutingService,
              private playersApiService: PlayersApiService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    const worldId = next.paramMap.get('worldId');

    if (!worldId) {
      this.logger.warn('no world id given');
      this.route.toWorldsPage();
      return false;
    }

    this.logger.debug(`trying to get player of user on world with id '${worldId}'`);

    return this.playersApiService.getPlayerOnWorld(worldId).pipe(
      first(),
      map((player: Player) => {
        this.logger.debug(`access forbidden, user is already player on world with id '${worldId}':`, player);
        this.route.toWorld(worldId);
        return false;
      }),
      catchError((res: ApiErrorResponse) => {
        if (res.error.errorName === 'PlayerNotFound') {
          this.logger.debug(`access allowed, user is not a player on world with id '${worldId}'`);
          return of(true);
        }

        this.logger.debug('some error besides PlayerNotFound occurred');
        this.route.toWorldsPage();
        return of(false);
      })
    );
  }

}
