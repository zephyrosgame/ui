import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {AppComponent} from './app.component';
import {BehaviorSubject} from 'rxjs';
import {DialogManagementService} from './modules/dialog/services/dialog-management.service';
import {LoggerTestingModule} from 'ngx-logger/testing';
import {CookiesNotificationComponent} from './components/cookies-notification/cookies-notification.component';
import {CookiesNotificationService} from './services/cookies-notification.service';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Store} from '@ngrx/store';
import {GetUserAction} from './store/auth';
import {provideMockStore} from '@ngrx/store/testing';
import {AppState} from './store/app.state';
import {MockComponents} from 'ng-mocks';
import {DialogsContainerComponent} from './modules/dialog/components/dialogs-container/dialogs-container.component';
import {DialogBoundsComponent} from './modules/dialog/components/dialog-bounds/dialog-bounds.component';
import {LoadingScreenComponent} from './components/loading-screen/loading-screen.component';

const dialogManagementServiceMock = {
  isDragging$: new BehaviorSubject(false)
};

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  const prepareComponent = (cookiesAccepted: boolean) => {
    TestBed.configureTestingModule({
      declarations: [
        MockComponents(DialogsContainerComponent, DialogBoundsComponent, CookiesNotificationComponent, LoadingScreenComponent),
        AppComponent
      ],
      imports: [
        RouterTestingModule,
        NoopAnimationsModule,
        TranslateTestingModule.withTranslations({}),
        LoggerTestingModule
      ],
      providers: [
        {provide: DialogManagementService, useValue: dialogManagementServiceMock},
        {provide: CookiesNotificationService, useValue: {hasBeenAccepted: cookiesAccepted, accept: () => {}}},
        provideMockStore()
      ]
    }).compileComponents();
  };

  it('should dispatch an action to get to current user', () => {
    prepareComponent(false);

    const store: Store<AppState> = TestBed.get(Store);
    spyOn(store, 'dispatch');

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(store.dispatch).toHaveBeenCalledWith(new GetUserAction());
  });

  it('should show cookies notification if not yet accepted', () => {
    prepareComponent(false);

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('zephyros-cookies-notification'));
    expect(element).toBeTruthy();
  });

  it('should not show cookies notification if already accepted', () => {
    prepareComponent(true);

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('zephyros-cookies-notification'));
    expect(element).toBeFalsy();
  });

  it('should hide cookies notification if it is accepted', () => {
    prepareComponent(false);

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const cookiesNotification = fixture.debugElement.query(By.css('zephyros-cookies-notification'));
    cookiesNotification.componentInstance.accept.emit();
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('zephyros-cookies-notification'));
    expect(element).toBeFalsy();
  });

  it('should disable selection if a dialog is being dragged', () => {
    prepareComponent(false);

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    dialogManagementServiceMock.isDragging$.next(true);
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.app--selection-disabled'));
    expect(element).toBeTruthy();
  });

  it('should enable selection if no dialog is being dragged', () => {
    prepareComponent(false);

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    dialogManagementServiceMock.isDragging$.next(false);
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.app--selection-disabled'));
    expect(element).toBeFalsy();
  });
});
