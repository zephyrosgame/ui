import {Action} from '@ngrx/store';

export enum AppActionTypes {
  SWITCH_TO_MAIN = '[App] Switch To Main',
  SWITCH_TO_INGAME = '[App] Switch To Ingame',
  SWITCH_TO_ACP = '[App] Switch To ACP'
}

export class SwitchToMainAction implements Action {
  readonly type = AppActionTypes.SWITCH_TO_MAIN;
}

export class SwitchToIngameAction implements Action {
  readonly type = AppActionTypes.SWITCH_TO_INGAME;
}

export class SwitchToAcpAction implements Action {
  readonly type = AppActionTypes.SWITCH_TO_ACP;
}
