import {App} from '../../models/enum';
import {appReducer} from './app.reducer';
import {SwitchToAcpAction, SwitchToIngameAction, SwitchToMainAction} from './app.actions';

describe('App Reducer', () => {
  it('should have an initial state', () => {
    const initialState: App = App.Main;

    const state = appReducer(initialState, {type: '@@init'});

    expect(state).toBe(initialState);
  });

  it('should switch to landing', () => {
    const initialState: App = App.Ingame;

    const state = appReducer(initialState, new SwitchToMainAction());

    expect(state).toBe(App.Main);
  });

  it('should switch to ingame', () => {
    const initialState: App = App.Main;

    const state = appReducer(initialState, new SwitchToIngameAction());

    expect(state).toBe(App.Ingame);
  });

  it('should switch to acp', () => {
    const initialState: App = App.Main;

    const state = appReducer(initialState, new SwitchToAcpAction());

    expect(state).toBe(App.Acp);
  });
});
