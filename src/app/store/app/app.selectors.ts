import {createFeatureSelector} from '@ngrx/store';
import {App} from '../../models/enum';

export const selectApp = createFeatureSelector<App>('app');
