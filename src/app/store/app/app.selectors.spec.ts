import {selectApp} from './app.selectors';
import {App} from '../../models/enum';
import {AppState} from '../app.state';

describe('App Selectors', () => {
  it('should return correct state', () => {
    const appState: AppState = {
      app: App.Ingame
    } as AppState;

    expect(selectApp(appState)).toBe(App.Ingame);
  });
});
