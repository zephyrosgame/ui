import {AppActionTypes, SwitchToAcpAction, SwitchToIngameAction, SwitchToMainAction} from './app.actions';

describe('App Actions', () => {
  describe('SwitchToMainAction', () => {
    it('should have correct type', () => {
      const action = new SwitchToMainAction();
      expect(action.type).toEqual(AppActionTypes.SWITCH_TO_MAIN);
    });
  });

  describe('SwitchToIngameAction', () => {
    it('should have correct type', () => {
      const action = new SwitchToIngameAction();
      expect(action.type).toEqual(AppActionTypes.SWITCH_TO_INGAME);
    });
  });

  describe('SwitchToAcpAction', () => {
    it('should have correct type', () => {
      const action = new SwitchToAcpAction();
      expect(action.type).toEqual(AppActionTypes.SWITCH_TO_ACP);
    });
  });
});
