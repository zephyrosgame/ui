import {Action} from '@ngrx/store';
import {AppActionTypes} from './app.actions';
import {App} from '../../models/enum';

const initialApp: App = App.Main;

export function appReducer(app: App = initialApp, action: Action): App {
  switch (action.type) {
    case AppActionTypes.SWITCH_TO_MAIN:
      return App.Main;
    case AppActionTypes.SWITCH_TO_INGAME:
      return App.Ingame;
    case AppActionTypes.SWITCH_TO_ACP:
      return App.Acp;
    default:
      return app;
  }
}
