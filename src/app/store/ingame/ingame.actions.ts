import {Action} from '@ngrx/store';

export enum IngameActionTypes {
  SWITCH_TO_CITY = '[Ingame] Switch To City',
  SWITCH_TO_WORLD = '[Ingame] Switch To World'
}

export class SwitchToCityAction implements Action {
  readonly type = IngameActionTypes.SWITCH_TO_CITY;
}

export class SwitchToWorldAction implements Action {
  readonly type = IngameActionTypes.SWITCH_TO_WORLD;
}
