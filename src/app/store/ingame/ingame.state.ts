import {IngameView} from '../../models/enum';

export interface IngameState {
  view: IngameView;
}
