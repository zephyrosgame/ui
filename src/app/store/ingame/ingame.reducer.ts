import {Action} from '@ngrx/store';
import {IngameView} from '../../models/enum';
import {IngameActionTypes} from './ingame.actions';
import {IngameState} from './ingame.state';

const initialState: IngameState = {
  view: IngameView.City
};

export function ingameReducer(state: IngameState = initialState, action: Action): IngameState {
  switch (action.type) {
    case IngameActionTypes.SWITCH_TO_CITY:
      return {...state, view: IngameView.City};
    case IngameActionTypes.SWITCH_TO_WORLD:
      return {...state, view: IngameView.World};
    default:
      return state;
  }
}
