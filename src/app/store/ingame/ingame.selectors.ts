import {createFeatureSelector, createSelector} from '@ngrx/store';
import {IngameState} from './ingame.state';

export const selectIngame = createFeatureSelector<IngameState>('ingame');

export const selectIngameView = createSelector(selectIngame, (state: IngameState) => state.view);
