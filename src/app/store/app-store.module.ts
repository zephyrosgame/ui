import {NgModule} from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {AuthEffects, authReducer} from './auth';
import {LoginEffects, loginReducer} from './login';
import {RegisterEffects, registerReducer} from './register';
import {appReducer} from './app';
import {ingameReducer} from './ingame';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../../environments/environment';

@NgModule({
  imports: [
    StoreModule.forRoot({
      app: appReducer,
      auth: authReducer,
      ingame: ingameReducer,
      login: loginReducer,
      register: registerReducer
    }),
    EffectsModule.forRoot([
      AuthEffects,
      LoginEffects,
      RegisterEffects
    ]),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  exports: [
    StoreModule,
    EffectsModule
  ]
})
export class AppStoreModule {
}
