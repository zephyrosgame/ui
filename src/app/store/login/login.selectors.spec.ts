import {selectLogin} from './login.selectors';
import {LoginState} from './login.state';
import {AppState} from '../app.state';

describe('Login Selectors', () => {
  it('should return correct state', () => {
    const loginState: LoginState = {
      formDisabled: true,
      waitingForResponse: true,
      success: false,
      error: null
    };

    const appState: AppState = {
      login: loginState
    } as AppState;

    expect(selectLogin(appState)).toBe(loginState);
  });
});
