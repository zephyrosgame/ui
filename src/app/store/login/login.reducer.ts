import {Action} from '@ngrx/store';
import {LoginActionTypes, LoginFailureAction} from './login.actions';
import {LoginState} from './login.state';

const initialState: LoginState = {
  formDisabled: false,
  waitingForResponse: false,
  success: false,
  error: null
};

export function loginReducer(state: LoginState = initialState, action: Action): LoginState {
  switch (action.type) {
    case LoginActionTypes.LOGIN_SUBMIT:
      return {...state, formDisabled: true, waitingForResponse: true, error: null};
    case LoginActionTypes.LOGIN_SUCCESS:
      return {...state, waitingForResponse: false, success: true};
    case LoginActionTypes.LOGIN_FAILURE:
      return {...state, formDisabled: false, waitingForResponse: false, error: (<LoginFailureAction>action).payload};
    default:
      return state;
  }
}
