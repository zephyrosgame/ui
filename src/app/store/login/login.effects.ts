import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {RoutingService, StorageService} from '../../services';
import {Observable, of} from 'rxjs';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {ApiError, ApiErrorResponse, AuthenticationResponse} from '@api/models';
import {LoginActionTypes, LoginFailureAction, LoginSubmitAction, LoginSuccessAction} from './login.actions';
import {GetUserAction} from '../auth';
import {StorageKeys} from '../../models/enum/storage-keys.enum';
import {Action} from '@ngrx/store';
import {NGXLogger} from 'ngx-logger';
import {AuthApiService} from '@api/services';

@Injectable()
export class LoginEffects {

  constructor(private actions: Actions,
              private authApiService: AuthApiService,
              private storageService: StorageService,
              private routingService: RoutingService,
              private logger: NGXLogger) {
  }

  @Effect()
  loginSubmit$: Observable<Action> = this.actions.pipe(
    ofType(LoginActionTypes.LOGIN_SUBMIT),
    switchMap((action: LoginSubmitAction) => this.authApiService.authenticate(action.dto).pipe(
      map((response: AuthenticationResponse) => new LoginSuccessAction(response, action.worldId)),
      catchError((response: ApiErrorResponse) => of(new LoginFailureAction(response.error)))
    ))
  );

  @Effect()
  loginSuccess$: Observable<Action> = this.actions.pipe(
    ofType(LoginActionTypes.LOGIN_SUCCESS),
    tap((action: LoginSuccessAction) => {
      this.storageService.setItem(StorageKeys.AccessToken, action.response.token);
      this.storageService.setItem(StorageKeys.TokenType, action.response.type);

      if (action.worldId !== undefined) {
        this.routingService.toWorld(action.worldId);
      } else {
        this.routingService.toWorldsPage();
      }
    }),
    map(() => new GetUserAction())
  );

  @Effect({dispatch: false})
  loginFailure$: Observable<any> = this.actions.pipe(
    ofType(LoginActionTypes.LOGIN_FAILURE),
    map((action: LoginFailureAction) => action.payload),
    tap((error: ApiError) => this.logger.debug('Login failed with error', error)),
    tap((error: ApiError) => {
      if (error.errorName === 'ValidationFailed') {
        this.logger.warn(
          'Validation of login data failed on backend.',
          'The frontend should prevent invalid data being sent to the backend.'
        );
      }
    })
  );

}
