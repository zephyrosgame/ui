import {Actions, getEffectsMetadata} from '@ngrx/effects';
import {of, throwError} from 'rxjs';
import {LoginEffects} from './login.effects';
import {LoginFailureAction, LoginSubmitAction, LoginSuccessAction} from './login.actions';
import {ApiError, ApiErrorResponse, AuthenticationResponse, LoginDto} from '@api/models';
import {RoutingService, StorageService} from '../../services';
import {StorageKeys} from '../../models/enum/storage-keys.enum';
import {GetUserAction} from '../auth';
import {NGXLogger} from 'ngx-logger';
import {NGXLoggerMock} from 'ngx-logger/testing';
import {AuthApiService} from '@api/services';
import objectContaining = jasmine.objectContaining;

const authApiServiceMock = {
  authenticate: (dto) => {}
} as AuthApiService;

const storageServiceMock = {
  setItem: (key, value) => {}
} as StorageService;

const routingServiceMock = {
  toWorldsPage: () => {}
} as RoutingService;

const loggerMock = new NGXLoggerMock() as NGXLogger;

const response = new AuthenticationResponse('Superzeus9000', [], 'abc-123', 'Bearer');

describe('Login Effects', () => {
  describe('loginSubmit$', () => {
    let effects: LoginEffects;

    beforeEach(() => {
      const loginAction = new LoginSubmitAction({} as LoginDto);
      const actions = new Actions(of(loginAction));
      effects = new LoginEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock, loggerMock);
    });

    it('should dispatch an action', () => {
      const metadata = getEffectsMetadata(effects);

      expect(metadata.loginSubmit$).toEqual(objectContaining({dispatch: true}));
    });

    it('should dispatch a LoginSuccessAction if the backend responds with a success code', (done) => {
      spyOn(authApiServiceMock, 'authenticate').and.returnValue(of(response));

      effects.loginSubmit$.subscribe((action) => {
        expect(action).toEqual(new LoginSuccessAction(response));
        done();
      });
    });

    it('should dispatch a LoginFailureAction if the backend responds with an error', (done) => {
      const error: ApiError = {
        errorName: 'LoremError',
        developerMessage: 'Lorem ipsum test error',
        timestamp: new Date()
      };

      const errorResource: ApiErrorResponse = {
        error: error,
        status: 400
      };

      spyOn(authApiServiceMock, 'authenticate').and.returnValue(throwError(errorResource));

      effects.loginSubmit$.subscribe((action) => {
        expect(action).toEqual(new LoginFailureAction(error));
        done();
      });
    });
  });

  describe('loginSuccess$', () => {
    let effects: LoginEffects;

    beforeEach(() => {
      const loginSuccessAction = new LoginSuccessAction(response);
      const actions = new Actions(of(loginSuccessAction));
      effects = new LoginEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock, loggerMock);
    });

    it('should dispatch an action', () => {
      const metadata = getEffectsMetadata(effects);

      expect(metadata.loginSuccess$).toEqual(objectContaining({dispatch: true}));
    });

    it('should save the token in storage', (done) => {
      spyOn(storageServiceMock, 'setItem');

      effects.loginSuccess$.subscribe(() => {
        expect(storageServiceMock.setItem).toHaveBeenCalledWith(StorageKeys.AccessToken, 'abc-123');
        expect(storageServiceMock.setItem).toHaveBeenCalledWith(StorageKeys.TokenType, 'Bearer');
        done();
      });
    });

    it('should dispatch a GetUserAction', (done) => {
      effects.loginSuccess$.subscribe((action) => {
        if (action instanceof GetUserAction) {
          done();
        }
      });
    });

    it('should route to the worlds page', (done) => {
      spyOn(routingServiceMock, 'toWorldsPage');

      effects.loginSuccess$.subscribe(() => {
        expect(routingServiceMock.toWorldsPage).toHaveBeenCalled();
        done();
      });
    });
  });
});
