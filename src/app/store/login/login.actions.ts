import {Action} from '@ngrx/store';
import {ApiError, AuthenticationResponse, LoginDto} from '@api/models';

export enum LoginActionTypes {
  LOGIN_SUBMIT = '[Login] Submit',
  LOGIN_SUCCESS = '[Login] Success',
  LOGIN_FAILURE = '[Login] Failure'
}

export class LoginSubmitAction implements Action {
  readonly type = LoginActionTypes.LOGIN_SUBMIT;

  constructor(public dto: LoginDto,
              public worldId?: string) {
  }
}

export class LoginSuccessAction implements Action {
  readonly type = LoginActionTypes.LOGIN_SUCCESS;

  constructor(public response: AuthenticationResponse,
              public worldId?: string) {
  }
}

export class LoginFailureAction implements Action {
  readonly type = LoginActionTypes.LOGIN_FAILURE;

  constructor(public payload: ApiError) {
  }
}
