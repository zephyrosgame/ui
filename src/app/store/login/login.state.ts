import {ApiError} from '@api/models';

export interface LoginState {
  formDisabled: boolean;
  waitingForResponse: boolean;
  success: boolean;
  error: ApiError | null;
}
