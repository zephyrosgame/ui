import {LoginFailureAction, LoginSubmitAction, LoginSuccessAction} from './login.actions';
import {ApiError, AuthenticationResponse, LoginDto} from '@api/models';
import {loginReducer} from './login.reducer';
import {LoginState} from './login.state';

const error: ApiError = {
  errorName: 'UsernameNotFound',
  developerMessage: 'Mock error for testing',
  timestamp: new Date()
};

describe('Login Reducer', () => {
  it('should have an initial state', () => {
    const initialState: LoginState = {
      formDisabled: false,
      waitingForResponse: false,
      success: false,
      error: null
    };

    const state = loginReducer(initialState, {type: '@@init'});

    expect(state).toBe(initialState);
  });

  describe('login submit', () => {
    let state: LoginState;

    beforeEach(() => {
      const initialState: LoginState = {
        formDisabled: false,
        waitingForResponse: false,
        success: false,
        error: error
      };

      const dto = new LoginDto('', '');
      state = loginReducer(initialState, new LoginSubmitAction(dto));
    });

    it('should disable the form', () => {
      expect(state.formDisabled).toBeTruthy();
    });

    it('should set waiting for response', () => {
      expect(state.waitingForResponse).toBeTruthy();
    });

    it('should not set success to true', () => {
      expect(state.success).toBeFalsy();
    });

    it('should reset the error', () => {
      expect(state.error).toBeNull();
    });
  });

  describe('login success', () => {
    let state: LoginState;

    beforeEach(() => {
      const initialState: LoginState = {
        formDisabled: true,
        waitingForResponse: true,
        success: false,
        error: null
      };

      const response = new AuthenticationResponse('User', [], '123-456-789', 'Bearer');
      state = loginReducer(initialState, new LoginSuccessAction(response));
    });

    it('should stop waiting for response', () => {
      expect(state.waitingForResponse).toBeFalsy();
    });

    it('should set success to true', () => {
      expect(state.success).toBeTruthy();
    });
  });

  describe('login failure', () => {
    let state: LoginState;

    beforeEach(() => {
      const initialState: LoginState = {
        formDisabled: true,
        waitingForResponse: true,
        success: false,
        error: null
      };

      state = loginReducer(initialState, new LoginFailureAction(error));
    });

    it('should enable the form', () => {
      expect(state.formDisabled).toBeFalsy();
    });

    it('should stop waiting for response', () => {
      expect(state.waitingForResponse).toBeFalsy();
    });

    it('should not set success to true', () => {
      expect(state.success).toBeFalsy();
    });

    it('should set the error', () => {
      expect(state.error).toBe(error);
    });
  });
});
