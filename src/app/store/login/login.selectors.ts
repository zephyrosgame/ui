import {createFeatureSelector} from '@ngrx/store';
import {LoginState} from './login.state';

export const selectLogin = createFeatureSelector<LoginState>('login');
