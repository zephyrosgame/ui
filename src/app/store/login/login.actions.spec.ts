import {ApiError, AuthenticationResponse, LoginDto} from '@api/models';
import {LoginActionTypes, LoginFailureAction, LoginSubmitAction, LoginSuccessAction} from './login.actions';

describe('Login Actions', () => {
  describe('LoginSubmitAction', () => {
    it('should have correct type', () => {
      const action = new LoginSubmitAction({} as LoginDto);
      expect(action.type).toEqual(LoginActionTypes.LOGIN_SUBMIT);
    });
  });

  describe('LoginSuccessAction', () => {
    it('should have correct type', () => {
      const action = new LoginSuccessAction({} as AuthenticationResponse);
      expect(action.type).toEqual(LoginActionTypes.LOGIN_SUCCESS);
    });
  });

  describe('LoginFailureAction', () => {
    it('should have correct type', () => {
      const action = new LoginFailureAction({} as ApiError);
      expect(action.type).toEqual(LoginActionTypes.LOGIN_FAILURE);
    });
  });
});
