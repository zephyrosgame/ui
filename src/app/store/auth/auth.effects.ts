import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {RoutingService, StorageService} from '../../services';
import {Observable, of} from 'rxjs';
import {AuthActionTypes, AuthenticatedAction, NotAuthenticatedAction} from './auth.actions';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {User} from '@api/models';
import {StorageKeys} from '../../models/enum/storage-keys.enum';
import {Action} from '@ngrx/store';
import {AuthApiService} from '@api/services';

@Injectable()
export class AuthEffects {

  constructor(private actions: Actions,
              private authApiService: AuthApiService,
              private storageService: StorageService,
              private routingService: RoutingService) {
  }

  @Effect()
  getUser$: Observable<Action> = this.actions.pipe(
    ofType(AuthActionTypes.GET_USER),
    switchMap(() => {
      if (this.storageService.getItem(StorageKeys.TokenType) !== null && this.storageService.getItem(StorageKeys.AccessToken) !== null) {
        return this.authApiService.getAuthenticatedUser().pipe(
          catchError(() => of(null))
        );
      } else {
        return of(null);
      }
    }),
    map((payload: User | null) => {
      if (payload !== null) {
        return new AuthenticatedAction(payload);
      } else {
        return new NotAuthenticatedAction();
      }
    })
  );

  @Effect({dispatch: false})
  notAuthenticated$: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.NOT_AUTHENTICATED),
    tap(() => {
      this.storageService.removeItem(StorageKeys.AccessToken);
      this.storageService.removeItem(StorageKeys.TokenType);
    })
  );

  @Effect({dispatch: false})
  logout$: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGOUT),
    tap(() => {
      this.storageService.removeItem(StorageKeys.AccessToken);
      this.storageService.removeItem(StorageKeys.TokenType);
      this.routingService.toHomepage();
    })
  );

}
