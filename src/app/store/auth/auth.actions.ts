import {Action} from '@ngrx/store';
import {User} from '@api/models';

export enum AuthActionTypes {
  GET_USER = '[Auth] Get User',
  AUTHENTICATED = '[Auth] Authenticated',
  NOT_AUTHENTICATED = '[Auth] Not Authenticated',
  LOGOUT = '[Auth] Logout'
}

export class GetUserAction implements Action {
  readonly type = AuthActionTypes.GET_USER;
}

export class AuthenticatedAction implements Action {
  readonly type = AuthActionTypes.AUTHENTICATED;

  constructor(public payload: User) {
  }
}

export class NotAuthenticatedAction implements Action {
  readonly type = AuthActionTypes.NOT_AUTHENTICATED;
}

export class LogoutAction implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}
