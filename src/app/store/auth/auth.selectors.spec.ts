import {selectAuth} from './auth.selectors';
import {AuthState} from './auth.state';
import {AppState} from '../app.state';

describe('Auth Selectors', () => {
  it('should return correct state', () => {
    const authState: AuthState = {
      pending: true,
      user: null
    };
    const appState: AppState = {
      auth: authState
    } as AppState;

    expect(selectAuth(appState)).toBe(authState);
  });
});
