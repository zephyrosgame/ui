import {AuthEffects} from './auth.effects';
import {Actions, getEffectsMetadata} from '@ngrx/effects';
import {EMPTY, of, throwError} from 'rxjs';
import {RoutingService, StorageService} from '../../services';
import {AuthenticatedAction, GetUserAction, LogoutAction, NotAuthenticatedAction} from './auth.actions';
import {StorageKeys} from '../../models/enum/storage-keys.enum';
import {User, UserRole} from '@api/models';
import {AuthApiService} from '@api/services';
import objectContaining = jasmine.objectContaining;

const authApiServiceMock = {
  getAuthenticatedUser: () => {}
} as AuthApiService;

const storageServiceMock = {
  getItem: (key) => {},
  removeItem: (key) => {}
} as StorageService;

const routingServiceMock = {
  toHomepage: () => {}
} as RoutingService;

describe('Auth Effects', () => {
  describe('getUser$', () => {
    it('should dispatch an action', () => {
      const actions = new Actions(EMPTY);
      const effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);
      const metadata = getEffectsMetadata(effects);

      expect(metadata.getUser$).toEqual(objectContaining({dispatch: true}));
    });

    it('should not request the authenticated user from the backend if no access token is saved in storage', (done) => {
      spyOn(authApiServiceMock, 'getAuthenticatedUser');

      storageServiceMock.getItem = (key) => {
        return key === StorageKeys.AccessToken ? null : 'someValue';
      };

      const getUserAction = new GetUserAction();
      const actions = new Actions(of(getUserAction));
      const effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);

      effects.getUser$.subscribe(() => {
        expect(authApiServiceMock.getAuthenticatedUser).not.toHaveBeenCalled();
        done();
      });
    });

    it('should not request the authenticated user from the backend if no token type is saved in storage', (done) => {
      spyOn(authApiServiceMock, 'getAuthenticatedUser');

      storageServiceMock.getItem = (key) => {
        return key === StorageKeys.TokenType ? null : 'someValue';
      };

      const getUserAction = new GetUserAction();
      const actions = new Actions(of(getUserAction));
      const effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);

      effects.getUser$.subscribe(() => {
        expect(authApiServiceMock.getAuthenticatedUser).not.toHaveBeenCalled();
        done();
      });
    });

    it('should dispatch an NotAuthenticatedAction if no access token is saved in storage', (done) => {
      storageServiceMock.getItem = (key) => {
        return key === StorageKeys.AccessToken ? null : 'someValue';
      };

      const getUserAction = new GetUserAction();
      const actions = new Actions(of(getUserAction));
      const effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);

      effects.getUser$.subscribe((action) => {
        expect(action).toEqual(new NotAuthenticatedAction());
        done();
      });
    });

    it('should dispatch an NotAuthenticatedAction if no token type is saved in storage', (done) => {
      storageServiceMock.getItem = (key) => {
        return key === StorageKeys.TokenType ? null : 'someValue';
      };

      const getUserAction = new GetUserAction();
      const actions = new Actions(of(getUserAction));
      const effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);

      effects.getUser$.subscribe((action) => {
        expect(action).toEqual(new NotAuthenticatedAction());
        done();
      });
    });

    it('should request the authenticated user from the backend if an access token and a token type is saved in storage', (done) => {
      spyOn(authApiServiceMock, 'getAuthenticatedUser').and.returnValue(throwError({}));

      storageServiceMock.getItem = () => 'someValue';

      const getUserAction = new GetUserAction();
      const actions = new Actions(of(getUserAction));
      const effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);

      effects.getUser$.subscribe(() => {
        expect(authApiServiceMock.getAuthenticatedUser).toHaveBeenCalled();
        done();
      });
    });

    it('should dispatch an NotAuthenticatedAction if the backend responds that the user is not authenticated', (done) => {
      spyOn(authApiServiceMock, 'getAuthenticatedUser').and.returnValue(throwError({}));

      storageServiceMock.getItem = () => 'token';

      const getUserAction = new GetUserAction();
      const actions = new Actions(of(getUserAction));
      const effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);

      effects.getUser$.subscribe((action) => {
        expect(action).toEqual(new NotAuthenticatedAction());
        done();
      });
    });

    it('should dispatch an AuthenticatedAction if the user is authenticated', (done) => {
      const user: User = {
        id: 'userid',
        username: 'Username',
        email: 'mail@user.com',
        avatar: 'avatar.png',
        authorities: [
          {authority: UserRole.User}
        ]
      };

      authApiServiceMock.getAuthenticatedUser = () => of(user);

      storageServiceMock.getItem = () => 'token';

      const getUserAction = new GetUserAction();
      const actions = new Actions(of(getUserAction));
      const effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);

      effects.getUser$.subscribe((action) => {
        expect(action).toEqual(new AuthenticatedAction(user));
        done();
      });
    });
  });

  describe('notAuthenticated$', () => {
    let effects: AuthEffects;

    beforeEach(() => {
      const notAuthenticatedAction = new NotAuthenticatedAction();
      const actions = new Actions(of(notAuthenticatedAction));
      effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);
    });

    it('should not dispatch any action', () => {
      const metadata = getEffectsMetadata(effects);

      expect(metadata.logout$).toEqual(objectContaining({dispatch: false}));
    });

    it('should remove access token from storage', (done) => {
      spyOn(storageServiceMock, 'removeItem');

      effects.notAuthenticated$.subscribe(() => {
        expect(storageServiceMock.removeItem).toHaveBeenCalledWith(StorageKeys.AccessToken);
        done();
      });
    });

    it('should remove token type from storage', (done) => {
      spyOn(storageServiceMock, 'removeItem');

      effects.notAuthenticated$.subscribe(() => {
        expect(storageServiceMock.removeItem).toHaveBeenCalledWith(StorageKeys.TokenType);
        done();
      });
    });
  });

  describe('logout$', () => {
    let effects: AuthEffects;

    beforeEach(() => {
      const logoutAction = new LogoutAction();
      const actions = new Actions(of(logoutAction));
      effects = new AuthEffects(actions, authApiServiceMock, storageServiceMock, routingServiceMock);
    });

    it('should not dispatch any action', () => {
      const metadata = getEffectsMetadata(effects);

      expect(metadata.logout$).toEqual(objectContaining({dispatch: false}));
    });

    it('should remove access token from storage', (done) => {
      spyOn(storageServiceMock, 'removeItem');

      effects.logout$.subscribe(() => {
        expect(storageServiceMock.removeItem).toHaveBeenCalledWith(StorageKeys.AccessToken);
        done();
      });
    });

    it('should remove token type from storage', (done) => {
      spyOn(storageServiceMock, 'removeItem');

      effects.logout$.subscribe(() => {
        expect(storageServiceMock.removeItem).toHaveBeenCalledWith(StorageKeys.TokenType);
        done();
      });
    });

    it('should route to homepage', (done) => {
      spyOn(routingServiceMock, 'toHomepage');

      effects.logout$.subscribe(() => {
        expect(routingServiceMock.toHomepage).toHaveBeenCalled();
        done();
      });
    });
  });
});
