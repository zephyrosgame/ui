import {Action} from '@ngrx/store';
import {AuthActionTypes, AuthenticatedAction} from './auth.actions';
import {AuthState} from './auth.state';

const initialState: AuthState = {
  pending: true,
  user: null
};

export function authReducer(state: AuthState = initialState, action: Action): AuthState {
  switch (action.type) {
    case AuthActionTypes.GET_USER:
      return {...state, pending: true};
    case AuthActionTypes.AUTHENTICATED:
      return {...state, pending: false, user: (action as AuthenticatedAction).payload};
    case AuthActionTypes.NOT_AUTHENTICATED:
    case AuthActionTypes.LOGOUT:
      return {...state, pending: false, user: null};
    default:
      return state;
  }
}
