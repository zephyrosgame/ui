import {AuthenticatedAction, GetUserAction, LogoutAction, NotAuthenticatedAction} from './auth.actions';
import {User, UserRole} from '@api/models';
import {authReducer} from './auth.reducer';
import {AuthState} from './auth.state';

const user: User = {
  id: 'userid',
  username: 'Username',
  email: 'mail@user.com',
  avatar: 'avatar.png',
  authorities: [
    {authority: UserRole.User}
  ]
};

const userUpdate: User = {
  id: 'userid',
  username: 'Updated username',
  email: 'updated.mail@user.com',
  avatar: 'avatar.png',
  authorities: [
    {authority: UserRole.Admin}
  ]
};

describe('Auth Reducer', () => {
  it('should have an initial state', () => {
    const initialState: AuthState = {
      pending: true,
      user: null
    };

    const state = authReducer(initialState, {type: '@@init'});

    expect(state).toBe(initialState);
  });

  describe('get user', () => {
    let state: AuthState;

    describe('when guest', () => {
      beforeEach(() => {
        const initialState: AuthState = {
          pending: false,
          user: null
        };

        const getUser = new GetUserAction();

        state = authReducer(initialState, getUser);
      });

      it('should set pending to true', () => {
        expect(state.pending).toBeTruthy();
      });

      it('should keep user as null', () => {
        expect(state.user).toBeNull();
      });
    });

    describe('when logged in', () => {
      beforeEach(() => {
        const initialState: AuthState = {
          pending: false,
          user: user
        };

        const getUser = new GetUserAction();

        state = authReducer(initialState, getUser);
      });

      it('should set pending to true', () => {
        expect(state.pending).toBeTruthy();
      });

      it('should keep user as is', () => {
        expect(state.user).toBe(user);
      });
    });
  });

  describe('authenticated', () => {
    let state: AuthState;

    describe('when guest', () => {
      beforeEach(() => {
        const initialState: AuthState = {
          pending: true,
          user: null
        };

        const authenticated = new AuthenticatedAction(user);

        state = authReducer(initialState, authenticated);
      });

      it('should set pending to false', () => {
        expect(state.pending).toBeFalsy();
      });

      it('should set the user to the authenticated user', () => {
        expect(state.user).toBe(user);
      });
    });

    describe('when logged in', () => {
      beforeEach(() => {
        const initialState: AuthState = {
          pending: false,
          user: user
        };

        const userRetrieved = new AuthenticatedAction(userUpdate);

        state = authReducer(initialState, userRetrieved);
      });

      it('should set pending to false', () => {
        expect(state.pending).toBeFalsy();
      });

      it('should set the user to new authenticated user', () => {
        expect(state.user).toBe(userUpdate);
      });
    });
  });

  describe('not authenticated', () => {
    let state: AuthState;

    describe('when pending', () => {
      beforeEach(() => {
        const initialState: AuthState = {
          pending: true,
          user: user
        };

        const notAuthenticated = new NotAuthenticatedAction();

        state = authReducer(initialState, notAuthenticated);
      });

      it('should set pending to false', () => {
        expect(state.pending).toBeFalsy();
      });

      it('should keep user as null', () => {
        expect(state.user).toBeNull();
      });
    });

    describe('when logged in', () => {
      beforeEach(() => {
        const initialState: AuthState = {
          pending: true,
          user: user
        };

        const notAuthenticated = new NotAuthenticatedAction();

        state = authReducer(initialState, notAuthenticated);
      });

      it('should set pending to false', () => {
        expect(state.pending).toBeFalsy();
      });

      it('should set the user to null', () => {
        expect(state.user).toBeNull();
      });
    });
  });

  describe('logout', () => {
    let state: AuthState;

    describe('when pending', () => {
      beforeEach(() => {
        const initialState: AuthState = {
          pending: true,
          user: null
        };

        const logout = new LogoutAction();

        state = authReducer(initialState, logout);
      });

      it('should set pending to false', () => {
        expect(state.pending).toBeFalsy();
      });

      it('should keep user as null', () => {
        expect(state.user).toBeNull();
      });
    });

    describe('when logged in', () => {
      beforeEach(() => {
        const initialState: AuthState = {
          pending: true,
          user: user
        };

        const logout = new LogoutAction();

        state = authReducer(initialState, logout);
      });

      it('should set pending to false', () => {
        expect(state.pending).toBeFalsy();
      });

      it('should set the user to null', () => {
        expect(state.user).toBeNull();
      });
    });
  });
});
