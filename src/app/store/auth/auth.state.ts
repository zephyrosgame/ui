import {User} from '@api/models';

export interface AuthState {
  pending: boolean;
  user: User | null;
}
