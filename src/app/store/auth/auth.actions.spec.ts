import {AuthActionTypes, AuthenticatedAction, GetUserAction, LogoutAction, NotAuthenticatedAction} from './auth.actions';
import {User} from '@api/models';

describe('Auth Actions', () => {
  describe('GetUserAction', () => {
    it('should have correct type', () => {
      const action = new GetUserAction();
      expect(action.type).toEqual(AuthActionTypes.GET_USER);
    });
  });

  describe('AuthenticatedAction', () => {
    it('should have correct type', () => {
      const action = new AuthenticatedAction({} as User);
      expect(action.type).toEqual(AuthActionTypes.AUTHENTICATED);
    });
  });

  describe('NotAuthenticatedAction', () => {
    it('should have correct type', () => {
      const action = new NotAuthenticatedAction();
      expect(action.type).toEqual(AuthActionTypes.NOT_AUTHENTICATED);
    });
  });

  describe('LogoutAction', () => {
    it('should have correct type', () => {
      const action = new LogoutAction();
      expect(action.type).toEqual(AuthActionTypes.LOGOUT);
    });
  });
});
