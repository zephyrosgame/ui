import {AuthState} from './auth';
import {LoginState} from './login';
import {RegisterState} from './register';
import {App} from '../models/enum';
import {IngameState} from './ingame';

export interface AppState {
  app: App;
  auth: AuthState;
  ingame: IngameState;
  login: LoginState;
  register: RegisterState;
}
