import {createFeatureSelector} from '@ngrx/store';
import {RegisterState} from './register.state';

export const selectRegister = createFeatureSelector<RegisterState>('register');
