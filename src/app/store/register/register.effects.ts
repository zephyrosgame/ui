import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {RegisterActionTypes, RegisterFailureAction, RegisterSubmitAction, RegisterSuccessAction} from './register.actions';
import {LoginFailureAction, LoginSubmitAction} from '../login';
import {ApiError, ApiErrorResponse, LoginDto} from '@api/models';
import {NGXLogger} from 'ngx-logger';
import {Action} from '@ngrx/store';
import {UsersApiService} from '@api/services';

@Injectable()
export class RegisterEffects {

  constructor(private actions: Actions,
              private usersApiService: UsersApiService,
              private logger: NGXLogger) {
  }

  @Effect()
  registerSubmit$: Observable<Action> = this.actions.pipe(
    ofType(RegisterActionTypes.REGISTER_SUBMIT),
    tap(() => this.logger.trace('Handling register submit action')),
    switchMap((action: RegisterSubmitAction) => this.usersApiService.createUser(action.dto).pipe(
      map(() => new RegisterSuccessAction(action.dto, action.worldId)),
      catchError((res: ApiErrorResponse) => of(new RegisterFailureAction(res.error)))
    ))
  );

  @Effect()
  registerSuccess$: Observable<Action> = this.actions.pipe(
    ofType(RegisterActionTypes.REGISTER_SUCCESS),
    tap(() => this.logger.trace('Handling register success action')),
    map((action: RegisterSubmitAction) => {
      const dto = new LoginDto(action.dto.username, action.dto.password);
      return new LoginSubmitAction(dto, action.worldId);
    })
  );

  @Effect({dispatch: false})
  registerFailure$: Observable<any> = this.actions.pipe(
    ofType(RegisterActionTypes.REGISTER_FAILURE),
    map((action: LoginFailureAction) => action.payload),
    tap((error: ApiError) => this.logger.debug('Registration failed with error', error)),
    tap((error: ApiError) => {
      if (error.errorName === 'ValidationFailed') {
        this.logger.warn(
          'Validation of register data failed on backend.',
          'The frontend should prevent invalid data being sent to the backend.'
        );
      }
    })
  );

}
