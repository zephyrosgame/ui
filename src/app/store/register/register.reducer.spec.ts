import {ApiError, RegisterDto} from '@api/models';
import {registerReducer} from './register.reducer';
import {RegisterFailureAction, RegisterSubmitAction, RegisterSuccessAction} from './register.actions';
import {RegisterState} from './register.state';

const error: ApiError = {
  errorName: 'UsernameTaken',
  developerMessage: 'This is an error for frontend testing.',
  timestamp: new Date()
};

describe('Register Reducer', () => {
  it('should have an initial state', () => {
    const initialState: RegisterState = {
      formDisabled: false,
      waitingForResponse: false,
      success: false,
      error: null
    };

    const state = registerReducer(initialState, {type: '@@init'});

    expect(state).toBe(initialState);
  });

  describe('register submit', () => {
    let state: RegisterState;

    beforeEach(() => {
      const initialState: RegisterState = {
        formDisabled: false,
        waitingForResponse: false,
        success: false,
        error: error
      };

      const dto = new RegisterDto('', '', '');
      state = registerReducer(initialState, new RegisterSubmitAction(dto));
    });

    it('should disable the form', () => {
      expect(state.formDisabled).toBeTruthy();
    });

    it('should set waiting for response', () => {
      expect(state.waitingForResponse).toBeTruthy();
    });

    it('should not set success to true', () => {
      expect(state.success).toBeFalsy();
    });

    it('should reset the error', () => {
      expect(state.error).toBeNull();
    });
  });

  describe('register success', () => {
    let state: RegisterState;

    beforeEach(() => {
      const initialState: RegisterState = {
        formDisabled: true,
        waitingForResponse: true,
        success: false,
        error: null
      };

      const dto = new RegisterDto('', '', '');
      state = registerReducer(initialState, new RegisterSuccessAction(dto));
    });

    it('should stop waiting for response', () => {
      expect(state.waitingForResponse).toBeFalsy();
    });

    it('should set success to true', () => {
      expect(state.success).toBeTruthy();
    });
  });

  describe('register failure', () => {
    let state: RegisterState;

    beforeEach(() => {
      const initialState: RegisterState = {
        formDisabled: true,
        waitingForResponse: false,
        success: false,
        error: null
      };

      state = registerReducer(initialState, new RegisterFailureAction(error));
    });

    it('should enable the form', () => {
      expect(state.formDisabled).toBeFalsy();
    });

    it('should stop waiting for response', () => {
      expect(state.waitingForResponse).toBeFalsy();
    });

    it('should not set success to true', () => {
      expect(state.success).toBeFalsy();
    });

    it('should set the error', () => {
      expect(state.error).toBe(error);
    });
  });
});
