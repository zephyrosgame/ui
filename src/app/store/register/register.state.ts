import {ApiError} from '@api/models';

export interface RegisterState {
  formDisabled: boolean;
  waitingForResponse: boolean;
  success: boolean;
  error: ApiError | null;
}
