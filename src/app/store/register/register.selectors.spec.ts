import {selectRegister} from './register.selectors';
import {RegisterState} from './register.state';
import {AppState} from '../app.state';

describe('Register selectors', () => {
  it('should return correct state', () => {
    const registerState: RegisterState = {
      formDisabled: true,
      waitingForResponse: true,
      success: false,
      error: null
    };

    const appState: AppState = {
      register: registerState
    } as AppState;

    expect(selectRegister(appState)).toBe(registerState);
  });
});
