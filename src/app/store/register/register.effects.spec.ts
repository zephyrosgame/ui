import {Actions, getEffectsMetadata} from '@ngrx/effects';
import {of, throwError} from 'rxjs';
import {ApiError, ApiErrorResponse, LoginDto, RegisterDto} from '@api/models';
import {RegisterEffects} from './register.effects';
import {RegisterFailureAction, RegisterSubmitAction, RegisterSuccessAction} from './register.actions';
import {LoginSubmitAction} from '../login';
import {NGXLogger} from 'ngx-logger';
import {NGXLoggerMock} from 'ngx-logger/testing';
import {UsersApiService} from '@api/services';
import {map} from 'rxjs/operators';
import objectContaining = jasmine.objectContaining;

const usersApiServiceMock = {
  createUser: (dto) => {}
} as UsersApiService;

const loggerMock = new NGXLoggerMock() as NGXLogger;

const registerDto = new RegisterDto('Superzeus9000', 'megagamer@danbos.de', 'secret');

describe('Register Effects', () => {
  describe('registerSubmit$', () => {
    let effects: RegisterEffects;

    beforeEach(() => {
      const registerAction = new RegisterSubmitAction(registerDto);
      const actions = new Actions(of(registerAction));
      effects = new RegisterEffects(actions, usersApiServiceMock, loggerMock);
    });

    it('should dispatch an action', () => {
      const metadata = getEffectsMetadata(effects);

      expect(metadata.registerSubmit$).toEqual(objectContaining({dispatch: true}));
    });

    it('should dispatch a RegisterSuccessAction if the backend responds with a success code', (done) => {
      spyOn(usersApiServiceMock, 'createUser').and.returnValue(of(null).pipe(map(() => {})));

      effects.registerSubmit$.subscribe((action) => {
        expect(action).toEqual(new RegisterSuccessAction(registerDto));
        done();
      });
    });

    it('should dispatch a RegisterFailureAction if the backend responds with an error', (done) => {
      const error: ApiError = {
        errorName: 'LoremError',
        developerMessage: 'Lorem ipsum test error',
        timestamp: new Date()
      };

      const errorResponse: ApiErrorResponse = {
        error: error,
        status: 400
      };

      spyOn(usersApiServiceMock, 'createUser').and.returnValue(throwError(errorResponse));

      effects.registerSubmit$.subscribe((action) => {
        expect(action).toEqual(new RegisterFailureAction(error));
        done();
      });
    });
  });

  describe('registerSuccess$', () => {
    let effects: RegisterEffects;

    beforeEach(() => {
      const registerSuccessAction = new RegisterSuccessAction(registerDto);
      const actions = new Actions(of(registerSuccessAction));
      effects = new RegisterEffects(actions, usersApiServiceMock, loggerMock);
    });

    it('should dispatch an action', () => {
      const metadata = getEffectsMetadata(effects);

      expect(metadata.registerSuccess$).toEqual(objectContaining({dispatch: true}));
    });

    it('should dispatch an AuthenticatedAction', (done) => {
      effects.registerSuccess$.subscribe((action) => {
        const loginDto = new LoginDto(registerDto.username, registerDto.password);
        expect(action).toEqual(new LoginSubmitAction(loginDto));
        done();
      });
    });
  });
});
