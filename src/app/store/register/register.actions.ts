import {Action} from '@ngrx/store';
import {ApiError, RegisterDto} from '@api/models';

export enum RegisterActionTypes {
  REGISTER_SUBMIT = '[Register] Submit',
  REGISTER_SUCCESS = '[Register] Success',
  REGISTER_FAILURE = '[Register] Failure'
}

export class RegisterSubmitAction implements Action {
  readonly type = RegisterActionTypes.REGISTER_SUBMIT;

  constructor(public dto: RegisterDto,
              public worldId?: string) {
  }
}

export class RegisterSuccessAction implements Action {
  readonly type = RegisterActionTypes.REGISTER_SUCCESS;

  constructor(public dto: RegisterDto,
              public worldId?: string) {
  }
}

export class RegisterFailureAction implements Action {
  readonly type = RegisterActionTypes.REGISTER_FAILURE;

  constructor(public payload: ApiError) {
  }
}
