import {Action} from '@ngrx/store';
import {RegisterActionTypes, RegisterFailureAction} from './register.actions';
import {RegisterState} from './register.state';

const initialState: RegisterState = {
  formDisabled: false,
  waitingForResponse: false,
  success: false,
  error: null
};

export function registerReducer(state: RegisterState = initialState, action: Action): RegisterState {
  switch (action.type) {
    case RegisterActionTypes.REGISTER_SUBMIT:
      return {...state, formDisabled: true, waitingForResponse: true, error: null};
    case RegisterActionTypes.REGISTER_SUCCESS:
      return {...state, waitingForResponse: false, success: true};
    case RegisterActionTypes.REGISTER_FAILURE:
      return {...state, formDisabled: false, waitingForResponse: false, error: (<RegisterFailureAction>action).payload};
    default:
      return state;
  }
}
