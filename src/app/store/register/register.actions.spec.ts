import {ApiError, RegisterDto} from '@api/models';
import {RegisterActionTypes, RegisterFailureAction, RegisterSubmitAction, RegisterSuccessAction} from './register.actions';

describe('Register Actions', () => {
  describe('RegisterSubmitAction', () => {
    it('should have correct type', () => {
      const action = new RegisterSubmitAction({} as RegisterDto);
      expect(action.type).toEqual(RegisterActionTypes.REGISTER_SUBMIT);
    });
  });

  describe('RegisterSuccessAction', () => {
    it('should have correct type', () => {
      const action = new RegisterSuccessAction({} as RegisterDto);
      expect(action.type).toEqual(RegisterActionTypes.REGISTER_SUCCESS);
    });
  });

  describe('RegisterFailureAction', () => {
    it('should have correct type', () => {
      const error = new ApiError();
      const action = new RegisterFailureAction(error);
      expect(action.type).toEqual(RegisterActionTypes.REGISTER_FAILURE);
    });
  });
});
