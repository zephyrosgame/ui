import {DialogLayer, DialogRoute} from './models';
import {ExceptionDialogComponent} from './components/dialogs/exception-dialog/exception-dialog.component';

export const dialogs: DialogRoute[] = [
  <DialogRoute>{
    dialogId: 'exception',
    component: ExceptionDialogComponent,
    layer: DialogLayer.Exception
  }
];
