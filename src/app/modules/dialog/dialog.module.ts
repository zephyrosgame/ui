import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {DialogContainerDirective, RequestDialogDirective} from './directives';
import {AngularDraggableModule} from 'angular2-draggable';
import {DialogComponent} from './components/dialogs/dialog/dialog.component';
import {DialogsContainerComponent} from './components/dialogs-container/dialogs-container.component';
import {DialogBoundsComponent} from './components/dialog-bounds/dialog-bounds.component';
import {ExceptionDialogComponent} from './components/dialogs/exception-dialog/exception-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    AngularDraggableModule,
    TranslateModule
  ],
  declarations: [
    DialogComponent,
    DialogsContainerComponent,
    RequestDialogDirective,
    DialogContainerDirective,
    DialogBoundsComponent,
    ExceptionDialogComponent
  ],
  entryComponents: [
    ExceptionDialogComponent
  ],
  exports: [
    DialogsContainerComponent,
    DialogBoundsComponent,
    RequestDialogDirective
  ]
})
export class DialogModule {
}
