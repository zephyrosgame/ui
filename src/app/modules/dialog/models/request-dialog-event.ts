import {DialogRoute} from './dialog-route';
import {DialogData} from './data';

export class RequestDialogEvent {
  readonly route: DialogRoute;
  readonly data: DialogData | undefined;

  constructor(route: DialogRoute, data?: DialogData) {
    this.route = route;
    this.data = data;
  }
}
