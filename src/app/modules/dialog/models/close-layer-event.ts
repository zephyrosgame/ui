import {DialogLayer} from './dialog-layer.enum';

export class CloseLayerEvent {
  readonly layer: DialogLayer;

  constructor(layer: DialogLayer) {
    this.layer = layer;
  }
}
