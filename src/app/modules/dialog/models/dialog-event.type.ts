import {CloseLayerEvent} from './close-layer-event';
import {RequestDialogEvent} from './request-dialog-event';

export type DialogEventType = CloseLayerEvent | RequestDialogEvent;
