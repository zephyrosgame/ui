import {Type} from '@angular/core';
import {DialogLayer} from './dialog-layer.enum';
import {Dialog} from '../dialog';

export interface DialogRoute {
  dialogId: string;
  component: Type<Dialog>;
  layer: DialogLayer;
}
