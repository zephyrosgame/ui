import {Injectable} from '@angular/core';
import {DialogRoute} from '../models';
import {dialogs} from '../dialogs';
import {NGXLogger} from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class DialogRouteService {

  constructor(private logger: NGXLogger) {
  }

  getRouteById(id: string): DialogRoute | undefined {
    const route: DialogRoute | undefined = dialogs.find(d => d.dialogId === id);
    this.logger.debug(`route for dialog id '${id}':`, route);
    return route;
  }

}
