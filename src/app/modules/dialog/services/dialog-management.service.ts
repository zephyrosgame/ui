import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {CloseLayerEvent, DialogEventType, DialogLayer, DialogRoute, RequestDialogEvent} from '../models';
import {NGXLogger} from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class DialogManagementService {

  private events$$: Subject<DialogEventType> = new Subject();
  readonly events$: Observable<DialogEventType> = this.events$$.asObservable();

  private readonly isDragging$$: Subject<boolean> = new BehaviorSubject<boolean>(false);
  readonly isDragging$: Observable<boolean> = this.isDragging$$.asObservable();

  private _dialogBounds: HTMLElement;

  constructor(private logger: NGXLogger) {
  }

  get dialogBounds(): HTMLElement {
    return this._dialogBounds;
  }

  set dialogBounds(dialogBounds: HTMLElement) {
    if (this._dialogBounds) {
      throw new Error('dialog bounds have already been set and should not change during runtime');
    }
    this._dialogBounds = dialogBounds;
  }

  requestDialog(route: DialogRoute, data?: any): void {
    this.logger.debug(`requesting dialog`, route, `with data`, data);
    this.events$$.next(new RequestDialogEvent(route, data));
  }

  closeLayer(layer: DialogLayer): void {
    this.logger.debug(`closing dialog on layer '${layer}'`);
    this.events$$.next(new CloseLayerEvent(layer));
  }

  onDragStart() {
    this.isDragging$$.next(true);
  }

  onDragStopped() {
    this.isDragging$$.next(false);
  }

}
