import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[zephyrosDialogContainer]'
})
export class DialogContainerDirective {

  constructor(public viewContainerRef: ViewContainerRef) {
  }

}
