import {Directive, HostListener, Input} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {DialogRoute} from '../models';
import {DialogRouteService} from '../services/dialog-route.service';
import {DialogManagementService} from '../services/dialog-management.service';

@Directive({
  selector: '[zephyrosRequestDialog]'
})
export class RequestDialogDirective {

  @Input('zephyrosRequestDialog') dialogId: string;

  @Input() dialogData: any;

  @HostListener('click', ['$event'])
  clickEvent(event: MouseEvent) {
    this.logger.debug('click event', event);

    event.preventDefault();
    event.stopPropagation();

    const route: DialogRoute | undefined = this.dialogRouteService.getRouteById(this.dialogId);
    if (route) {
      this.dialogManager.requestDialog(route, this.dialogData);
    } else {
      this.logger.debug(`no dialog route found for id '${this.dialogId}'`);
    }
  }

  constructor(private dialogRouteService: DialogRouteService,
              private dialogManager: DialogManagementService,
              private logger: NGXLogger) {
  }

}
