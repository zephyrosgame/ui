import {Input} from '@angular/core';
import {DialogData, DialogRoute} from './models';
import {DialogManagementService} from './services/dialog-management.service';

export abstract class Dialog {

  @Input() dialog: DialogRoute;

  @Input() data: DialogData | undefined;

  protected constructor(private dialogManager: DialogManagementService) {
  }

  onClose(): void {
    this.dialogManager.closeLayer(this.dialog.layer);
  }

}
