import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {DialogContainerDirective} from '../../directives';
import {DialogManagementService} from '../../services/dialog-management.service';
import {CloseLayerEvent, DialogEventType, DialogLayer, RequestDialogEvent} from '../../models';
import {Dialog} from '../../dialog';
import {takeUntil, tap} from 'rxjs/operators';
import {NGXLogger} from 'ngx-logger';
import {Subject} from 'rxjs';

@Component({
  selector: 'zephyros-dialogs-container',
  templateUrl: './dialogs-container.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogsContainerComponent implements OnInit, OnDestroy {

  @ViewChild(DialogContainerDirective, { static: true }) dialogContainer: DialogContainerDirective;

  private readonly destroyed$ = new Subject();

  private readonly dialogs: Map<DialogLayer, ComponentRef<Dialog>> = new Map();

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private dialogManager: DialogManagementService,
              private logger: NGXLogger,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.dialogManager.events$.pipe(
      takeUntil(this.destroyed$),
      tap((event: DialogEventType) => this.logger.debug('new dialog event', event))
    ).subscribe((event: DialogEventType) => {
      if (event instanceof RequestDialogEvent) {
        this.requestDialog(event);
      }
      if (event instanceof CloseLayerEvent) {
        this.closeLayer(event.layer);
      }
    });
  }

  ngOnDestroy() {
    this.logger.warn('no longer subscribed to dialog events');
    this.destroyed$.next();
  }

  private requestDialog(event: RequestDialogEvent) {
    this.logger.debug('handling dialog request for', event.route, 'with data', event.data);

    const viewContainerRef = this.dialogContainer.viewContainerRef;

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory<Dialog>(event.route.component);

    const dialog = this.dialogs.get(event.route.layer);
    if (dialog !== undefined) {
      this.logger.debug(`another dialog is open on layer '${event.route.layer}'`);
      dialog.destroy();
      this.logger.debug('dialog destroyed');
    }

    this.logger.debug('opening dialog');
    const componentRef: ComponentRef<Dialog> = viewContainerRef.createComponent(componentFactory);
    this.dialogs.set(event.route.layer, componentRef);

    componentRef.instance.dialog = event.route;
    componentRef.instance.data = event.data;

    this.cdr.markForCheck();
    this.logger.debug('dialog opened');
  }

  private closeLayer(layer: DialogLayer) {
    const dialog: ComponentRef<Dialog> | undefined = this.dialogs.get(layer);
    if (dialog !== undefined) {
      dialog.destroy();
      this.dialogs.delete(layer);
      this.logger.debug(`dialog on layer '${layer}' closed`);
    } else {
      this.logger.warn(`no dialog to close on layer '${layer}'`);
    }
  }

}
