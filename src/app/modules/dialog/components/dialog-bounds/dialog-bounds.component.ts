import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';
import {App} from '../../../../models';
import {DialogManagementService} from '../../services/dialog-management.service';
import {Store} from '@ngrx/store';
import {selectApp} from '../../../../store/app/app.selectors';
import {AppState} from '../../../../store/app.state';

@Component({
  selector: 'zephyros-dialog-bounds',
  templateUrl: './dialog-bounds.component.html',
  styleUrls: ['./dialog-bounds.component.scss']
})
export class DialogBoundsComponent implements OnInit {

  @ViewChild('dialogBounds', { static: true }) dialogBounds: ElementRef;

  activeApp$: Observable<App>;

  constructor(private store: Store<AppState>,
              private dialogManager: DialogManagementService) {
  }

  ngOnInit(): void {
    this.dialogManager.dialogBounds = this.dialogBounds.nativeElement;

    this.activeApp$ = this.store.select(selectApp);
  }

}
