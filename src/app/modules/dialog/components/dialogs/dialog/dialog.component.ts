import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DialogManagementService} from '../../../services/dialog-management.service';
import {Observable} from 'rxjs';
import {App} from '../../../../../models';
import {Store} from '@ngrx/store';
import {selectApp} from '../../../../../store/app/app.selectors';
import {AppState} from '../../../../../store/app.state';

@Component({
  selector: 'zephyros-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogComponent implements OnInit {

  @Input() title: string;

  @Input() image = 'default';

  @Output() close: EventEmitter<void> = new EventEmitter();

  dialogBounds: HTMLElement;

  activeApp$: Observable<App>;

  constructor(private dialogManager: DialogManagementService,
              private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.dialogBounds = this.dialogManager.dialogBounds;

    this.activeApp$ = this.store.select(selectApp);
  }

  onStarted(): void {
    this.dialogManager.onDragStart();
  }

  onStopped(): void {
    this.dialogManager.onDragStopped();
  }

}
