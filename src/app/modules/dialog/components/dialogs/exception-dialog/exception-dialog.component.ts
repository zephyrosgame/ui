import {ChangeDetectionStrategy, Component} from '@angular/core';
import {DialogManagementService} from '../../../services/dialog-management.service';
import {Dialog} from '../../../dialog';

@Component({
  selector: 'zephyros-exception-dialog',
  templateUrl: './exception-dialog.component.html',
  styleUrls: ['./exception-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExceptionDialogComponent extends Dialog {

  constructor(dialogManager: DialogManagementService) {
    super(dialogManager);
  }

}
