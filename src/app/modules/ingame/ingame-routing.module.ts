import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IngameScreenComponent} from './components/ingame-screen/ingame-screen.component';
import {PlayerGuard} from './guards/player.guard';
import {WorldResolver} from '../../resolvers';

const routes: Routes = [
  {
    path: '',
    component: IngameScreenComponent,
    canActivate: [PlayerGuard],
    resolve: {
      world: WorldResolver
    },
    children: [
      {
        path: '',
        redirectTo: 'city',
        pathMatch: 'prefix'
      },
      {
        path: 'city',
        loadChildren: () => import('./modules/city/city.module').then(m => m.CityModule),
        data: {
          view: 'city'
        }
      },
      {
        path: 'world',
        loadChildren: () => import('./modules/world/world.module').then(m => m.WorldModule),
        data: {
          view: 'world'
        }
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'city'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IngameRoutingModule {
}
