import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ResourceManagementService} from '../../services/resource-management.service';
import {ResourcesApiService} from '@api/services';
import {Player, ResourceDiscoveryInformation} from '@api/models';
import {Observable} from 'rxjs';
import {PlayerManagementService} from '../../services/player-management.service';
import {map, switchMap} from 'rxjs/operators';

@Component({
  selector: 'zephyros-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResourcesComponent implements OnInit {

  resources$: Observable<ResourceDiscoveryInformation[]>;

  constructor(private resourceManagement: ResourceManagementService,
              private resourcesApiService: ResourcesApiService,
              private playerManagement: PlayerManagementService) {
  }

  ngOnInit(): void {
    this.resourceManagement.fetchCityResources();

    this.resources$ = this.playerManagement.player$.pipe(
      switchMap((player: Player) => this.resourcesApiService.getDiscoveredResourcesOfPlayer(player.id)),
      map((discoveryInformation: ResourceDiscoveryInformation[]) => {
        return discoveryInformation.sort((a, b) => {
          if (a.order < b.order) {
            return -1;
          }
          if (a.order > b.order) {
            return 1;
          }
          return 0;
        });
      })
    );
  }

}
