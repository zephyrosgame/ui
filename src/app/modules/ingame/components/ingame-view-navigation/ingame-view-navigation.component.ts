import {ChangeDetectionStrategy, Component} from '@angular/core';

class View {
  readonly translationKey: string;
  readonly routerLink: string;

  constructor(translationKey: string, routerLink: string) {
    this.translationKey = translationKey;
    this.routerLink = routerLink;
  }
}

@Component({
  selector: 'zephyros-ingame-view-navigation',
  templateUrl: './ingame-view-navigation.component.html',
  styleUrls: ['./ingame-view-navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IngameViewNavigationComponent {

  readonly views: View[] = [
    new View('views.world.title', 'world'),
    new View('views.city.title', 'city'),
  ];

}
