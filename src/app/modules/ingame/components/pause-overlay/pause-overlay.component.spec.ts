import {ComponentFixture, TestBed} from '@angular/core/testing';
import {PauseOverlayComponent} from './pause-overlay.component';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {By} from '@angular/platform-browser';
import {PauseOverlayAction} from '../../models/pause-overlay-action.enum';

const translations = {
  ['de']: {
    'pause-overlay': {
      'title': 'Spielmenü',
      'back-to-game': 'Zurück zum Spiel',
      'leave-game': 'Spiel verlassen'
    }
  }
};

describe('PauseOverlayComponent', () => {
  let component: PauseOverlayComponent;
  let fixture: ComponentFixture<PauseOverlayComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PauseOverlayComponent],
      imports: [
        TranslateTestingModule.withTranslations(translations)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PauseOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have a title', () => {
    const element = fixture.debugElement.query(By.css('.menu__title'));
    expect(element.nativeElement.textContent).toContain('Spielmenü');
  });

  it(`should have display 'ESC' in title`, () => {
    const element = fixture.debugElement.query(By.css('.menu__title'));
    expect(element.nativeElement.textContent).toContain('ESC');
  });

  it(`should have a button to close the pause overlay`, () => {
    spyOn(component.action, 'emit');

    const elements = fixture.debugElement.queryAll(By.css('.buttons__button'));
    const button = elements.find(element => element.nativeElement.textContent === 'Zurück zum Spiel');

    if (button !== undefined) {
      button.nativeElement.click();

      expect(component.action.emit).toHaveBeenCalledTimes(1);
      expect(component.action.emit).toHaveBeenCalledWith(PauseOverlayAction.Unpause);
    } else {
      fail('Button does not exist');
    }
  });

  it(`should have a button to leave the game`, () => {
    spyOn(component.action, 'emit');

    const elements = fixture.debugElement.queryAll(By.css('.buttons__button'));
    const button = elements.find(element => element.nativeElement.textContent === 'Spiel verlassen');

    if (button !== undefined) {
      button.nativeElement.click();

      expect(component.action.emit).toHaveBeenCalledTimes(1);
      expect(component.action.emit).toHaveBeenCalledWith(PauseOverlayAction.LeaveGame);
    } else {
      fail('Button does not exist');
    }
  });
});
