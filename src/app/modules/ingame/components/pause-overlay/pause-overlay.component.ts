import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';
import {PauseOverlayAction} from '../../models/pause-overlay-action.enum';

@Component({
  selector: 'zephyros-pause-overlay',
  templateUrl: './pause-overlay.component.html',
  styleUrls: ['./pause-overlay.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PauseOverlayComponent {

  @Output() action: EventEmitter<PauseOverlayAction> = new EventEmitter();

  readonly PauseOverlayAction = PauseOverlayAction;

}
