import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {City} from '@api/models';
import {Subject} from 'rxjs';
import {sortByCityName} from '../../helpers';
import {takeUntil} from 'rxjs/operators';
import {PlayerCitiesManagementService} from '../../services/player-cities-management.service';

@Component({
  selector: 'zephyros-cities-list',
  templateUrl: './cities-list.component.html',
  styleUrls: ['./cities-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CitiesListComponent implements OnInit, OnDestroy {

  @Input() cities: City[];

  selectedCity: City | null = null;

  isDropdownOpen = false;

  private readonly destroyed$: Subject<void> = new Subject();

  @HostListener('document:click', ['$event'])
  onClickBtn(event: MouseEvent) {
    if (this.isDropdownOpen && !this.elementRef.nativeElement.contains(event.target)) {
      this.isDropdownOpen = false;
    }
  }

  constructor(private elementRef: ElementRef,
              private playerCitiesManagement: PlayerCitiesManagementService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.playerCitiesManagement.selectedCity$.pipe(
      takeUntil(this.destroyed$)
    ).subscribe((selectedCity: City) => {
      this.selectedCity = selectedCity;
      this.cdr.markForCheck();
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  sort(cities: City[]): City[] {
    return cities.sort(sortByCityName);
  }

  selectCity(city: City): void {
    this.playerCitiesManagement.selectCity(city);
    this.isDropdownOpen = false;
    // todo: route to city map
  }

  toggleDropdown(): void {
    this.isDropdownOpen = !this.isDropdownOpen;
  }

}
