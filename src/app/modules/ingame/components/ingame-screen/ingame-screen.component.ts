import {ChangeDetectionStrategy, Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {City} from '../../../../api';
import {RoutingService, TitleService} from '../../../../services';
import {IngameView} from '../../../../models';
import {PauseOverlayAction} from '../../models/pause-overlay-action.enum';
import {pauseMenuAnimation} from '../../animations/pause-menu-animation';
import {routingFadeAnimation} from '../../../../animations/routing-fade.animation';
import {Store} from '@ngrx/store';
import {SwitchToIngameAction} from '../../../../store/app';
import {selectIngameView} from '../../../../store/ingame';
import {AppState} from '../../../../store/app.state';
import {PlayerCitiesManagementService} from '../../services/player-cities-management.service';

@Component({
  selector: 'zephyros-ingame-screen',
  templateUrl: './ingame-screen.component.html',
  styleUrls: ['./ingame-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    routingFadeAnimation,
    pauseMenuAnimation
  ]
})
export class IngameScreenComponent implements OnInit, OnDestroy {

  activeView$: Observable<IngameView>;

  showPauseOverlay = false;

  cities$: Observable<City[]>;

  private readonly destroyed$: Subject<void> = new Subject();

  constructor(private store: Store<AppState>,
              private titleService: TitleService,
              private routingService: RoutingService,
              private playerCitiesManagement: PlayerCitiesManagementService) {
  }

  ngOnInit(): void {
    this.store.dispatch(new SwitchToIngameAction());

    this.activeView$ = this.store.select(selectIngameView);

    this.cities$ = this.playerCitiesManagement.cities$;

    this.titleService.setTitle('');

    this.playerCitiesManagement.selectedCity$.pipe(
      takeUntil(this.destroyed$)
    ).subscribe((city: City) => {
      this.titleService.setTitle(city.name);
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  getRouteView(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['view'];
  }

  handlePauseOverlayAction(action: PauseOverlayAction): void {
    switch (action) {
      case PauseOverlayAction.Unpause:
        return this.unpause();
      case PauseOverlayAction.LeaveGame:
        return this.routingService.toWorldsPage();
    }
  }

  private pause(): void {
    this.showPauseOverlay = true;
  }

  private unpause(): void {
    this.showPauseOverlay = false;
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent): void {
    if (event.key === 'Escape') {
      this.showPauseOverlay ? this.unpause() : this.pause();
    }
  }

}
