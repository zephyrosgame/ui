import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {ResourceManagementService} from '../../services/resource-management.service';
import {ResourceDiscoveryInformation} from '@api/models';

@Component({
  selector: 'zephyros-resources-item',
  templateUrl: './resources-item.component.html',
  styleUrls: ['./resources-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResourcesItemComponent implements OnInit {

  @Input() info: ResourceDiscoveryInformation;

  amount = 0;

  constructor(private resourceManagement: ResourceManagementService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.resourceManagement.getAmountOfResource(this.info.resource).subscribe((amount: number) => {
      this.amount = amount;
      this.cdr.markForCheck();
    });
  }

}
