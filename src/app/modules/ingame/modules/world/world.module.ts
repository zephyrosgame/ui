import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {WidgetsModule} from '../../../widgets/widgets.module';
import {WorldScreenComponent} from './components/world-screen/world-screen.component';
import {WorldMapComponent} from './components/world-map/world-map.component';
import {CityComponent} from './components/city/city.component';
import {WorldRoutingModule} from './world-routing.module';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {WorldMapService} from './services/world-map.service';
import {WorldMiniMapComponent} from './components/world-mini-map/world-mini-map.component';

@NgModule({
  imports: [
    CommonModule,
    WorldRoutingModule,
    TranslateModule,
    WidgetsModule,
    DragDropModule
  ],
  declarations: [
    WorldScreenComponent,
    WorldMapComponent,
    CityComponent,
    WorldMiniMapComponent
  ],
  providers: [
    WorldMapService
  ]
})
export class WorldModule {
}
