import {Injectable} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {WorldMapViewport} from '../models/world-map-viewport';
import {WorldMapInfo} from '@api/models';

@Injectable()
export class WorldMapService {

  private viewport: WorldMapViewport = new WorldMapViewport(0, 0, 0, 0);
  private readonly viewport$$: Subject<WorldMapViewport> = new ReplaySubject(1);
  readonly viewport$: Observable<WorldMapViewport> = this.viewport$$.asObservable();

  updateViewportSize(mapInfo: WorldMapInfo, width: number, height: number): void {
    const maxX = mapInfo.width - width;
    const x = Math.min(this.viewport.x, maxX);

    const maxY = mapInfo.height - height;
    const y = Math.min(this.viewport.y, maxY);

    this.viewport = new WorldMapViewport(x, y, width, height);
    this.viewport$$.next(this.viewport);
  }

  updateViewportPosition(mapInfo: WorldMapInfo, preferredX: number, preferredY: number): void {
    const maxX = mapInfo.width - this.viewport.width;
    const x = Math.min(Math.max(preferredX, 0), maxX);

    const maxY = mapInfo.height - this.viewport.height;
    const y = Math.min(Math.max(preferredY, 0), maxY);

    this.viewport = new WorldMapViewport(x, y, this.viewport.width, this.viewport.height);
    this.viewport$$.next(this.viewport);
  }

}
