export class WorldMapViewport {
  constructor(public readonly x: number,
              public readonly y: number,
              public readonly width: number,
              public readonly height: number) {
  }
}
