import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import {SwitchToWorldAction} from '../../../../../../store/ingame';
import {AppState} from '../../../../../../store/app.state';
import {ActivatedRoute} from '@angular/router';
import {World, WorldMapInfo} from '@api/models';
import {WorldMapService} from '../../services/world-map.service';
import {WorldMapApiService} from '@api/services/world-map-api.service';

@Component({
  selector: 'zephyros-world-screen',
  templateUrl: './world-screen.component.html',
  styleUrls: ['./world-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorldScreenComponent implements OnInit {

  @ViewChild('map', {static: true}) map: ElementRef<HTMLDivElement>;

  mapInfo: WorldMapInfo;

  constructor(private store: Store<AppState>,
              private route: ActivatedRoute,
              private worldMapService: WorldMapService,
              private worldMapApiService: WorldMapApiService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.store.dispatch(new SwitchToWorldAction());

    const world: World = this.route.snapshot.parent!.parent!.data.world;
    this.worldMapApiService.getMapInfo(world.id).subscribe((info: WorldMapInfo) => {
      this.mapInfo = info;
      this.cdr.markForCheck();

      this.updateViewportSize();
    });
  }

  @HostListener('window:resize')
  onResize(): void {
    if (this.mapInfo !== undefined) {
      this.updateViewportSize();
    }
  }

  private updateViewportSize(): void {
    const mapWidth = this.map.nativeElement.clientWidth;
    const mapHeight = this.map.nativeElement.clientHeight;
    this.worldMapService.updateViewportSize(this.mapInfo, mapWidth, mapHeight);
  }

}
