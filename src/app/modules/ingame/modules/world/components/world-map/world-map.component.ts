import {ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {WorldMapChunk, WorldMapInfo} from '@api/models';
import {Application, Loader, LoaderResource, Sprite} from 'pixi.js';
import {WorldMapService} from '../../services/world-map.service';
import {WorldMapViewport} from '../../models/world-map-viewport';
import {NGXLogger} from 'ngx-logger';

@Component({
  selector: 'zephyros-world-map',
  templateUrl: './world-map.component.html',
  styleUrls: ['./world-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorldMapComponent implements OnInit {

  @Input() mapInfo: WorldMapInfo;

  @ViewChild('worldMap', {static: true}) worldMap: ElementRef<HTMLDivElement>;

  private app: Application;

  isDragging = false;
  private canDrag = false;

  private parentResource: LoaderResource | undefined;
  private requestedResources: string[] = [];

  private offsetX: number;
  private offsetY: number;
  private previousX: number;
  private previousY: number;

  constructor(private worldMapService: WorldMapService,
              private logger: NGXLogger) {
  }

  ngOnInit(): void {
    this.app = new Application({
      resizeTo: this.worldMap.nativeElement,
      antialias: true
    });

    this.worldMap.nativeElement.appendChild(this.app.view);

    this.worldMapService.viewport$.subscribe((viewport: WorldMapViewport) => {
      this.canDrag = true;

      this.offsetX = viewport.x;
      this.offsetY = viewport.y;

      this.paintViewport(viewport);
    });
  }

  private paintViewport(viewport: WorldMapViewport): void {
    const startX = Math.floor(viewport.x / this.mapInfo.chunkWidth);
    const startY = Math.floor(viewport.y / this.mapInfo.chunkHeight);

    const endX = Math.ceil((viewport.x + viewport.width) / this.mapInfo.chunkWidth);
    const endY = Math.ceil((viewport.y + viewport.height) / this.mapInfo.chunkHeight);

    this.loadChunks(startX, startY, endX, endY);

    this.app.loader.load((loader: Loader, resources: Partial<Record<string, LoaderResource>>) => {
      if (!this.parentResource) {
        // Set a loaded resource as parent resource for subsequently loaded resources to prevent a Pixi.js error
        const resourceKey = Object.keys(resources)[0];
        this.parentResource = resources[resourceKey];
      }

      this.removeAllChildrenFromStage();

      this.paintChunks(resources, viewport, startX, startY, endX, endY);
    });
  }

  private loadChunks(startX: number, startY: number, endX: number, endY: number): void {
    for (let x = startX; x < endX; ++x) {
      for (let y = startY; y < endY; ++y) {
        const chunkResourceName = `chunk_${x}_${y}`;

        if (!this.requestedResources.includes(chunkResourceName)) {
          const chunk = this.mapInfo.chunks.find((c: WorldMapChunk) => c.x === x && c.y === y);

          if (chunk !== undefined) {
            this.app.loader.add(chunkResourceName, chunk.image.url, {
              parentResource: this.parentResource
            });

            this.requestedResources.push(chunkResourceName);
          } else {
            this.logger.error(`Missing chunk information for chunk [${x}, ${y}]`);
          }
        }
      }
    }
  }

  private paintChunks(resources: Partial<Record<string, LoaderResource>>, viewport: WorldMapViewport, startX: number, startY: number,
                      endX: number, endY: number): void {
    for (let x = startX; x < endX; ++x) {
      for (let y = startY; y < endY; ++y) {
        const chunkResource: LoaderResource | undefined = resources[`chunk_${x}_${y}`];

        if (chunkResource !== undefined) {
          const tile = new Sprite(chunkResource.texture);
          tile.x = x * this.mapInfo.chunkWidth - viewport.x;
          tile.y = y * this.mapInfo.chunkHeight - viewport.y;
          this.app.stage.addChild(tile);
        } else {
          this.logger.error(`Failed to load texture of chunk [${x}, ${y}]`);
        }
      }
    }
  }

  private removeAllChildrenFromStage(): void {
    for (let i = this.app.stage.children.length - 1; i >= 0; --i) {
      this.app.stage.removeChild(this.app.stage.children[i]);
    }
  }

  onDragStart(event: MouseEvent): void {
    if (this.canDrag) {
      this.isDragging = true;
      this.previousX = event.x;
      this.previousY = event.y;
    }
  }

  onDragMove(event: MouseEvent): void {
    if (this.isDragging) {
      this.offsetX += this.previousX - event.x;
      this.offsetY += this.previousY - event.y;

      this.previousX = event.x;
      this.previousY = event.y;

      this.worldMapService.updateViewportPosition(this.mapInfo, this.offsetX, this.offsetY);
    }
  }

  onDragEnd(): void {
    this.isDragging = false;
  }

}
