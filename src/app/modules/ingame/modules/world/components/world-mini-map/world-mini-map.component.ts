import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {WorldMapService} from '../../services/world-map.service';
import {WorldMapViewport} from '../../models/world-map-viewport';
import {WorldMapInfo} from '@api/models';

@Component({
  selector: 'zephyros-world-mini-map',
  templateUrl: './world-mini-map.component.html',
  styleUrls: ['./world-mini-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorldMiniMapComponent implements OnInit {

  @Input() mapInfo: WorldMapInfo;

  @ViewChild('miniMap', {static: true}) miniMap: ElementRef<HTMLDivElement>;

  @ViewChild('miniMapHighlight', {static: true}) miniMapHighlight: ElementRef<HTMLDivElement>;

  miniMapHighlightStyle: { [attribute: string]: string };

  isDragging = false;

  constructor(private worldMapService: WorldMapService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.worldMapService.viewport$.subscribe((viewport: WorldMapViewport) => {
      const highlightWidth = viewport.width / this.mapInfo.width * this.miniMapWidth;
      const highlightHeight = viewport.height / this.mapInfo.height * this.miniMapHeight;

      const highlightX = viewport.x / this.mapInfo.width * this.miniMapWidth;
      const highlightY = viewport.y / this.mapInfo.height * this.miniMapHeight;

      this.miniMapHighlightStyle = {
        width: `${highlightWidth}px`,
        height: `${highlightHeight}px`,
        left: `${highlightX}px`,
        top: `${highlightY}px`
      };

      this.cdr.markForCheck();
    });
  }

  startDragging(event: MouseEvent): void {
    this.isDragging = true;
    this.changeViewportPosition(event);
  }

  drag(event: MouseEvent): void {
    if (this.isDragging) {
      this.changeViewportPosition(event);
    }
  }

  stopDragging(): void {
    this.isDragging = false;
  }

  private changeViewportPosition(event: MouseEvent): void {
    const miniMapBounds = this.miniMap.nativeElement.getBoundingClientRect();
    const miniMapX = event.x - miniMapBounds.left;
    const miniMapY = event.y - miniMapBounds.top;

    const x = (miniMapX - this.miniMapHighlightWidth / 2) / this.miniMapWidth * this.mapInfo.width;
    const y = (miniMapY - this.miniMapHighlightHeight / 2) / this.miniMapHeight * this.mapInfo.height;

    this.worldMapService.updateViewportPosition(this.mapInfo, x, y);
  }

  private get miniMapWidth(): number {
    return this.miniMap.nativeElement.clientWidth;
  }

  get miniMapHeight(): number {
    return this.miniMapWidth / this.mapInfo.width * this.mapInfo.height;
  }

  private get miniMapHighlightWidth(): number {
    return this.miniMapHighlight.nativeElement.clientWidth;
  }

  private get miniMapHighlightHeight(): number {
    return this.miniMapHighlight.nativeElement.clientHeight;
  }

}
