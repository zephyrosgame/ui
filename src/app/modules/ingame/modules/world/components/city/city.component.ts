import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {City} from '@api/models';

@Component({
  selector: 'zephyros-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CityComponent {

  @Input() city: City;

  get tooltipTranslationKey(): string {
    if (this.city.capital) {
      return 'world-map.city.tooltip.capital';
    } else {
      return 'world-map.city.tooltip.default';
    }
  }

}
