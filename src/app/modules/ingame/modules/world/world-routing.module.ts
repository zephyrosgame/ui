import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WorldScreenComponent} from './components/world-screen/world-screen.component';

const routes: Routes = [
  {
    path: '',
    component: WorldScreenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorldRoutingModule {
}
