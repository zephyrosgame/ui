import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {BuildingSite, City} from '../../../../../../api';
import {MatDialog} from '@angular/material';
import {ConstructBuildingDialogComponent} from '../construct-building-dialog/construct-building-dialog.component';
import {ConstructBuildingDialogData} from '../construct-building-dialog/construct-building-dialog-data';

@Component({
  selector: 'zephyros-city-map',
  templateUrl: './city-map.component.html',
  styleUrls: ['./city-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CityMapComponent {

  @Input() city: City | null;

  @Input() buildingSites: BuildingSite[] | null;

  private readonly sites = 14;

  constructor(private dialog: MatDialog) {
  }

  get emptySites(): number[] {
    if (this.buildingSites !== null) {
      const sitesWithBuildings = this.buildingSites.map((buildingSite: BuildingSite) => buildingSite.site);
      return Array.from({length: this.sites}, (x, i) => i + 1).filter(site => !sitesWithBuildings.includes(site));
    } else {
      return [];
    }
  }

  hasWall(): boolean {
    return this.buildingSites !== null &&
      this.buildingSites.find((buildingSite: BuildingSite) => buildingSite.building.id === 'wall') !== undefined;
  }

  constructBuilding(site: number): void {
    this.dialog.open(ConstructBuildingDialogComponent, {
      data: new ConstructBuildingDialogData(this.city!, site)
    });
  }

}
