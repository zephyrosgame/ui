import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {City} from '@api/models';
import {Store} from '@ngrx/store';
import {SwitchToCityAction} from '../../../../../../store/ingame';
import {AppState} from '../../../../../../store/app.state';
import {PlayerCitiesManagementService} from '../../../../services/player-cities-management.service';

@Component({
  selector: 'zephyros-city-screen',
  templateUrl: './city-screen.component.html',
  styleUrls: ['./city-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CityScreenComponent implements OnInit {

  city$: Observable<City>;

  constructor(private store: Store<AppState>,
              private playerCitiesManagement: PlayerCitiesManagementService) {
  }

  ngOnInit(): void {
    this.store.dispatch(new SwitchToCityAction());

    this.city$ = this.playerCitiesManagement.selectedCity$;
  }

}
