import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'zephyros-prosperity-bar',
  templateUrl: './prosperity-bar.component.html',
  styleUrls: ['./prosperity-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProsperityBarComponent {

  @Input() level: number;

  @Input() currentProsperity: number;

  @Input() requiredProsperity: number;

  get progress(): number {
    return this.currentProsperity / this.requiredProsperity;
  }

}
