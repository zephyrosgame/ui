import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {BuildingSite, City} from '@api/models';
import {PlayerCitiesManagementService} from '../../../../services/player-cities-management.service';
import {BuildingSitesManagementService} from '../../../../services/building-sites-management.service';

@Component({
  selector: 'zephyros-selected-city-map',
  templateUrl: './selected-city-map.component.html',
  styleUrls: ['./selected-city-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectedCityMapComponent implements OnInit {

  selectedCity$: Observable<City>;

  buildingSites$: Observable<BuildingSite[]>;

  constructor(private playerCitiesManagement: PlayerCitiesManagementService,
              private buildingSitesManagement: BuildingSitesManagementService) {
  }

  ngOnInit(): void {
    this.selectedCity$ = this.playerCitiesManagement.selectedCity$;
    this.buildingSites$ = this.buildingSitesManagement.buildingSites$;
  }

}
