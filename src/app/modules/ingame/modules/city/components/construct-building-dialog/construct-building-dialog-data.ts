import {City} from '../../../../../../api/models';

export class ConstructBuildingDialogData {

  constructor(public readonly city: City,
              public readonly site: number) {
  }

}
