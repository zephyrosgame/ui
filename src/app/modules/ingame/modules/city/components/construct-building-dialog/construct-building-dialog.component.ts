import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Building} from '@api/models';
import {ConstructBuildingDialogData} from './construct-building-dialog-data';
import {BuildingsApiService, BuildingSitesApiService} from '@api/services';
import {Observable} from 'rxjs';
import {BuildingSitesManagementService} from '../../../../services/building-sites-management.service';

@Component({
  selector: 'zephyros-construct-building-dialog',
  templateUrl: './construct-building-dialog.component.html',
  styleUrls: ['./construct-building-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConstructBuildingDialogComponent implements OnInit {

  buildings$: Observable<Building[]>;

  constructor(private dialogRef: MatDialogRef<ConstructBuildingDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ConstructBuildingDialogData,
              private buildingsApiService: BuildingsApiService,
              private buildingSitesApiService: BuildingSitesApiService,
              private buildingSitesManagement: BuildingSitesManagementService) {
  }

  ngOnInit(): void {
    this.buildings$ = this.buildingsApiService.getBuildings();
  }

  startConstruction(building: Building): void {
    this.buildingSitesApiService.createBuildingSite(this.data.city.id, this.data.site, building.id).subscribe(() => {
      this.buildingSitesManagement.fetchBuildingSite(this.data.city, this.data.site);
      this.dialogRef.close();
    });
  }

}
