import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {EMPTY, Observable} from 'rxjs';
import {BuildingSite} from '@api/models';
import {filter, take} from 'rxjs/operators';
import {BuildingSitesManagementService} from '../../../services/building-sites-management.service';

@Injectable()
export class BuildingSiteResolver implements Resolve<BuildingSite> {

  constructor(private logger: NGXLogger,
              private buildingSitesManagement: BuildingSitesManagementService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<BuildingSite> | Observable<never> {
    if (!route.parent || !route.parent.parent || !route.parent.parent.parent || !route.parent.parent.parent.parent) {
      this.logger.error('can not determine world id parameter');
      return EMPTY;
    }

    const worldId = route.parent.parent.parent.parent.paramMap.get('worldId');

    if (!worldId) {
      this.logger.error('no world id given');
      return EMPTY;
    }

    const buildingId = route.url[0].path;

    return this.buildingSitesManagement.getBuildingSite(buildingId).pipe(
      take(1),
      filter((buildingSite: BuildingSite | null): buildingSite is BuildingSite => buildingSite !== null)
    );
  }

}
