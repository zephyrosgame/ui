import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {CityScreenComponent} from './components/city-screen/city-screen.component';
import {CityMapComponent} from './components/city-map/city-map.component';
import {PalaceGuard} from './guards';
import {SelectedCityMapComponent} from './components/selected-city-map/selected-city-map.component';
import {ProsperityBarComponent} from './components/prosperity-bar/prosperity-bar.component';
import {WidgetsModule} from '../../../widgets/widgets.module';
import {BuildingSiteResolver} from './resolvers/building-site.resolver';
import {WallGuard} from './guards/wall.guard';
import {CityRoutingModule} from './city-routing.module';
import {MatDialogModule} from '@angular/material';
import {ConstructBuildingDialogComponent} from './components/construct-building-dialog/construct-building-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    CityRoutingModule,
    TranslateModule,
    WidgetsModule,
    MatDialogModule
  ],
  declarations: [
    CityScreenComponent,
    CityMapComponent,
    SelectedCityMapComponent,
    ProsperityBarComponent,
    ConstructBuildingDialogComponent
  ],
  entryComponents: [
    ConstructBuildingDialogComponent
  ],
  providers: [
    BuildingSiteResolver,
    PalaceGuard,
    WallGuard
  ]
})
export class CityModule {
}
