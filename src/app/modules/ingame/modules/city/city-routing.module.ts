import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CityScreenComponent} from './components/city-screen/city-screen.component';
import {SelectedCityMapComponent} from './components/selected-city-map/selected-city-map.component';
import {PalaceGuard} from './guards';
import {BuildingSiteResolver} from './resolvers/building-site.resolver';
import {WallGuard} from './guards/wall.guard';

const routes: Routes = [
  {
    path: '',
    component: CityScreenComponent,
    children: [
      {
        path: '',
        component: SelectedCityMapComponent
      },
      {
        path: 'building',
        children: [
          {
            path: 'palace',
            loadChildren: () => import('./modules/palace/palace.module').then(m => m.PalaceModule),
            canActivate: [PalaceGuard],
            resolve: {
              buildingSite: BuildingSiteResolver
            }
          },
          {
            path: 'wall',
            loadChildren: () => import('./modules/wall/wall.module').then(m => m.WallModule),
            canActivate: [WallGuard],
            resolve: {
              buildingSite: BuildingSiteResolver
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CityRoutingModule {
}
