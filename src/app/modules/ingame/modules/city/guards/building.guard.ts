import {Injector} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import {NGXLogger} from 'ngx-logger';
import {map, tap} from 'rxjs/operators';
import {RoutingService} from '../../../../../services';
import {BuildingSite} from '../../../../../api';
import {BuildingSitesManagementService} from '../../../services/building-sites-management.service';

export abstract class BuildingGuard implements CanActivate {

  private readonly logger: NGXLogger;
  private readonly buildingSitesManagement: BuildingSitesManagementService;
  private readonly route: RoutingService;

  protected constructor(private injector: Injector,
                        private buildingId: string) {
    this.logger = this.injector.get<NGXLogger>(NGXLogger);
    this.buildingSitesManagement = this.injector.get<BuildingSitesManagementService>(BuildingSitesManagementService);
    this.route = this.injector.get<RoutingService>(RoutingService);
  }

  canActivate(next: ActivatedRouteSnapshot): Observable<boolean> | boolean {
    if (!next.parent || !next.parent.parent || !next.parent.parent.parent || !next.parent.parent.parent.parent) {
      this.logger.error('can not determine world id parameter');
      this.route.toWorldsPage();
      return false;
    }

    const worldId = next.parent.parent.parent.parent.paramMap.get('worldId');

    if (!worldId) {
      this.logger.error('no world id given');
      this.route.toWorldsPage();
      return false;
    }

    return this.buildingSitesManagement.getBuildingSite(this.buildingId).pipe(
      map((buildingSite: BuildingSite | null) => buildingSite !== null),
      tap((canActivate: boolean) => {
        if (!canActivate) {
          this.logger.warn(`access forbidden, selected city has no building with id '${this.buildingId}'`);
          this.route.toWorld(worldId);
        }
      })
    );
  }

}
