import {Injectable, Injector} from '@angular/core';
import {BuildingGuard} from './building.guard';

@Injectable()
export class WallGuard extends BuildingGuard {

  constructor(injector: Injector) {
    super(injector, 'wall');
  }

}
