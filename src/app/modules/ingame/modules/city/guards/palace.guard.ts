import {Injectable, Injector} from '@angular/core';
import {BuildingGuard} from './building.guard';

@Injectable()
export class PalaceGuard extends BuildingGuard {

  constructor(injector: Injector) {
    super(injector, 'palace');
  }

}
