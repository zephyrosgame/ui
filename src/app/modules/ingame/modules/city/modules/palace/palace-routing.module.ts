import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PalaceScreenComponent} from './components/palace-screen/palace-screen.component';
import {PalaceOverviewScreenComponent} from './components/palace-overview-screen/palace-overview-screen.component';
import {PalaceRenameScreenComponent} from './components/palace-rename-screen/palace-rename-screen.component';

const routes: Routes = [
  {
    path: '',
    component: PalaceScreenComponent,
    children: [
      {
        path: '',
        redirectTo: 'overview'
      },
      {
        path: 'overview',
        component: PalaceOverviewScreenComponent
      },
      {
        path: 'rename',
        component: PalaceRenameScreenComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PalaceRoutingModule {
}
