import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-palace-sidebar',
  templateUrl: './palace-sidebar.component.html',
  styleUrls: ['./palace-sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PalaceSidebarComponent {
}
