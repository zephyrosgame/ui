import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {first, map, switchMap} from 'rxjs/operators';
import {City, RandomCityNameResponse, UpdateCityDto} from '@api/models';
import {CitiesApiService} from '@api/services';
import {Icon} from '../../../../../../../widgets/components/icon-widget/icon.enum';
import {PlayerCitiesManagementService} from '../../../../../../services/player-cities-management.service';

@Component({
  selector: 'zephyros-palace-rename-screen',
  templateUrl: './palace-rename-screen.component.html',
  styleUrls: ['./palace-rename-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PalaceRenameScreenComponent implements OnInit {

  readonly Icon = Icon;

  dto: UpdateCityDto = {
    name: ''
  };

  isRequestingRandomName = false;

  isWaitingForResponse = true;

  cityNameSuccessfullySaved = false;

  private currentCityName: string;

  constructor(private citiesApiService: CitiesApiService,
              private playerCitiesManagement: PlayerCitiesManagementService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.playerCitiesManagement.selectedCity$.pipe(
      first(),
      map((city: City) => city.name)
    ).subscribe((name: string) => {
      this.isWaitingForResponse = false;
      this.nameUpdated(name);
    });
  }

  cityNameChanged(): void {
    this.cityNameSuccessfullySaved = false;
  }

  get canSave(): boolean {
    return !this.isRequestingRandomName
      && !this.isWaitingForResponse
      && !this.cityNameSuccessfullySaved
      && this.dto.name !== this.currentCityName;
  }

  onSubmit(): void {
    this.isWaitingForResponse = true;

    this.playerCitiesManagement.selectedCity$.pipe(
      first(),
      switchMap((city: City) => this.citiesApiService.updateCity(city.id, this.dto))
    ).subscribe((city: City) => {
      this.isWaitingForResponse = false;
      this.cityNameSuccessfullySaved = true;

      this.nameUpdated(city.name);

      this.playerCitiesManagement.cityUpdated(city);
    }, (error) => {
      this.isWaitingForResponse = false;
      // todo
    });
  }

  requestRandomName(): void {
    this.isRequestingRandomName = true;

    this.citiesApiService.getRandomCityName().pipe(
      map((response: RandomCityNameResponse) => response.name)
    ).subscribe((name: string) => {
      this.isRequestingRandomName = false;
      this.dto.name = name;
      this.cityNameSuccessfullySaved = false;
      this.cdr.markForCheck();
    }, (error) => { // todo
      this.isRequestingRandomName = false;
      // todo
      this.cdr.markForCheck();
    });
  }

  private nameUpdated(newName: string): void {
    this.dto.name = newName;
    this.currentCityName = newName;
    this.cdr.markForCheck();
  }

}
