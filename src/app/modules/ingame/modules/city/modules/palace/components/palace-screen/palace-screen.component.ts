import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Observable} from 'rxjs';
import {BuildingSite} from '@api/models';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'zephyros-palace-screen',
  templateUrl: './palace-screen.component.html',
  styleUrls: ['./palace-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PalaceScreenComponent {

  readonly buildingSite$: Observable<BuildingSite>;

  constructor(route: ActivatedRoute) {
    this.buildingSite$ = route.data.pipe(
      map(data => data['buildingSite'])
    );
  }

}
