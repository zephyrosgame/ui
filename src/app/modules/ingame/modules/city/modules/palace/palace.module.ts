import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {IngameWidgetsModule} from '../../../ingame-widgets/ingame-widgets.module';
import {PalaceScreenComponent} from './components/palace-screen/palace-screen.component';
import {PalaceSidebarComponent} from './components/palace-sidebar/palace-sidebar.component';
import {PalaceOverviewScreenComponent} from './components/palace-overview-screen/palace-overview-screen.component';
import {PalaceRenameScreenComponent} from './components/palace-rename-screen/palace-rename-screen.component';
import {PalaceRoutingModule} from './palace-routing.module';
import {WidgetsModule} from '../../../../../widgets/widgets.module';

@NgModule({
  imports: [
    CommonModule,
    PalaceRoutingModule,
    FormsModule,
    TranslateModule,
    IngameWidgetsModule,
    WidgetsModule
  ],
  declarations: [
    PalaceScreenComponent,
    PalaceSidebarComponent,
    PalaceOverviewScreenComponent,
    PalaceRenameScreenComponent
  ]
})
export class PalaceModule {
}
