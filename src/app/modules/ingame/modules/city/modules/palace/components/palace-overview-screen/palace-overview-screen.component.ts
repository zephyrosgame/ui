import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Building, BuildingSite} from '@api/models';
import {BuildingSitesManagementService} from '../../../../../../services/building-sites-management.service';

@Component({
  selector: 'zephyros-palace-overview-screen',
  templateUrl: './palace-overview-screen.component.html',
  styleUrls: ['./palace-overview-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PalaceOverviewScreenComponent implements OnInit {

  readonly Building = Building;

  buildingSites$: Observable<BuildingSite[]>;

  constructor(private buildingSitesManagement: BuildingSitesManagementService) {
  }

  ngOnInit(): void {
    this.buildingSites$ = this.buildingSitesManagement.buildingSites$;
  }

}
