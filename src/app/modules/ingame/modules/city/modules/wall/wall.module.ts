import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {IngameWidgetsModule} from '../../../ingame-widgets/ingame-widgets.module';
import {WallScreenComponent} from './components/wall-screen/wall-screen.component';
import {WallRoutingModule} from './wall-routing.module';

@NgModule({
  imports: [
    CommonModule,
    WallRoutingModule,
    FormsModule,
    TranslateModule,
    IngameWidgetsModule
  ],
  declarations: [
    WallScreenComponent
  ]
})
export class WallModule {
}
