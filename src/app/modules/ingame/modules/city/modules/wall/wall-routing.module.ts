import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WallScreenComponent} from './components/wall-screen/wall-screen.component';

const routes: Routes = [
  {
    path: '',
    component: WallScreenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WallRoutingModule {
}
