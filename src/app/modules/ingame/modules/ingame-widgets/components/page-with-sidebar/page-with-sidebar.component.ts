import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-page-with-sidebar',
  templateUrl: './page-with-sidebar.component.html',
  styleUrls: ['./page-with-sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageWithSidebarComponent {
}
