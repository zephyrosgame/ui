import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-sidebar-block',
  templateUrl: './sidebar-block.component.html',
  styleUrls: ['./sidebar-block.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarBlockComponent {
}
