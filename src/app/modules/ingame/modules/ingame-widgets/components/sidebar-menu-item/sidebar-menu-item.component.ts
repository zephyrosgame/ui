import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'zephyros-sidebar-menu-item',
  templateUrl: './sidebar-menu-item.component.html',
  styleUrls: ['./sidebar-menu-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarMenuItemComponent {

  @Input() label: string;

  @Input() routerLink: string;

}
