import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarMenuComponent {
}
