import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Building, BuildingSite} from '@api/models';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IngameRoutingService} from '../../../../services/ingame-routing.service';
import {BuildingSitesManagementService} from '../../../../services/building-sites-management.service';

@Component({
  selector: 'zephyros-building-template',
  templateUrl: './building-template.component.html',
  styleUrls: ['./building-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BuildingTemplateComponent implements OnInit {

  @Input() buildingSite: BuildingSite;

  buildingSites$: Observable<BuildingSite[]>;

  constructor(private buildingSitesManagement: BuildingSitesManagementService,
              private ingameRoutingService: IngameRoutingService) {
  }

  ngOnInit(): void {
    this.buildingSites$ = this.buildingSitesManagement.buildingSites$.pipe(
      map((sites: BuildingSite[]) => sites.filter((site: BuildingSite) => {
        return site.building.id !== this.buildingSite.building.id && site.building.category === this.buildingSite.building.category;
      }))
    );
  }

  selectBuilding(building: Building): void {
    this.ingameRoutingService.toBuilding(building);
  }

}
