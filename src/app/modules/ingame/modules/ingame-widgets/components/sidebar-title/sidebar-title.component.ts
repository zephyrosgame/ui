import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-sidebar-title',
  templateUrl: './sidebar-title.component.html',
  styleUrls: ['./sidebar-title.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarTitleComponent {
}
