import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {BuildingSite} from '@api/models';

@Component({
  selector: 'zephyros-building-site-switcher',
  templateUrl: './building-site-switcher.component.html',
  styleUrls: ['./building-site-switcher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BuildingSiteSwitcherComponent {

  @Input() buildingSite: BuildingSite;

  @Input() active: boolean;

}
