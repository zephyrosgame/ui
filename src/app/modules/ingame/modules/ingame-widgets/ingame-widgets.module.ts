import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {PageWithSidebarComponent} from './components/page-with-sidebar/page-with-sidebar.component';
import {SidebarTitleComponent} from './components/sidebar-title/sidebar-title.component';
import {SidebarBlockComponent} from './components/sidebar-block/sidebar-block.component';
import {SidebarMenuComponent} from './components/sidebar-menu/sidebar-menu.component';
import {SidebarMenuItemComponent} from './components/sidebar-menu-item/sidebar-menu-item.component';
import {BuildingTemplateComponent} from './components/building-template/building-template.component';
import {TranslateModule} from '@ngx-translate/core';
import {BuildingSiteSwitcherComponent} from './components/building-site-switcher/building-site-switcher.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule
  ],
  declarations: [
    PageWithSidebarComponent,
    SidebarTitleComponent,
    SidebarBlockComponent,
    SidebarMenuComponent,
    SidebarMenuItemComponent,
    BuildingTemplateComponent,
    BuildingSiteSwitcherComponent
  ],
  exports: [
    PageWithSidebarComponent,
    SidebarTitleComponent,
    SidebarBlockComponent,
    SidebarMenuComponent,
    SidebarMenuItemComponent,
    BuildingTemplateComponent
  ]
})
export class IngameWidgetsModule {
}
