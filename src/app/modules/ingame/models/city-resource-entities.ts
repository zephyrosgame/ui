import {CityResource} from '@api/models';

export interface CityResourceEntities {
  [resourceId: string]: CityResource;
}
