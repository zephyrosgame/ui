import {BuildingSite} from '@api/models';

export interface BuildingSiteEntities {
  [buildingId: string]: BuildingSite;
}
