import {City} from '@api/models';

export interface CityEntities {
  [cityId: string]: City;
}
