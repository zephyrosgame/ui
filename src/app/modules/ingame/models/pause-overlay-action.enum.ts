export enum PauseOverlayAction {
  Unpause,
  LeaveGame
}
