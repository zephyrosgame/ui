import {City} from '@api/models';

export const sortByCityName = (city1: City, city2: City): number => {
  const cityName1 = city1.name.toLowerCase();
  const cityName2 = city2.name.toLowerCase();
  if (cityName1 < cityName2) {
    return -1;
  }
  if (cityName1 > cityName2) {
    return 1;
  }
  return 0;
};
