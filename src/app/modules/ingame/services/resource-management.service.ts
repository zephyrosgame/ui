import {Injectable} from '@angular/core';
import {CityResourcesApiService} from '@api/services';
import {map, switchMap} from 'rxjs/operators';
import {City, CityResource} from '@api/models';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {CityResourceEntities} from '../models/city-resource-entities';
import {PlayerCitiesManagementService} from './player-cities-management.service';

@Injectable()
export class ResourceManagementService {

  private readonly cityResources$: Subject<CityResourceEntities> = new ReplaySubject(1);

  constructor(private cityResourcesApiService: CityResourcesApiService,
              private playerCitiesManagement: PlayerCitiesManagementService) {
  }

  getAmountOfResource(resourceId: string): Observable<number> {
    return this.cityResources$.pipe(
      map((entities: CityResourceEntities) => entities[resourceId] ? entities[resourceId].amount : 0)
    );
  }

  fetchCityResources(): void {
    this.playerCitiesManagement.selectedCity$.pipe(
      switchMap((city: City) => this.cityResourcesApiService.getResourcesOfCity(city.id)),
      map((cityResources: CityResource[]) => {
        return cityResources.reduce((entities: CityResourceEntities, cityResource: CityResource) => ({
          ...entities,
          [cityResource.resource.id]: cityResource
        }), {});
      })
    ).subscribe((entities: CityResourceEntities) => {
      this.cityResources$.next(entities);
    });
  }

}
