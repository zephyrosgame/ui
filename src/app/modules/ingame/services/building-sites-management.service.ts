import {Injectable, OnDestroy} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {BuildingSite, City} from '@api/models';
import {first, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {NGXLogger} from 'ngx-logger';
import {BuildingSitesApiService} from '@api/services';
import {PlayerCitiesManagementService} from './player-cities-management.service';
import {BuildingSiteEntities} from '../models/building-site-entities';

@Injectable()
export class BuildingSitesManagementService implements OnDestroy {

  private readonly entities$: Subject<BuildingSiteEntities> = new ReplaySubject(1);

  readonly buildingSites$: Observable<BuildingSite[]> = this.entities$.pipe(
    map((entities: BuildingSiteEntities) => Object.values(entities))
  );

  private readonly destroyed$: Subject<void> = new Subject();

  constructor(private buildingSitesApiService: BuildingSitesApiService,
              private playerCitiesManagement: PlayerCitiesManagementService,
              private logger: NGXLogger) {
    this.playerCitiesManagement.selectedCity$.pipe(
      takeUntil(this.destroyed$)
    ).subscribe((city: City) => this.fetchBuildingSites(city));
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  getBuildingSite(buildingId: string): Observable<BuildingSite | null> {
    return this.entities$.pipe(
      map((entities: BuildingSiteEntities) => entities[buildingId] || null)
    );
  }

  fetchBuildingSites(city: City): void {
    this.logger.trace('Fetching building sites of city', city);

    this.buildingSitesApiService.getBuildingSitesOfCity(city.id).pipe(
      tap((buildingSites: BuildingSite[]) => this.logger.debug(`Building sites retrieved`, buildingSites)),
      map((buildingSites: BuildingSite[]): BuildingSiteEntities => {
        return buildingSites.reduce((entities: BuildingSiteEntities, buildingSite: BuildingSite) => ({
          ...entities,
          [buildingSite.building.id]: buildingSite
        }), {});
      })
    ).subscribe((entities: BuildingSiteEntities) => {
      this.entities$.next(entities);
    });
  }

  fetchBuildingSite(city: City, site: number): void {
    this.logger.trace(`Fetching building site '${site}' of city`, city);

    this.buildingSitesApiService.getBuildingSite(city.id, site).pipe(
      switchMap((buildingSite: BuildingSite) => this.entities$.pipe(
        first(),
        tap((entities: BuildingSiteEntities) => entities[buildingSite.building.id] = buildingSite),
        tap(() => {
          if (buildingSite.construction !== null) {
            setTimeout(() => this.fetchBuildingSite(city, site), buildingSite.construction.timeNeeded * 1000);
          }
        })
      ))
    ).subscribe((entities: BuildingSiteEntities) => {
      this.entities$.next(entities);
    });
  }

}
