import {Injectable} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {Player} from '@api/models';
import {NGXLogger} from 'ngx-logger';

@Injectable()
export class PlayerManagementService {

  private readonly _player$: Subject<Player> = new ReplaySubject(1);

  readonly player$: Observable<Player> = this._player$.asObservable();

  constructor(private logger: NGXLogger) {
  }

  playerRetrieved(player: Player): void {
    this.logger.trace('Player retrieved', player);
    this._player$.next(player);
  }

}
