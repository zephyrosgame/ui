import {Injectable, OnDestroy} from '@angular/core';
import {EMPTY, Observable, of, ReplaySubject, Subject} from 'rxjs';
import {first, map, startWith, switchMap, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {City, Player} from '@api/models';
import {CityEntities} from '../models/city-entities';
import {CitiesApiService} from '@api/services';
import {NGXLogger} from 'ngx-logger';
import {RoutingService} from '../../../services';
import {PlayerManagementService} from './player-management.service';

@Injectable()
export class PlayerCitiesManagementService implements OnDestroy {

  private readonly entities$: Subject<CityEntities> = new ReplaySubject(1);

  readonly cities$: Observable<City[]> = this.entities$.pipe(
    map((entities: CityEntities) => Object.values(entities))
  );

  private readonly selectedCityId$: Subject<string> = new ReplaySubject(1);

  readonly selectedCity$: Observable<City> = this.entities$.pipe(
    switchMap(() => this.selectedCityId$),
    switchMap((selectedCityId: string) => this.getCity(selectedCityId)),
    switchMap((selectedCity: City | null) => {
      if (selectedCity === null) {
        this.logger.warn('Selected a city the player does not know');

        // todo: show error to user

        return this.cities$.pipe(
          first(),
          map((cities: City[]) => cities.find((city: City) => city.capital)),
          switchMap((capitalCity: City | undefined) => {
            if (capitalCity === undefined) {
              this.handlePlayerHasNoCapitalCity();
            } else {
              this.selectCity(capitalCity);
            }

            return EMPTY;
          })
        );
      }

      return of(selectedCity);
    })
  );

  private readonly destroyed$: Subject<void> = new Subject();

  constructor(private citiesApiService: CitiesApiService,
              private logger: NGXLogger,
              private routingService: RoutingService,
              private playerManagement: PlayerManagementService) {
    this.playerManagement.player$.pipe(
      takeUntil(this.destroyed$)
    ).subscribe(() => {
      this.fetchCities();
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  getCity(id: string): Observable<City | null> {
    return this.entities$.pipe(
      map((entities: CityEntities) => entities[id] || null)
    );
  }

  cityUpdated(city: City): void {
    this.entities$.pipe(
      first(),
      tap((entities: CityEntities) => {
        if (entities[city.id] !== undefined) {
          this.logger.debug('Replacing player city entity', entities[city.id], 'with new value', city);
        } else {
          this.logger.debug('Adding player city to entities', city);
        }
      }),
      map((entities: CityEntities) => ({
        ...entities,
        [city.id]: city
      }))
    ).subscribe((entities: CityEntities) => {
      this.entities$.next(entities);
    });
  }

  fetchCities(): void {
    this.logger.debug('Fetching cities of player');

    this.playerManagement.player$.pipe(
      first(),
      switchMap((player: Player) => this.citiesApiService.getCitiesOfPlayer(player.id)),
      tap((cities: City[]) => {
        this.logger.debug('Cities of own player retrieved', cities);
      }),
      switchMap((cities: City[]) => {
        if (cities.length === 0) {
          this.logger.error('Own player has no cities!');
          this.routingService.toWorldsPage();
          return EMPTY;
        }

        return of(cities);
      }),
      map((cities: City[]): CityEntities => {
        return cities.reduce((entities: CityEntities, city: City) => ({
          ...entities,
          [city.id]: city
        }), {});
      }),
      withLatestFrom(this.selectedCityId$.pipe(
        startWith(undefined)
      )),
      switchMap(([entities, selectedCityId]: [CityEntities, string | undefined]) => {
        if (selectedCityId === undefined || entities[selectedCityId] === undefined) {
          // If no city is selected or the previously selected city no longer belongs to the player, select the player's capital city
          const cities: City[] = Object.keys(entities).map((cityId: string) => entities[cityId]);
          const capitalCity: City | undefined = cities.find((city: City) => city.capital);

          if (capitalCity === undefined) {
            this.handlePlayerHasNoCapitalCity();
            return EMPTY;
          }

          this.selectCity(capitalCity);
        }

        return of(entities);
      })
    ).subscribe((entities: CityEntities) => {
      this.entities$.next(entities);
    });
  }

  selectCity(city: City): void {
    this.logger.info(`Selected city with id '${city.id}':`, city);
    this.selectedCityId$.next(city.id);
  }

  private handlePlayerHasNoCapitalCity(): void {
    this.logger.error('Player has no capital city!');

    // todo: show error to user

    this.routingService.toWorldsPage();
  }

}
