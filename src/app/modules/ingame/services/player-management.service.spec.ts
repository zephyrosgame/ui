import {PlayerManagementService} from './player-management.service';
import {NGXLogger} from 'ngx-logger';
import {NGXLoggerMock} from 'ngx-logger/testing';
import {Player} from '@api/models';

const ngxLoggerMock = new NGXLoggerMock() as NGXLogger;

describe('PlayerManagementService', () => {
  let service: PlayerManagementService;

  beforeEach(() => {
    service = new PlayerManagementService(ngxLoggerMock);
  });

  it('should emit the player that was retrieved', (done) => {
    const retrievedPlayer = {
      id: 'somePlayerId'
    } as Player;

    service.playerRetrieved(retrievedPlayer);

    service.player$.subscribe((player: Player) => {
      expect(player).toBe(retrievedPlayer);
      done();
    });
  });
});
