import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {map} from 'rxjs/operators';
import {Building} from '@api/models';

@Injectable()
export class IngameRoutingService {

  private worldId: string | undefined = undefined;

  constructor(private logger: NGXLogger,
              private router: Router,
              private route: ActivatedRoute) {
    const worldRoute = this.route.pathFromRoot[0].firstChild;
    if (worldRoute !== null) {
      worldRoute.params.pipe(
        map(params => params['worldId'])
      ).subscribe(worldId => this.worldId = worldId);
    } else {
      this.logger.error('Activated world route was not found');
    }
  }

  private navigateOnWorld(commands: string[]): void {
    if (this.worldId !== undefined) {
      this.router.navigate(['world', this.worldId, ...commands]);
    } else {
      this.logger.error('Can not navigate on world, world id is not set');
    }
  }

  toBuilding(building: Building): void {
    this.logger.debug('redirecting to building:', building);
    this.navigateOnWorld(['city', 'building', building.id]);
  }

}
