import {PlayerManagementService} from './player-management.service';
import {NGXLogger} from 'ngx-logger';
import {NGXLoggerMock} from 'ngx-logger/testing';
import {City, Player} from '@api/models';
import {PlayerCitiesManagementService} from './player-cities-management.service';
import {CitiesApiService} from '@api/services';
import {RoutingService} from '../../../services';
import {of} from 'rxjs';

const citiesApiServiceMock = {
  getCitiesOfPlayer: (id: string) => {}
} as CitiesApiService;

const ngxLoggerMock = new NGXLoggerMock() as NGXLogger;

const routingServiceMock = {
  toWorldsPage: () => {}
} as RoutingService;

const playerManagementServiceMock = {
  player$: of({
    id: 'somePlayerId'
  } as Player)
} as PlayerManagementService;

const capitalCityMock = {
  id: 'capitalCityId',
  capital: true
} as City;

const normalCityMock = {
  id: 'normalCityId',
  capital: false
} as City;

describe('PlayerCitiesManagementService', () => {
  let service: PlayerCitiesManagementService;

  beforeEach(() => {
    service = new PlayerCitiesManagementService(
      citiesApiServiceMock,
      ngxLoggerMock,
      routingServiceMock,
      playerManagementServiceMock
    );
  });

  describe('fetchCities', () => {
    it('should route to worlds page if player has no cities', () => {
      spyOn(routingServiceMock, 'toWorldsPage');
      spyOn(citiesApiServiceMock, 'getCitiesOfPlayer').and.returnValue(of([]));

      service.fetchCities();

      expect(routingServiceMock.toWorldsPage).toHaveBeenCalledTimes(1);
    });

    it('should select the capital city if no city was previously selected', (done) => {
      spyOn(citiesApiServiceMock, 'getCitiesOfPlayer').and.returnValue(of([
        normalCityMock,
        capitalCityMock
      ]));

      service.fetchCities();

      service.selectedCity$.subscribe((selectedCity: City) => {
        expect(selectedCity).toBe(capitalCityMock);
        done();
      });
    });

    it('should redirect to the worlds page if the player has no capital city', () => {
      spyOn(routingServiceMock, 'toWorldsPage');

      spyOn(citiesApiServiceMock, 'getCitiesOfPlayer').and.returnValue(of([normalCityMock]));
      service.fetchCities();

      expect(routingServiceMock.toWorldsPage).toHaveBeenCalledTimes(1);
    });
  });
});
