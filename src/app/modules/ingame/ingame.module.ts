import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {DialogModule} from '../dialog/dialog.module';
import {WidgetsModule} from '../widgets/widgets.module';
import {IngameScreenComponent} from './components/ingame-screen/ingame-screen.component';
import {CitiesListComponent} from './components/cities-list/cities-list.component';
import {IngameViewNavigationComponent} from './components/ingame-view-navigation/ingame-view-navigation.component';
import {PauseOverlayComponent} from './components/pause-overlay/pause-overlay.component';
import {IngameRoutingModule} from './ingame-routing.module';
import {IngameRoutingService} from './services/ingame-routing.service';
import {ResourceManagementService} from './services/resource-management.service';
import {PlayerCitiesManagementService} from './services/player-cities-management.service';
import {BuildingSitesManagementService} from './services/building-sites-management.service';
import {PlayerManagementService} from './services/player-management.service';
import {PlayerGuard} from './guards/player.guard';
import {ResourcesComponent} from './components/resources/resources.component';
import {MatTooltipModule} from '@angular/material';
import {ResourcesItemComponent} from './components/resources-item/resources-item.component';

@NgModule({
  imports: [
    CommonModule,
    IngameRoutingModule,
    TranslateModule,
    DialogModule,
    WidgetsModule,
    MatTooltipModule
  ],
  declarations: [
    IngameScreenComponent,
    CitiesListComponent,
    IngameViewNavigationComponent,
    PauseOverlayComponent,
    ResourcesComponent,
    ResourcesItemComponent
  ],
  providers: [
    PlayerGuard,
    BuildingSitesManagementService,
    IngameRoutingService,
    PlayerCitiesManagementService,
    PlayerManagementService,
    ResourceManagementService
  ]
})
export class IngameModule {
}
