import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {Observable, of} from 'rxjs';
import {catchError, first, map, tap} from 'rxjs/operators';
import {RoutingService} from '../../../services';
import {Player} from '@api/models';
import {PlayersApiService} from '@api/services';
import {PlayerManagementService} from '../services/player-management.service';

@Injectable()
export class PlayerGuard implements CanActivate {

  constructor(private logger: NGXLogger,
              private route: RoutingService,
              private playersApiService: PlayersApiService,
              private playerManagement: PlayerManagementService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    this.logger.trace('Checking if player on world');

    const worldId = next.paramMap.get('worldId');

    if (!worldId) {
      this.logger.warn('No world id was given');
      this.route.toWorldsPage();
      return false;
    }

    this.logger.trace(`Checking if authenticated user has a player on world with id '${worldId}'`);

    return this.playersApiService.getPlayerOnWorld(worldId).pipe(
      first(),
      tap((player: Player) => this.logger.debug(`player of current user on world '${worldId}' retrieved`, player)),
      tap((player: Player) => this.playerManagement.playerRetrieved(player)),
      map(() => true),
      catchError(() => {
        this.logger.debug(`User has no player on world with id '${worldId}'`);
        this.route.toJoinWorldPage(worldId);
        return of(false);
      })
    );
  }

}
