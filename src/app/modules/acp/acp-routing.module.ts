import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AcpScreenComponent} from './components/acp-screen/acp-screen.component';

const routes: Routes = [
  {
    path: '',
    component: AcpScreenComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'prefix'
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'worlds',
        loadChildren: () => import('./modules/acp-worlds/acp-worlds.module').then(m => m.AcpWorldsModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcpRoutingModule {
}
