import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {AcpScreenComponent} from './components/acp-screen/acp-screen.component';
import {AcpHeaderComponent} from './components/acp-header/acp-header.component';
import {AcpRoutingModule} from './acp-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AcpRoutingModule,
    TranslateModule
  ],
  declarations: [
    AcpScreenComponent,
    AcpHeaderComponent
  ]
})
export class AcpModule {
}
