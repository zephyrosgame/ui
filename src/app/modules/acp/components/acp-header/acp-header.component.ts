import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-acp-header',
  templateUrl: './acp-header.component.html',
  styleUrls: ['./acp-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AcpHeaderComponent {
}
