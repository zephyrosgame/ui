import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {SwitchToAcpAction} from '../../../../store/app';
import {AppState} from '../../../../store/app.state';
import {TitleService} from '../../../../services';

@Component({
  selector: 'zephyros-acp-screen',
  templateUrl: './acp-screen.component.html',
  styleUrls: ['./acp-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AcpScreenComponent implements OnInit {

  constructor(private store: Store<AppState>,
              private titleService: TitleService) {
  }

  ngOnInit(): void {
    this.titleService.setTitle('');

    this.store.dispatch(new SwitchToAcpAction());
  }

}
