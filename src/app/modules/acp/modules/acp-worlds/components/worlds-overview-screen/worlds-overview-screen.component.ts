import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {CreateWorldDto, CreateWorldResponse, CreateWorldResponseCode, World, WorldNameResponse} from '@api/models';
import {WorldsApiService} from '@api/services';

@Component({
  selector: 'zephyros-worlds-overview-screen',
  templateUrl: './worlds-overview-screen.component.html',
  styleUrls: ['./worlds-overview-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorldsOverviewScreenComponent implements OnInit {

  readonly dto: CreateWorldDto = {
    name: ''
  };

  canSubmit = true;

  constructor(private readonly logger: NGXLogger,
              private readonly worldsApiService: WorldsApiService,
              private readonly cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.fetchRandomWorldName();
  }

  async fetchRandomWorldName() {
    this.logger.trace('fetching random world name');

    const response: WorldNameResponse = await this.worldsApiService.getRandomWorldName();
    this.dto.name = response.name;

    this.cdr.markForCheck();
  }

  onSubmit() {
    this.logger.debug(`submit create world form with data`, this.dto);

    if (!this.canSubmit) {
      this.logger.warn('create world form should not have been submitted');
      return;
    }

    this.canSubmit = false;
    this.logger.debug('registration form disabled');

    this.worldsApiService.createWorld(this.dto).then((response: CreateWorldResponse) => {
      if (response.code === CreateWorldResponseCode.Success) {
        this.worldCreated(response.world as World);
      } else if (response.code === CreateWorldResponseCode.NameAlreadyExists) {
        // todo: show error
        this.canSubmit = true;
        this.cdr.markForCheck();
        this.logger.debug('create world form enabled');
      }
    });
  }

  private worldCreated(world: World) {
    this.logger.debug('world created successfully', world);
  }

}
