import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {WorldsOverviewScreenComponent} from './components/worlds-overview-screen/worlds-overview-screen.component';
import {AcpWorldsRoutingModule} from './acp-worlds-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AcpWorldsRoutingModule,
    FormsModule,
    TranslateModule
  ],
  declarations: [
    WorldsOverviewScreenComponent
  ]
})
export class AcpWorldsModule {
}
