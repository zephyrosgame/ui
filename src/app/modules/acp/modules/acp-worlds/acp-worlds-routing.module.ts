import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WorldsOverviewScreenComponent} from './components/worlds-overview-screen/worlds-overview-screen.component';

const routes: Routes = [
  {
    path: '',
    component: WorldsOverviewScreenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcpWorldsRoutingModule {
}
