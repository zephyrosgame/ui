import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-dashboard-screen',
  templateUrl: './dashboard-screen.component.html',
  styleUrls: ['./dashboard-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardScreenComponent {
}
