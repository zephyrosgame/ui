import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {DashboardScreenComponent} from './components/dashboard-screen/dashboard-screen.component';
import {DashboardRoutingModule} from './dashboard-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    TranslateModule
  ],
  declarations: [
    DashboardScreenComponent
  ]
})
export class DashboardModule {
}
