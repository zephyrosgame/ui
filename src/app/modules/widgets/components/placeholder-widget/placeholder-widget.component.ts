import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-placeholder-widget',
  templateUrl: './placeholder-widget.component.html',
  styleUrls: ['./placeholder-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaceholderWidgetComponent  {
}
