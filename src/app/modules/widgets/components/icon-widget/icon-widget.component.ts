import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Icon} from './icon.enum';

@Component({
  selector: 'zephyros-icon-widget',
  templateUrl: './icon-widget.component.html',
  styleUrls: ['./icon-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconWidgetComponent {

  @Input() icon: Icon;

  readonly Icon = Icon;

}
