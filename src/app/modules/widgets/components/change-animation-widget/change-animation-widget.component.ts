import {ChangeDetectorRef, Component, Input} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

const AnimationTime = 300;

@Component({
  selector: 'zephyros-change-animation-widget',
  templateUrl: './change-animation-widget.component.html',
  styleUrls: ['./change-animation-widget.component.scss'],
  animations: [
    trigger('slideInOutOld', [
      state('visible', style({
        opacity: '1'
      })),
      state('hidden', style({
        opacity: '0'
      })),
      transition('visible => hidden', animate(`${AnimationTime}ms ease-in-out`))
    ]),
    trigger('slideInOutCurrent', [
      state('visible', style({
        opacity: '1'
      })),
      state('hidden', style({
        opacity: '0'
      })),
      transition('hidden => visible', animate(`${AnimationTime}ms ease-in-out`))
    ])
  ]
})
export class ChangeAnimationWidgetComponent {

  currentValue: any;
  oldValue: any;

  oldState = 'visible'; // todo: enum
  currentState = 'hidden';

  @Input() set value(value: any) {
    this.currentValue = value;

    if (this.oldValue === undefined || this.oldValue === null) { // todo
      this.oldValue = value;
    } else {
      this.oldState = 'hidden';
      this.currentState = 'visible';

      setTimeout(() => {
        this.oldValue = value;
        this.oldState = 'visible';
        this.currentState = 'hidden';
        this.cdr.markForCheck();
      }, AnimationTime);
    }
  }

  constructor(private cdr: ChangeDetectorRef) {
  }


}
