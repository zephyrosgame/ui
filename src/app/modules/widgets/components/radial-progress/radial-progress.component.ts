import {ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'zephyros-radial-progress',
  templateUrl: './radial-progress.component.html',
  styleUrls: ['./radial-progress.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RadialProgressComponent implements OnInit {

  @Input() set value(value: number) {
    this._value = value;
    this.setProgress();
  }

  @Input() radius: number;

  @ViewChild('progress', {static: true}) progress: ElementRef<SVGCircleElement>;

  size: number;

  strokeWidth: number;

  private _value: number;

  private circumference: number;

  ngOnInit(): void {
    this.strokeWidth = this.radius / 10;
    this.size = this.radius * 2 + this.strokeWidth;

    this.circumference = 2 * this.radius * Math.PI;
    this.progress.nativeElement.style.strokeDasharray = this.circumference + '';

    this.setProgress();
  }

  get value(): number {
    return this._value;
  }

  private setProgress(): void {
    const dashoffset = this.circumference * (1 - this.value / 100);
    this.progress.nativeElement.style.strokeDashoffset = dashoffset + '';
  }

}
