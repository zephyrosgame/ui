import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {CityNameWidgetComponent} from './city-name-widget.component';
import {City} from '@api/models';
import {PlaceholderWidgetComponent} from '../placeholder-widget/placeholder-widget.component';
import {By} from '@angular/platform-browser';

const translations = {
  ['de']: {
    city: {
      placeholderName: 'Polis'
    }
  }
};

const cityMock: City = {
  id: 'cityId',
  name: 'Mockpolis',
  capital: false
};

describe('CityNameWidgetComponent', () => {
  let component: CityNameWidgetComponent;
  let fixture: ComponentFixture<CityNameWidgetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CityNameWidgetComponent,
        PlaceholderWidgetComponent
      ],
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations(translations)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CityNameWidgetComponent);
    component = fixture.componentInstance;
  });

  it('should show placeholder if no city given', () => {
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('zephyros-placeholder-widget'));
    expect(element).toBeTruthy();
  });

  it('should hide placeholder if city given', () => {
    component.city = cityMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('zephyros-placeholder-widget'));
    expect(element).toBeFalsy();
  });

  it('should show actual city name if city given', () => {
    component.city = cityMock;
    fixture.detectChanges();

    expect(fixture.nativeElement.textContent.trim()).toBe('Mockpolis');
  });

  it('should not show a city name if no city given', () => {
    fixture.detectChanges();

    expect(fixture.nativeElement.textContent.trim()).toBe('');
  });
});
