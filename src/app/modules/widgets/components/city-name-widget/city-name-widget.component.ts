import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {City} from '@api/models';

@Component({
  selector: 'zephyros-city-name-widget',
  templateUrl: './city-name-widget.component.html',
  styleUrls: ['./city-name-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CityNameWidgetComponent {

  @Input() city: City;

}
