import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input} from '@angular/core';
import {Icon} from '../icon-widget/icon.enum';

@Component({
  selector: 'zephyros-avatar-widget',
  templateUrl: './avatar-widget.component.html',
  styleUrls: ['./avatar-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarWidgetComponent {

  @Input() set avatarSrc(avatarSrc: string | null) {
    this._avatarSrc = avatarSrc;
    this.loading = true;
  }

  @Input() username: string | null;

  @Input() pending: boolean;

  readonly Icon = Icon;

  loading = true;

  private _avatarSrc: string | null;

  constructor(private cdr: ChangeDetectorRef) {
  }

  get avatarSrc(): string | null {
    return this._avatarSrc;
  }

  get avatar(): string {
    if (this.avatarSrc !== null) {
      return this.avatarSrc;
    }

    // todo
    return '/assets/main/avatars/researcher.png';
  }

  onImageLoaded(): void {
    this.loading = false;
    this.cdr.markForCheck();
  }

}
