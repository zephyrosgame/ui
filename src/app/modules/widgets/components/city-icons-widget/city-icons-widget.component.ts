import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {City} from '@api/models';

@Component({
  selector: 'zephyros-city-icons-widget',
  templateUrl: './city-icons-widget.component.html',
  styleUrls: ['./city-icons-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CityIconsWidgetComponent {

  @Input() city: City | null;

}
