import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {CityIconsWidgetComponent} from './city-icons-widget.component';
import {City} from '@api/models';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {MatTooltipModule} from '@angular/material';

const translations = {
  ['de']: {
    city: {
      capital: 'Hauptstadt'
    }
  }
};

const cityMock: City = {
  id: 'cityId',
  name: 'Mockpolis',
  capital: false
};

const capitalMock: City = {
  id: 'capitalId',
  name: 'Hauptstadtpolis',
  capital: true
};

describe('CityIconsWidgetComponent', () => {
  let component: CityIconsWidgetComponent;
  let fixture: ComponentFixture<CityIconsWidgetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CityIconsWidgetComponent],
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations(translations),
        MatTooltipModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CityIconsWidgetComponent);
    component = fixture.componentInstance;
  });

  it('should not show icons if no city given', () => {
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.icons'));
    expect(element).toBeFalsy();
  });

  it('should show icons if city given', () => {
    component.city = cityMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.icons'));
    expect(element).toBeTruthy();
  });

  it('should not show abandoned icon', () => {
    component.city = cityMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.icon--abandoned'));
    expect(element).toBeFalsy();
  });

  it('should not show capital icon if city is not capital', () => {
    component.city = cityMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.icon--capital'));
    expect(element).toBeFalsy();
  });

  it('should show capital icon if city is capital', () => {
    component.city = capitalMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.icon--capital'));
    expect(element).toBeTruthy();
  });

  it('should not show connection icon', () => {
    component.city = cityMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.icon--connection'));
    expect(element).toBeFalsy();
  });

  it('should not show conquest icon', () => {
    component.city = cityMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.icon--conquest'));
    expect(element).toBeFalsy();
  });

  it('should not show spy icon', () => {
    component.city = cityMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.icon--spy'));
    expect(element).toBeFalsy();
  });
});
