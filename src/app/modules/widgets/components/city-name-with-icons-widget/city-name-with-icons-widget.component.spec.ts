import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {CityNameWithIconsWidgetComponent} from './city-name-with-icons-widget.component';
import {CityNameWidgetComponent} from '../city-name-widget/city-name-widget.component';
import {CityIconsWidgetComponent} from '../city-icons-widget/city-icons-widget.component';
import {City} from '@api/models';
import {PlaceholderWidgetComponent} from '../placeholder-widget/placeholder-widget.component';
import {MatTooltipModule} from '@angular/material';

const cityMock: City = {
  id: 'cityId',
  name: 'Mockpolis',
  capital: false
};

describe('CityNameWithIconsWidgetComponent', () => {
  let component: CityNameWithIconsWidgetComponent;
  let fixture: ComponentFixture<CityNameWithIconsWidgetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CityNameWithIconsWidgetComponent,
        CityNameWidgetComponent,
        CityIconsWidgetComponent,
        PlaceholderWidgetComponent
      ],
      imports: [
        TranslateTestingModule.withTranslations({}),
        MatTooltipModule
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CityNameWithIconsWidgetComponent);
    component = fixture.componentInstance;
  });

  it('should correctly pass city to city-name-widget', () => {
    component.city = cityMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('zephyros-city-name-widget'));
    expect(element.componentInstance.city).toBe(cityMock);
  });

  it('should correctly pass city to city-icons-widget', () => {
    component.city = cityMock;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('zephyros-city-icons-widget'));
    expect(element.componentInstance.city).toBe(cityMock);
  });
});
