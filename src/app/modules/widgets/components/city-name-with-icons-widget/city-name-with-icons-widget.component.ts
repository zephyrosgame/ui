import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {City} from '@api/models';

@Component({
  selector: 'zephyros-city-name-with-icons-widget',
  templateUrl: './city-name-with-icons-widget.component.html',
  styleUrls: ['./city-name-with-icons-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CityNameWithIconsWidgetComponent {

  @Input() city: City | null;

}
