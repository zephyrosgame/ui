import {Directive, ElementRef, HostListener, Input, OnInit} from '@angular/core';
import {ScrollSaverService} from '../../../services/scroll-saver.service';

@Directive({
  selector: '[zephyrosSaveScroll]'
})
export class SaveScrollDirective implements OnInit {

  @Input('zephyrosSaveScroll') id: string;

  constructor(private host: ElementRef,
              private scrollSaver: ScrollSaverService) {
  }

  ngOnInit(): void {
    const scrollState = this.scrollSaver.getScrollState(this.id);
    if (scrollState !== undefined) {
      this.host.nativeElement.scrollTop = scrollState;
      // todo: handle resize
    }
  }

  @HostListener('scroll', ['$event'])
  onScroll(event: Event): void {
    // todo: wait until typescript team fixes their bug
    // @ts-ignore
    if (event.srcElement !== null && event.srcElement.scrollTop !== 0) {
      // @ts-ignore
      this.scrollSaver.saveScrollState(this.id, event.srcElement.scrollTop);
    }
  }

}
