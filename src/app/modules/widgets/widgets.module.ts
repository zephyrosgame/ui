import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {ChangeAnimationWidgetComponent} from './components/change-animation-widget/change-animation-widget.component';
import {CityNameWidgetComponent} from './components/city-name-widget/city-name-widget.component';
import {CityIconsWidgetComponent} from './components/city-icons-widget/city-icons-widget.component';
import {CityNameWithIconsWidgetComponent} from './components/city-name-with-icons-widget/city-name-with-icons-widget.component';
import {SaveScrollDirective} from './directives/save-scroll.directive';
import {IconWidgetComponent} from './components/icon-widget/icon-widget.component';
import {PlaceholderWidgetComponent} from './components/placeholder-widget/placeholder-widget.component';
import {NumeralModule} from 'ngx-numeral';
import {AvatarWidgetComponent} from './components/avatar-widget/avatar-widget.component';
import {RadialProgressComponent} from './components/radial-progress/radial-progress.component';
import {MatTooltipModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    NumeralModule,
    MatTooltipModule
  ],
  declarations: [
    ChangeAnimationWidgetComponent,
    CityNameWidgetComponent,
    CityIconsWidgetComponent,
    CityNameWithIconsWidgetComponent,
    SaveScrollDirective,
    IconWidgetComponent,
    PlaceholderWidgetComponent,
    AvatarWidgetComponent,
    RadialProgressComponent
  ],
  exports: [
    NumeralModule,
    ChangeAnimationWidgetComponent,
    CityNameWidgetComponent,
    CityIconsWidgetComponent,
    CityNameWithIconsWidgetComponent,
    SaveScrollDirective,
    IconWidgetComponent,
    PlaceholderWidgetComponent,
    AvatarWidgetComponent,
    RadialProgressComponent
  ]
})
export class WidgetsModule {
}
