import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {UserRole} from '@api/models';
import {userDropdownAnimation} from '../../animations/user-dropdown.animation';
import {AuthState} from '../../../../store/auth';

@Component({
  selector: 'zephyros-main-user-menu',
  templateUrl: './main-user-menu.component.html',
  styleUrls: ['./main-user-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [userDropdownAnimation]
})
export class MainUserMenuComponent {

  @Input() authState: AuthState;

  @Output() logout: EventEmitter<void> = new EventEmitter();

  isDropdownVisible = false;

  constructor(private elementRef: ElementRef) {
  }

  get isAuthenticationPending(): boolean {
    return this.authState.pending;
  }

  get isGuest(): boolean {
    return this.authState.user === null;
  }

  get username(): string {
    return this.authState.user !== null ? this.authState.user.username : '';
  }

  get showAcpLink(): boolean {
    return this.authState.user !== null
      && this.authState.user.authorities.find(authority => authority.authority === UserRole.Admin) !== undefined;
  }

  toggleDropdown(): void {
    if (!this.isAuthenticationPending && !this.isGuest) {
      this.isDropdownVisible ? this.hideDropdown() : this.showDropdown();
    }
  }

  hideDropdown(): void {
    this.isDropdownVisible = false;
  }

  showDropdown(): void {
    this.isDropdownVisible = true;
  }

  handleLogout(): void {
    this.logout.emit();
    this.hideDropdown();
  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event: MouseEvent): void {
    if (this.isDropdownVisible && !this.elementRef.nativeElement.contains(event.target)) {
      this.hideDropdown();
    }
  }

}
