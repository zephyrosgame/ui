import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Component, NgZone, NO_ERRORS_SCHEMA} from '@angular/core';
import {Location} from '@angular/common';
import {Router, Routes} from '@angular/router';
import {By} from '@angular/platform-browser';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {MainUserMenuComponent} from './main-user-menu.component';
import {UserRole} from '@api/models';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {AuthState} from '../../../../store/auth';

const routes: Routes = [
  {path: '', component: Component},
  {path: 'fake', component: Component},
  {path: 'join', component: Component},
  {path: 'profile/:id', component: Component},
  {path: 'settings', component: Component},
  {path: 'acp', component: Component}
];

const translations = {
  ['de']: {
    'acp.nav-label': 'Admin',
    'guest': 'Gast',
    'logout': 'Abmelden',
    'profile.nav-label': 'Profil',
    'settings.nav-label': 'Einstellungen',
    'username-pending': 'Laden...'
  }
};

const pendingState: AuthState = {
  pending: true,
  user: null
};

const unauthenticatedState: AuthState  = {
  pending: false,
  user: null
};

const userAuthenticatedState: AuthState = {
  pending: false,
  user: {
    id: 'someUserId',
    username: 'Mockuser',
    email: 'user@mock.com',
    avatar: 'avatar.png',
    authorities: [
      {authority: UserRole.User}
    ]
  }
};

const adminAuthenticatedState: AuthState = {
  pending: false,
  user: {
    id: 'someAdminId',
    username: 'Mockadmin',
    email: 'admin@mock.com',
    avatar: 'avatar.png',
    authorities: [
      {authority: UserRole.Admin}
    ]
  }
};

describe('MainUserMenuComponent', () => {
  let component: MainUserMenuComponent;
  let fixture: ComponentFixture<MainUserMenuComponent>;
  let ngZone: NgZone;
  let location: Location;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MainUserMenuComponent
      ],
      imports: [
        RouterTestingModule.withRoutes(routes),
        TranslateTestingModule.withTranslations(translations),
        NoopAnimationsModule
      ],
      providers: [
        Location
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(MainUserMenuComponent);
    component = fixture.componentInstance;

    ngZone = TestBed.get(NgZone);
    location = TestBed.get(Location);
    router = TestBed.get(Router);
    ngZone.run(() => router.initialNavigation());
  });

  it('should route to join page if no user logged in', fakeAsync(() => {
    // ensure that another route is active than the one it should route to
    ngZone.run(() => router.navigate(['/']));
    tick();
    expect(location.path()).toBe('');

    component.authState = unauthenticatedState;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.user-menu'));
    element.nativeElement.click();
    tick();
    expect(location.path()).toBe('/join');
  }));

  it(`should not route to another page if logged in`, fakeAsync(() => {
    // ensure that another route is active than the one it should route to
    ngZone.run(() => router.navigate(['/fake']));
    tick();
    expect(location.path()).toBe('/fake');

    component.authState = userAuthenticatedState;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.user-menu'));
    element.nativeElement.click();
    tick();
    expect(location.path()).toBe('/fake');
  }));

  it('should show that the username is pending if the auth state is pending', () => {
    component.authState = pendingState;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.user-menu__username'));
    expect(element.nativeElement.value).toBe('Laden...');
  });

  it(`should display 'Gast' as username if no user is logged in`, () => {
    component.authState = unauthenticatedState;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.user-menu__username'));
    expect(element.nativeElement.value).toBe('Gast');
  });

  it('should display username of logged in user if a user is logged in', () => {
    component.authState = userAuthenticatedState;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.user-menu__username'));
    expect(element.nativeElement.value).toBe('Mockuser');
  });

  it('should not show a dropdown by default if no user is logged in', () => {
    component.authState = unauthenticatedState;
    fixture.detectChanges();

    const dropdown = fixture.debugElement.query(By.css('.user-dropdown'));
    expect(dropdown).toBeFalsy();
  });

  it('should not show a dropdown by default if a user is logged in', () => {
    component.authState = userAuthenticatedState;
    fixture.detectChanges();

    const dropdown = fixture.debugElement.query(By.css('.user-dropdown'));
    expect(dropdown).toBeFalsy();
  });

  it('should not show a dropdown if no user is logged in and a click is performed', () => {
    component.authState = unauthenticatedState;
    fixture.detectChanges();

    const userMenu = fixture.debugElement.query(By.css('.user-menu'));
    userMenu.nativeElement.click();
    fixture.detectChanges();

    const dropdown = fixture.debugElement.query(By.css('.user-dropdown'));
    expect(dropdown).toBeFalsy();
  });

  it('should show a dropdown if user is logged in and a click is performed', () => {
    component.authState = userAuthenticatedState;
    fixture.detectChanges();

    const userMenu = fixture.debugElement.query(By.css('.user-menu'));
    userMenu.nativeElement.click();
    fixture.detectChanges();

    const dropdown = fixture.debugElement.query(By.css('.user-dropdown'));
    expect(dropdown).toBeTruthy();
  });

  it('should show exactly 3 links in the dropdown for a user', () => {
    component.authState = userAuthenticatedState;
    component.isDropdownVisible = true;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.user-dropdown__link'));
    expect(elements.length).toBe(3);
  });

  it('should show exactly 4 links in the dropdown for an admin', () => {
    component.authState = adminAuthenticatedState;
    component.isDropdownVisible = true;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.user-dropdown__link'));
    expect(elements.length).toBe(4);
  });

  it(`should have a link to the user's profile in the dropdown`, fakeAsync(() => {
    // ensure that another route is active than the one it should route to
    ngZone.run(() => router.navigate(['/']));
    tick();
    expect(location.path()).toBe('');

    component.authState = userAuthenticatedState;
    component.isDropdownVisible = true;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.user-dropdown__link'));
    const link = elements.find(element => element.nativeElement.textContent.trim() === 'Profil');

    if (link !== undefined) {
      link.nativeElement.click();
      tick();
      expect(location.path()).toBe('/profile/someUserId');
    } else {
      fail('Profile link does not exist');
    }
  }));

  it('should have a link to the settings in the dropdown', fakeAsync(() => {
    // ensure that another route is active than the one it should route to
    ngZone.run(() => router.navigate(['/']));
    tick();
    expect(location.path()).toBe('');

    component.authState = userAuthenticatedState;
    component.isDropdownVisible = true;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.user-dropdown__link'));
    const link = elements.find(element => element.nativeElement.textContent.trim() === 'Einstellungen');

    if (link !== undefined) {
      link.nativeElement.click();
      tick();
      expect(location.path()).toBe('/settings');
    } else {
      fail('Settings link does not exist');
    }
  }));

  it('should output the logout event if logout link in dropdown is clicked', () => {
    spyOn(component.logout, 'emit');

    component.authState = userAuthenticatedState;
    component.isDropdownVisible = true;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.user-dropdown__link'));
    const link = elements.find(element => element.nativeElement.textContent.trim() === 'Abmelden');

    if (link !== undefined) {
      link.nativeElement.click();

      expect(component.logout.emit).toHaveBeenCalledTimes(1);
    } else {
      fail('LogoutAction link does not exist');
    }
  });

  it(`should have a link to the ACP for an admin user in the dropdown`, fakeAsync(() => {
    // ensure that another route is active than the one it should route to
    ngZone.run(() => router.navigate(['/']));
    tick();
    expect(location.path()).toBe('');

    component.authState = adminAuthenticatedState;
    component.isDropdownVisible = true;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.user-dropdown__link'));
    const link = elements.find(element => element.nativeElement.textContent.trim() === 'Admin');

    if (link !== undefined) {
      link.nativeElement.click();
      tick();
      expect(location.path()).toBe('/acp');
    } else {
      fail('ACP does not exist');
    }
  }));
});
