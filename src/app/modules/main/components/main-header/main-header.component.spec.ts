import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {UserRole} from '@api/models';
import {MainHeaderComponent} from './main-header.component';
import {MainHeaderMenuItemComponent} from '../main-header-menu-item/main-header-menu-item.component';
import {MainUserMenuComponent} from '../main-user-menu/main-user-menu.component';
import {AuthState} from '../../../../store/auth';
import {MockComponents} from 'ng-mocks';
import {DebugElement} from '@angular/core';

const translations = {
  ['de']: {
    'worlds.nav-label': 'Welten',
    'about.nav-label': 'Das Spiel',
    'blog.nav-label': 'Blog'
  }
};

const userMock1AuthenticatedState: AuthState = {
  pending: false,
  user: {
    id: 'someUserId',
    username: 'Mockuser1',
    email: 'user1@mock.com',
    avatar: 'avatar.png',
    authorities: [
      {authority: UserRole.User}
    ]
  }
};

const userMock2AuthenticatedState: AuthState = {
  pending: false,
  user: {
    id: 'anotherUserId',
    username: 'Mockuser2',
    email: 'user2@mock.com',
    avatar: 'avatar.png',
    authorities: [
      {authority: UserRole.Admin}
    ]
  }
};

const pendingState: AuthState = {
  pending: true,
  user: null
};

describe('MainHeaderComponent', () => {
  let component: MainHeaderComponent;
  let fixture: ComponentFixture<MainHeaderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MainHeaderComponent,
        MockComponents(MainHeaderMenuItemComponent, MainUserMenuComponent)
      ],
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations(translations)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MainHeaderComponent);
    component = fixture.componentInstance;
  });

  it('should have exactly one logo', fakeAsync(() => {
    component.authState = pendingState;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.logo'));
    expect(elements.length).toBe(1);
  }));

  it('should correctly display a single menu item', () => {
    component.menuItems = [
      {translationKey: 'blog.nav-label', routerLink: '/blog'},
    ];
    component.authState = pendingState;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('zephyros-main-header-menu-item'));
    expect(elements.length).toBe(1);

    const components: MainHeaderMenuItemComponent[] = elements.map((element: DebugElement) => element.componentInstance);

    expect(components[0].label).toBe('Blog');
    expect(components[0].link).toBe('/blog');
  });

  it('should correctly display multiple menu items', () => {
    component.menuItems = [
      {translationKey: 'worlds.nav-label', routerLink: '/'},
      {translationKey: 'about.nav-label', routerLink: '/about'}
    ];
    component.authState = pendingState;
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('zephyros-main-header-menu-item'));
    expect(elements.length).toBe(2);

    const components: MainHeaderMenuItemComponent[] = elements.map((element: DebugElement) => element.componentInstance);

    expect(components[0].label).toBe('Welten');
    expect(components[0].link).toBe('/');

    expect(components[1].label).toBe('Das Spiel');
    expect(components[1].link).toBe('/about');
  });

  it('should pass the correct user to the user menu component for userMock1', () => {
    component.authState = userMock1AuthenticatedState;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('zephyros-main-user-menu'));
    expect(element.componentInstance.authState).toBe(userMock1AuthenticatedState);
  });

  it('should pass the correct user to the user menu component for userMock2', () => {
    component.authState = userMock2AuthenticatedState;
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('zephyros-main-user-menu'));
    expect(element.componentInstance.authState).toBe(userMock2AuthenticatedState);
  });

  it('should correctly pass logout event', () => {
    component.authState = userMock1AuthenticatedState;
    fixture.detectChanges();

    spyOn(component.logout, 'emit');

    const element = fixture.debugElement.query(By.css('zephyros-main-user-menu'));
    element.componentInstance.logout.emit();

    expect(component.logout.emit).toHaveBeenCalledTimes(1);
  });
});
