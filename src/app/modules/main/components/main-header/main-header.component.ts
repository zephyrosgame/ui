import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {MainHeaderMenuItem} from '../../models';
import {AuthState} from '../../../../store/auth';

@Component({
  selector: 'zephyros-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainHeaderComponent {

  @Input() menuItems: MainHeaderMenuItem[];

  @Input() authState: AuthState;

  @Output() logout: EventEmitter<void> = new EventEmitter();

}
