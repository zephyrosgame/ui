import {ComponentFixture, TestBed} from '@angular/core/testing';
import {Component, EventEmitter, NO_ERRORS_SCHEMA, Output} from '@angular/core';
import {By} from '@angular/platform-browser';
import {RoutingService, TitleService} from '../../../../services';
import {User, UserRole} from '@api/models';
import {MainScreenComponent} from './main-screen.component';
import {RouterTestingModule} from '@angular/router/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Store} from '@ngrx/store';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {AuthState, LogoutAction} from '../../../../store/auth';
import {SwitchToMainAction} from '../../../../store/app';
import {AppState} from '../../../../store/app.state';

const userMock1: User = {
  id: 'someUserId',
  username: 'Mockuser1',
  email: 'user1@mock.com',
  avatar: 'avatar.png',
  authorities: [
    {authority: UserRole.User}
  ]
};

const userMock2: User = {
  id: 'anotherUserId',
  username: 'Mockuser2',
  email: 'user2@mock.com',
  avatar: 'avatar.png',
  authorities: [
    {authority: UserRole.Admin}
  ]
};

const titleServiceMock = {
  setTitle: () => {}
};

const routingServiceMock = {
  toHomepage: () => {}
};

@Component({
  selector: 'zephyros-main-header',
  template: ''
})
class MainHeaderMockComponent {

  @Output() logout = new EventEmitter();

}

function createState(authState: AuthState): AppState {
  return {
    auth: authState,
  } as AppState;
}

describe('MainScreenComponent', () => {
  let component: MainScreenComponent;
  let fixture: ComponentFixture<MainScreenComponent>;
  let store: MockStore<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MainScreenComponent,
        MainHeaderMockComponent
      ],
      imports: [
        RouterTestingModule,
        NoopAnimationsModule
      ],
      providers: [
        {provide: TitleService, useValue: titleServiceMock},
        {provide: RoutingService, useValue: routingServiceMock},
        provideMockStore()
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    spyOn(titleServiceMock, 'setTitle');
    spyOn(routingServiceMock, 'toHomepage');

    store = TestBed.get(Store);
    spyOn(store, 'dispatch');

    fixture = TestBed.createComponent(MainScreenComponent);
    component = fixture.componentInstance;
  });

  it('should switch to main app', () => {
    fixture.detectChanges();

    expect(store.dispatch).toHaveBeenCalledWith(new SwitchToMainAction());
  });

  it('should reset title once', () => {
    fixture.detectChanges();

    expect(titleServiceMock.setTitle).toHaveBeenCalledWith('');
    expect(titleServiceMock.setTitle).toHaveBeenCalledTimes(1);
  });

  it('should correctly handle and pass auth state changes to the main header component', () => {
    const element = fixture.debugElement.query(By.css('zephyros-main-header'));

    const authState1: AuthState = {
      pending: true,
      user: null
    };
    store.setState(createState(authState1));
    fixture.detectChanges();
    expect(element.nativeElement.authState).toBe(authState1);

    const authState2: AuthState = {
      pending: false,
      user: userMock1
    };
    store.setState(createState(authState2));
    fixture.detectChanges();
    expect(element.nativeElement.authState).toBe(authState2);

    const authState3: AuthState = {
      pending: false,
      user: userMock2
    };
    store.setState(createState(authState3));
    fixture.detectChanges();
    expect(element.nativeElement.authState).toBe(authState3);
  });

  it('should dispatch a logout action if a logout event is emitted', () => {
    const element = fixture.debugElement.query(By.css('zephyros-main-header'));
    element.componentInstance.logout.emit();

    expect(store.dispatch).toHaveBeenCalledWith(new LogoutAction());
  });
});
