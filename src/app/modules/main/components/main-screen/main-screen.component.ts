import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {Observable} from 'rxjs';
import {TitleService} from '../../../../services';
import {routingFadeAnimation} from '../../../../animations/routing-fade.animation';
import {guestHeaderMenuItems, MainHeaderMenuItem, userHeaderMenuItems} from '../../models';
import {Store} from '@ngrx/store';
import {AuthState, LogoutAction, selectAuth} from '../../../../store/auth';
import {SwitchToMainAction} from '../../../../store/app';
import {AppState} from '../../../../store/app.state';
import {map} from 'rxjs/operators';

@Component({
  selector: 'zephyros-main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [routingFadeAnimation]
})
export class MainScreenComponent implements OnInit {

  headerMenuItems$: Observable<MainHeaderMenuItem[]>;

  authState$: Observable<AuthState>;

  constructor(private titleService: TitleService,
              private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.store.dispatch(new SwitchToMainAction());

    this.titleService.setTitle('');

    this.authState$ = this.store.select(selectAuth);

    this.headerMenuItems$ = this.authState$.pipe(
      map((authState: AuthState) => authState.user !== null),
      map((isAuthenticated: boolean) => isAuthenticated ? userHeaderMenuItems : guestHeaderMenuItems)
    );
  }

  getRoutePage(outlet: RouterOutlet): string | undefined {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['page'];
  }

  onLogout(): void {
    this.store.dispatch(new LogoutAction());
  }

}
