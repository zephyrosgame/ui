import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'zephyros-main-header-menu-item',
  templateUrl: './main-header-menu-item.component.html',
  styleUrls: ['./main-header-menu-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainHeaderMenuItemComponent {

  @Input() label: string;

  @Input() link: string;

}
