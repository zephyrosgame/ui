import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {MainHeaderMenuItemComponent} from './main-header-menu-item.component';

describe('MainHeaderMenuItemComponent', () => {
  let component: MainHeaderMenuItemComponent;
  let fixture: ComponentFixture<MainHeaderMenuItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MainHeaderMenuItemComponent],
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations({})
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MainHeaderMenuItemComponent);
    component = fixture.componentInstance;
  });

  it('should have the correct label', () => {
    component.label = 'Lorem ipsum';
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.menu-item'));
    expect(element.nativeElement.textContent).toBe('Lorem ipsum');
  });

  it('should route to correct link', () => {
    component.link = '/fake';
    fixture.detectChanges();

    const element = fixture.debugElement.query(By.css('.menu-item'));
    expect(element.nativeElement.href).toContain('/fake');
  });
});
