import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainScreenComponent} from './components/main-screen/main-screen.component';
import {AuthGuard, GuestGuard, JoinWorldGuard} from '../../guards';
import {WorldResolver} from '../../resolvers';
import {HomepageComponent} from './modules/worlds/components/homepage/homepage.component';
import {WorldsScreenComponent} from './modules/worlds/components/worlds-screen/worlds-screen.component';
import {AboutGameScreenComponent} from './modules/about/components/about-game-screen/about-game-screen.component';
import {ExtensionsScreenComponent} from './modules/extensions/components/extensions-screen/extensions-screen.component';
import {ExtensionScreenComponent} from './modules/extensions/components/extension-screen/extension-screen.component';

const routes: Routes = [
  {
    path: '',
    component: MainScreenComponent,
    children: [
      {
        path: '',
        component: HomepageComponent,
        canActivate: [GuestGuard],
        data: {
          page: 'homepage'
        }
      },
      {
        path: 'worlds',
        component: WorldsScreenComponent,
        canActivate: [AuthGuard],
        data: {
          page: 'worlds'
        }
      },
      {
        path: 'extensions',
        children: [
          {
            path: '',
            component: ExtensionsScreenComponent
          },
          {
            path: ':id',
            component: ExtensionScreenComponent
          }
        ],
        data: {
          page: 'extensions'
        }
      },
      {
        path: 'about',
        component: AboutGameScreenComponent,
        data: {
          page: 'about'
        }
      },
      {
        path: 'join',
        loadChildren: () => import('./modules/join/join.module').then(m => m.JoinModule),
        canActivate: [GuestGuard],
        data: {
          page: 'join'
        }
      },
      {
        path: 'join-world/:worldId',
        loadChildren: () => import('./modules/join-world/join-world.module').then(m => m.JoinWorldModule),
        canActivate: [AuthGuard, JoinWorldGuard],
        data: {
          page: 'join-world'
        },
        resolve: {
          world: WorldResolver
        }
      },
      {
        path: 'profile',
        loadChildren: () => import('./modules/profile/profile.module').then(m => m.ProfileModule),
        data: {
          page: 'profile'
        }
      },
      {
        path: 'settings',
        loadChildren: () => import('./modules/settings/settings.module').then(m => m.SettingsModule),
        canActivate: [AuthGuard],
        data: {
          page: 'settings'
        }
      },
      {
        path: 'policies',
        loadChildren: () => import('./modules/policies/policies.module').then(m => m.PoliciesModule),
        data: {
          page: 'policies'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
