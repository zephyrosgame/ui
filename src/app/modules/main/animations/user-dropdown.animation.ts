import {animate, style, transition, trigger} from '@angular/animations';

export const userDropdownAnimation = trigger('userDropdownAnimation', [
  transition(':enter', [
    style({opacity: 0, transform: 'translateY(20px)'}),
    animate('150ms', style({opacity: 1, transform: 'translateY(0)'}))
  ]),
  transition(':leave', [
    animate('150ms', style({opacity: 0, transform: 'translateY(20px)'}))
  ])
]);
