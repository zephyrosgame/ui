import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {MainScreenComponent} from './components/main-screen/main-screen.component';
import {MainHeaderComponent} from './components/main-header/main-header.component';
import {WidgetsModule} from '../widgets/widgets.module';
import {MainUserMenuComponent} from './components/main-user-menu/main-user-menu.component';
import {MainHeaderMenuItemComponent} from './components/main-header-menu-item/main-header-menu-item.component';
import {MainRoutingModule} from './main-routing.module';
import {WorldsModule} from './modules/worlds/worlds.module';
import {MainWidgetsModule} from './modules/main-widgets/main-widgets.module';
import {ExtensionsModule} from './modules/extensions/extensions.module';
import {AboutModule} from './modules/about/about.module';

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    TranslateModule,
    WidgetsModule,
    MainWidgetsModule,
    WorldsModule,
    ExtensionsModule,
    AboutModule
  ],
  declarations: [
    MainScreenComponent,
    MainHeaderComponent,
    MainUserMenuComponent,
    MainHeaderMenuItemComponent
  ]
})
export class MainModule {
}
