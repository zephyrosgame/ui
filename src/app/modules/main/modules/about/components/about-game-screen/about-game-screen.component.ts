import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-about-game-screen',
  templateUrl: './about-game-screen.component.html',
  styleUrls: ['./about-game-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AboutGameScreenComponent {
}
