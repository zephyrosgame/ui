import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {AboutGameScreenComponent} from './components/about-game-screen/about-game-screen.component';
import {MainWidgetsModule} from '../main-widgets/main-widgets.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    MainWidgetsModule
  ],
  declarations: [
    AboutGameScreenComponent
  ]
})
export class AboutModule {
}
