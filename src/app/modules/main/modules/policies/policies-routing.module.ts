import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CookiesPolicyScreenComponent} from './components/cookies-policy-screen/cookies-policy-screen.component';

const routes: Routes = [
  {
    path: 'cookies-policy',
    component: CookiesPolicyScreenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PoliciesRoutingModule {
}
