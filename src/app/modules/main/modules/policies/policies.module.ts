import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {CookiesPolicyScreenComponent} from './components/cookies-policy-screen/cookies-policy-screen.component';
import {MainWidgetsModule} from '../main-widgets/main-widgets.module';
import {PoliciesRoutingModule} from './policies-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PoliciesRoutingModule,
    TranslateModule,
    MainWidgetsModule
  ],
  declarations: [
    CookiesPolicyScreenComponent
  ]
})
export class PoliciesModule {
}
