import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-cookies-policy-screen',
  templateUrl: './cookies-policy-screen.component.html',
  styleUrls: ['./cookies-policy-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CookiesPolicyScreenComponent {
}
