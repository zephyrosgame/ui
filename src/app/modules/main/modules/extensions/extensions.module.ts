import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ExtensionsScreenComponent} from './components/extensions-screen/extensions-screen.component';
import {ExtensionComponent} from './components/extension/extension.component';
import {ExtensionScreenComponent} from './components/extension-screen/extension-screen.component';
import {MainWidgetsModule} from '../main-widgets/main-widgets.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    MainWidgetsModule
  ],
  declarations: [
    ExtensionsScreenComponent,
    ExtensionComponent,
    ExtensionScreenComponent
  ]
})
export class ExtensionsModule {
}
