import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Extension} from '@api/models';

@Component({
  selector: 'zephyros-extension',
  templateUrl: './extension.component.html',
  styleUrls: ['./extension.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExtensionComponent {

  @Input() extension: Extension;

  activate(): void {
    // todo
  }

}
