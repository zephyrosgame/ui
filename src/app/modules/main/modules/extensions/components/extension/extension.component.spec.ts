import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ExtensionComponent} from './extension.component';
import {By} from '@angular/platform-browser';
import {Extension} from '@api/models';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {RouterTestingModule} from '@angular/router/testing';

const translations = {
  ['de']: {
    extensions: {
      activate: 'Aktivieren ({{price}})'
    }
  }
};

const extension: Extension = {
  id: 'mockid',
  title: 'Erweiterung',
  price: '$1.99',
  description: 'Lorem ipsum dolor sit amet',
  variation: 1
};

describe('ExtensionComponent', () => {
  let component: ExtensionComponent;
  let fixture: ComponentFixture<ExtensionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ExtensionComponent],
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations(translations)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ExtensionComponent);
    component = fixture.componentInstance;
    component.extension = extension;
    fixture.detectChanges();
  });

  it('should have an image', () => {
    const element = fixture.debugElement.query(By.css('.extension__image'));
    expect(element).toBeTruthy();
  });

  it('should display the correct title', () => {
    const element = fixture.debugElement.query(By.css('.extension__title'));
    expect(element.nativeElement.textContent).toBe('Erweiterung');
  });

  it('should display the correct description', () => {
    const element = fixture.debugElement.query(By.css('.extension__paragraph'));
    expect(element.nativeElement.textContent).toBe('Lorem ipsum dolor sit amet');
  });

  it('should display the correct price', () => {
    const element = fixture.debugElement.query(By.css('.extension__buttons button'));
    expect(element.nativeElement.textContent).toBe('Aktivieren ($1.99)');
  });

  it('should have a link to details page', () => {
    const element = fixture.debugElement.query(By.css('.extension__buttons a'));
    expect(element.nativeElement.href).toContain('/extensions/mockid');
  });
});
