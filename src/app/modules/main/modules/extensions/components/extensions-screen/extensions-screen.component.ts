import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Extension} from '@api/models';
import {extensionsListAnimation} from '../../animations';
import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';

@Component({
  selector: 'zephyros-extensions-screen',
  templateUrl: './extensions-screen.component.html',
  styleUrls: ['./extensions-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [extensionsListAnimation]
})
export class ExtensionsScreenComponent {

  readonly extensions$: Observable<Extension[]> = of([
    {
      id: 'mesapotamien',
      title: 'Zweistromland',
      price: '1,99 €',
      description: 'An jeder Stelle könnte ein Geheimnis versteckt sein, das dich dem Sieg ein Stück näher bringt. ' +
        'Die Gunst der Götter kann dir dabei ebenso helfen, wie das Wissen deiner Gelehrten und Philosophen, die durch ihre ' +
        'Forschungen stets neue Erkenntnisse erlangen.',
      variation: 1
    },
    {
      id: 'mesapotamien',
      title: 'Zweistromland',
      price: '1,99 €',
      description: 'An jeder Stelle könnte ein Geheimnis versteckt sein, das dich dem Sieg ein Stück näher bringt. ' +
        'Die Gunst der Götter kann dir dabei ebenso helfen, wie das Wissen deiner Gelehrten und Philosophen, die durch ihre ' +
        'Forschungen stets neue Erkenntnisse erlangen.',
      variation: 2
    },
    {
      id: 'mesapotamien',
      title: 'Zweistromland',
      price: '1,99 €',
      description: 'An jeder Stelle könnte ein Geheimnis versteckt sein, das dich dem Sieg ein Stück näher bringt. ' +
        'Die Gunst der Götter kann dir dabei ebenso helfen, wie das Wissen deiner Gelehrten und Philosophen, die durch ihre ' +
        'Forschungen stets neue Erkenntnisse erlangen.',
      variation: 3
    },
    {
      id: 'mesapotamien',
      title: 'Zweistromland',
      price: '1,99 €',
      description: 'An jeder Stelle könnte ein Geheimnis versteckt sein, das dich dem Sieg ein Stück näher bringt. ' +
        'Die Gunst der Götter kann dir dabei ebenso helfen, wie das Wissen deiner Gelehrten und Philosophen, die durch ihre ' +
        'Forschungen stets neue Erkenntnisse erlangen.',
      variation: 4
    },
    {
      id: 'mesapotamien',
      title: 'Zweistromland',
      price: '1,99 €',
      description: 'An jeder Stelle könnte ein Geheimnis versteckt sein, das dich dem Sieg ein Stück näher bringt. ' +
        'Die Gunst der Götter kann dir dabei ebenso helfen, wie das Wissen deiner Gelehrten und Philosophen, die durch ihre ' +
        'Forschungen stets neue Erkenntnisse erlangen.',
      variation: 5
    },
    {
      id: 'mesapotamien',
      title: 'Zweistromland',
      price: '1,99 €',
      description: 'An jeder Stelle könnte ein Geheimnis versteckt sein, das dich dem Sieg ein Stück näher bringt. ' +
        'Die Gunst der Götter kann dir dabei ebenso helfen, wie das Wissen deiner Gelehrten und Philosophen, die durch ihre ' +
        'Forschungen stets neue Erkenntnisse erlangen.',
      variation: 6
    }
  ]).pipe(delay(0));

}
