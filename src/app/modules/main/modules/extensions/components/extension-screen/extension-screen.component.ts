import {Component} from '@angular/core';
import {Extension} from '@api/models';

@Component({
  selector: 'zephyros-extension-screen',
  templateUrl: './extension-screen.component.html',
  styleUrls: ['./extension-screen.component.scss']
})
export class ExtensionScreenComponent {

  readonly extension: Extension = {
    id: 'mesapotamien',
    title: 'Zweistromland',
    price: '1,99 €',
    description: 'An jeder Stelle könnte ein Geheimnis versteckt sein, das dich dem Sieg ein Stück näher bringt. ' +
      'Die Gunst der Götter kann dir dabei ebenso helfen, wie das Wissen deiner Gelehrten und Philosophen, die durch ihre ' +
      'Forschungen stets neue Erkenntnisse erlangen.',
    variation: 1
  };

}
