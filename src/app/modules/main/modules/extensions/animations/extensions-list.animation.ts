import {animate, query, stagger, style, transition, trigger} from '@angular/animations';

export const extensionsListAnimation = trigger('extensionsListAnimation', [
  transition(':enter', [
    query('zephyros-extension', [
      style({opacity: 0, transform: 'translateY(20px)'}),
      stagger(100, [
        animate('300ms', style({opacity: 1, transform: 'translateY(0px)'}))
      ])
    ])
  ])
]);
