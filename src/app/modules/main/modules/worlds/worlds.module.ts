import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {WorldsScreenComponent} from './components/worlds-screen/worlds-screen.component';
import {WorldComponent} from './components/world/world.component';
import {WidgetsModule} from '../../../widgets/widgets.module';
import {FeaturedComponent} from './components/featured/featured.component';
import {SidebarBlogPreviewComponent} from './components/sidebar-blog-preview/sidebar-blog-preview.component';
import {MainWidgetsModule} from '../main-widgets/main-widgets.module';
import {HomepageComponent} from './components/homepage/homepage.component';
import {RouterModule} from '@angular/router';
import {WorldsListComponent} from './components/worlds-list/worlds-list.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    WidgetsModule,
    MainWidgetsModule
  ],
  declarations: [
    HomepageComponent,
    WorldsScreenComponent,
    WorldComponent,
    FeaturedComponent,
    SidebarBlogPreviewComponent,
    WorldsListComponent
  ],
  exports: [
    HomepageComponent,
    WorldsScreenComponent
  ]
})
export class WorldsModule {
}
