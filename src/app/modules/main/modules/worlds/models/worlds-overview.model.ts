import {World} from '@api/models';

export interface WorldsOverview {
  continueOn: World[];
  startOn: World[];
}
