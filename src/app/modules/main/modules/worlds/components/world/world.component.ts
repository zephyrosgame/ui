import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {World} from '@api/models';

@Component({
  selector: 'zephyros-world',
  templateUrl: './world.component.html',
  styleUrls: ['./world.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorldComponent {

  @Input() world: World;

}
