import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Player, World} from '@api/models';
import {first, map, mergeMap, switchMap, takeUntil, toArray} from 'rxjs/operators';
import {AuthState, selectAuth} from '../../../../../../store/auth';
import {Store} from '@ngrx/store';
import {AppState} from '../../../../../../store/app.state';
import {PlayersApiService, WorldsApiService} from '@api/services';

@Component({
  selector: 'zephyros-worlds-screen',
  templateUrl: './worlds-screen.component.html',
  styleUrls: ['./worlds-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorldsScreenComponent implements OnInit, OnDestroy {

  worldsToJoin$: Observable<World[]>;

  joinedWorlds$: Observable<World[]>;

  private readonly destroyed$: Subject<void> = new Subject();

  constructor(private worldsApiService: WorldsApiService,
              private playersApiService: PlayersApiService,
              private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.worldsToJoin$ = this.worldsApiService.getWorldsToJoin().pipe(
      takeUntil(this.destroyed$)
    );

    this.joinedWorlds$ = this.store.select(selectAuth).pipe(
      takeUntil(this.destroyed$),
      first((authState: AuthState) => !authState.pending && authState.user !== null),
      map((authState: AuthState) => authState.user!.id),
      switchMap((userId: string) => this.playersApiService.getPlayersOfUser(userId)),
      mergeMap((players: Player[]) => players),
      switchMap((player: Player) => this.worldsApiService.getWorldOfPlayer(player)),
      map((world: World) => world),
      toArray()
    );
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
