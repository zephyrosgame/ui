import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TimeoutError} from 'rxjs';
import {World} from '@api/models';
import {WorldsApiService} from '@api/services';
import {Icon} from '../../../../../widgets/components/icon-widget/icon.enum';

@Component({
  selector: 'zephyros-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomepageComponent implements OnInit {

  readonly Icon = Icon;

  worlds: World[];

  isLoading: boolean;

  isTimeout: boolean;

  constructor(private worldsApiService: WorldsApiService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.fetchWorlds();
  }

  fetchWorlds(): void {
    this.isLoading = true;
    this.isTimeout = false;

    this.worldsApiService.getWorldsToJoin().subscribe((worlds: World[]) => {
      this.worlds = worlds;
      this.isLoading = false;
      this.isTimeout = false;
      this.cdr.markForCheck();
    }, (error) => {
      if (error instanceof TimeoutError) {
        this.isLoading = false;
        this.isTimeout = true;
        this.cdr.markForCheck();
      } else {
        // todo: other potential errors should be handled somehow as well as logged and reported to a backend resource
      }
    });
  }

}
