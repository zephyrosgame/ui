import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {SidebarBlogPreviewComponent} from './sidebar-blog-preview.component';

describe('SidebarBlogPreviewComponent', () => {
  let component: SidebarBlogPreviewComponent;
  let fixture: ComponentFixture<SidebarBlogPreviewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarBlogPreviewComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(SidebarBlogPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have a title', () => {
    const elements = fixture.debugElement.queryAll(By.css('.preview__title'));
    expect(elements.length).toBe(1);
  });

  it('should have a body', () => {
    const elements = fixture.debugElement.queryAll(By.css('.preview__body'));
    expect(elements.length).toBe(1);
  });

  it('should display details', () => {
    const elements = fixture.debugElement.queryAll(By.css('.preview__details'));
    expect(elements.length).toBe(1);
  });
});
