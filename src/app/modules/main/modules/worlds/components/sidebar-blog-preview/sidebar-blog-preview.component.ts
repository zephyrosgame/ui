import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-sidebar-blog-preview',
  templateUrl: './sidebar-blog-preview.component.html',
  styleUrls: ['./sidebar-blog-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarBlogPreviewComponent {
}
