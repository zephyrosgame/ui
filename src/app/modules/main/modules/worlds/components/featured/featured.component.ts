import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FeaturedComponent {

  private readonly characters = ['priest', 'researcher', 'syvoulos'];

  readonly character = this.characters[Math.floor(Math.random() * this.characters.length)];

}
