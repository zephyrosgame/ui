import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {worldsListAnimation} from '../../animations';
import {World} from '@api/models';

@Component({
  selector: 'zephyros-worlds-list',
  templateUrl: './worlds-list.component.html',
  styleUrls: ['./worlds-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [worldsListAnimation]
})
export class WorldsListComponent {

  @Input() title: string;

  @Input() worlds: World[];

}
