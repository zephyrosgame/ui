import {animate, query, stagger, style, transition, trigger} from '@angular/animations';

export const worldsListAnimation = trigger('worldsListAnimation', [
  transition(':enter', [
    query('.worlds__item', [
      style({opacity: 0, transform: 'translateY(20px)'}),
      stagger(150, [
        animate('300ms', style({opacity: 1, transform: 'translateY(0px)'}))
      ])
    ])
  ])
]);
