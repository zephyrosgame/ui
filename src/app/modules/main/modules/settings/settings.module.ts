import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettingsScreenComponent} from './components/settings-screen/settings-screen.component';
import {SettingsRoutingModule} from './settings-routing.module';
import {MainWidgetsModule} from '../main-widgets/main-widgets.module';
import {AvatarUploadComponent} from './components/avatar-upload/avatar-upload.component';
import {WidgetsModule} from '../../../widgets/widgets.module';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    MainWidgetsModule,
    WidgetsModule
  ],
  declarations: [
    SettingsScreenComponent,
    AvatarUploadComponent
  ]
})
export class SettingsModule {
}
