import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SettingsScreenComponent} from './components/settings-screen/settings-screen.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsScreenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {
}
