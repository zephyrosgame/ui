import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, ViewChild} from '@angular/core';
import {UsersApiService} from '@api/services';
import {Store} from '@ngrx/store';
import {AppState} from '../../../../../../store/app.state';
import {GetUserAction} from '../../../../../../store/auth';
import {User} from '@api/models';

@Component({
  selector: 'zephyros-avatar-upload',
  templateUrl: './avatar-upload.component.html',
  styleUrls: ['./avatar-upload.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarUploadComponent {

  @Input() user: User | null;

  @ViewChild('imageInput', {static: true}) imageInput: ElementRef<HTMLInputElement>;

  previewSrc: string | null = null;

  uploadPending = false;

  private selectedFile: File;

  constructor(private store: Store<AppState>,
              private usersApiService: UsersApiService,
              private cdr: ChangeDetectorRef) {
  }

  processFile(): void {
    const files: FileList | null = this.imageInput.nativeElement.files;

    if (files !== null) {
      const file: File = files[0];

      const reader = new FileReader();
      reader.addEventListener('load', (event: any) => {
        this.previewSrc = event.target.result;
        this.selectedFile = file;
        this.cdr.markForCheck();

        this.uploadAvatar();
      });
      reader.readAsDataURL(file);
    }
  }

  private uploadAvatar(): void {
    this.uploadPending = true;

    this.usersApiService.uploadAvatar(this.selectedFile).subscribe(() => {
      this.previewSrc = null;
      this.uploadPending = false;
      this.store.dispatch(new GetUserAction());
      this.cdr.markForCheck();
    }, () => {
      this.previewSrc = null;
      this.uploadPending = false;
      this.cdr.markForCheck();
    });
  }

}
