import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '@api/models';
import {Store} from '@ngrx/store';
import {AppState} from '../../../../../../store/app.state';
import {AuthState, selectAuth} from '../../../../../../store/auth';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'zephyros-settings-screen',
  templateUrl: './settings-screen.component.html',
  styleUrls: ['./settings-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsScreenComponent implements OnInit {

  user$: Observable<User>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.user$ = this.store.select(selectAuth).pipe(
      filter((authState: AuthState) => !authState.pending),
      map((authState: AuthState) => authState.user!)
    );
  }

}
