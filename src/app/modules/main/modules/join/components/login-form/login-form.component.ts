import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {TranslationWithParams} from '../../../../models';
import {Store} from '@ngrx/store';
import {ApiError, LoginDto} from '@api/models';
import {LoginState, LoginSubmitAction, selectLogin} from '../../../../../../store/login';
import {Observable} from 'rxjs';
import {Icon} from '../../../../../widgets/components/icon-widget/icon.enum';
import {NGXLogger} from 'ngx-logger';
import {TranslateService} from '@ngx-translate/core';
import {AppState} from '../../../../../../store/app.state';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'zephyros-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginFormComponent implements OnInit {

  readonly Icon = Icon;

  readonly dto: LoginDto = new LoginDto('', '');

  state$: Observable<LoginState>;

  error: TranslationWithParams | null = null;

  constructor(private store: Store<AppState>,
              private logger: NGXLogger,
              private translate: TranslateService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.state$ = this.store.select(selectLogin);
  }

  getErrorMessage(error: ApiError): Observable<string> {
    switch (error.errorName) {
      case 'UserNotFound':
        return this.translate.get('login.error.username-not-found');
      case 'IncorrectPassword':
        return this.translate.get('login.error.password-incorrect');
      case 'ValidationFailed':
        return this.translate.get('login.error.validation-failed');
      default:
        return this.translate.get('login.error.unknown-error-occurred');
    }
  }

  onSubmit(): void {
    this.logger.trace('submitted login form');

    // todo: validate
    if (this.dto.username === undefined || this.dto.password === undefined) {
      return;
    }

    this.route.queryParams.subscribe((params: Params) => {
      this.store.dispatch(new LoginSubmitAction(this.dto, params.world));
    });
  }

}
