import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ApiError, RegisterDto} from '../../../../../../api';
import {Store} from '@ngrx/store';
import {RegisterState, RegisterSubmitAction, selectRegister} from '../../../../../../store/register';
import {Observable} from 'rxjs';
import {NGXLogger} from 'ngx-logger';
import {Icon} from '../../../../../widgets/components/icon-widget/icon.enum';
import {TranslateService} from '@ngx-translate/core';
import {AppState} from '../../../../../../store/app.state';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'zephyros-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterFormComponent implements OnInit {

  readonly Icon = Icon;

  readonly dto: RegisterDto = new RegisterDto('', '', '');

  state$: Observable<RegisterState>;

  constructor(private store: Store<AppState>,
              private logger: NGXLogger,
              private translate: TranslateService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.state$ = this.store.select(selectRegister);
  }

  getErrorMessage(error: ApiError): Observable<string> {
    switch (error.errorName) {
      case 'UsernameForbidden':
        return this.translate.get('register.error.username-not-allowed', {forbiddenUsername: error.data});
      case 'UsernameTaken':
        return this.translate.get('register.error.username-taken');
      case 'EmailTaken':
        return this.translate.get('register.error.email-taken');
      case 'ValidationFailed':
        return this.translate.get('register.error.validation-failed');
      default:
        return this.translate.get('register.error.unknown-error-occurred');
    }
  }

  onSubmit(): void {
    this.logger.trace('submitted register form');

    // todo: validate

    this.route.queryParams.subscribe((params: Params) => {
      this.store.dispatch(new RegisterSubmitAction(this.dto, params.world));
    });
  }

}
