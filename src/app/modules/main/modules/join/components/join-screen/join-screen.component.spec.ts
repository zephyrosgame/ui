import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {JoinScreenComponent} from './join-screen.component';

describe('JoinScreenComponent', () => {
  let component: JoinScreenComponent;
  let fixture: ComponentFixture<JoinScreenComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JoinScreenComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(JoinScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show the login form', () => {
    const element = fixture.debugElement.query(By.css('zephyros-login-form'));
    expect(element).toBeTruthy();
  });

  it('should show the register form', () => {
    const element = fixture.debugElement.query(By.css('zephyros-register-form'));
    expect(element).toBeTruthy();
  });
});
