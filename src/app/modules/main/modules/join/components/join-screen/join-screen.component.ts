import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-join-screen',
  templateUrl: './join-screen.component.html',
  styleUrls: ['./join-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JoinScreenComponent {
}
