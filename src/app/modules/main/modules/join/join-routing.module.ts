import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {JoinScreenComponent} from './components/join-screen/join-screen.component';

const routes: Routes = [
  {
    path: '',
    component: JoinScreenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JoinRoutingModule {
}
