import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {JoinScreenComponent} from './components/join-screen/join-screen.component';
import {RegisterFormComponent} from './components/register-form/register-form.component';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {MainWidgetsModule} from '../main-widgets/main-widgets.module';
import {JoinRoutingModule} from './join-routing.module';
import {WidgetsModule} from '../../../widgets/widgets.module';

@NgModule({
  imports: [
    CommonModule,
    JoinRoutingModule,
    FormsModule,
    TranslateModule,
    MainWidgetsModule,
    WidgetsModule
  ],
  declarations: [
    JoinScreenComponent,
    RegisterFormComponent,
    LoginFormComponent
  ]
})
export class JoinModule {
}
