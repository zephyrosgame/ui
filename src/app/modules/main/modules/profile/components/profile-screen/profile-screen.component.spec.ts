import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ProfileScreenComponent} from './profile-screen.component';
import {ProfileComponent} from '../profile/profile.component';
import {ProfileNotFoundComponent} from '../profile-not-found/profile-not-found.component';
import {MockComponents} from 'ng-mocks';
import {RouterTestingModule} from '@angular/router/testing';
import {LoggerTestingModule} from 'ngx-logger/testing';
import {UsersApiService} from '@api/services';
import {ActivatedRoute} from '@angular/router';
import {of, ReplaySubject, Subject} from 'rxjs';
import {ApiError, ApiErrorResponse, UserProfile} from '@api/models';
import {MainFullPageMockComponent} from '../../../main-widgets/components/main-full-page/main-full-page.component.mock';
import {By} from '@angular/platform-browser';

const profile: UserProfile = {
  id: 'someUserId',
  username: 'Mockuser',
  avatar: 'avatar.png'
};

const userProfile$: Subject<UserProfile> = new ReplaySubject();

const usersApiServiceMock = {
  getUserProfile: (id) => {}
} as UsersApiService;

describe('ProfileScreenComponent', () => {
  let component: ProfileScreenComponent;
  let fixture: ComponentFixture<ProfileScreenComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        LoggerTestingModule
      ],
      declarations: [
        MockComponents(ProfileComponent, ProfileNotFoundComponent),
        MainFullPageMockComponent,
        ProfileScreenComponent
      ],
      providers: [
        {
          provide: UsersApiService,
          useValue: usersApiServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({userId: 'someUserId'})
          }
        }
      ]
    }).compileComponents();

    spyOn(usersApiServiceMock, 'getUserProfile').and.returnValue(userProfile$.asObservable());

    fixture = TestBed.createComponent(ProfileScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show the profile while isLoading', () => {
    const element = fixture.debugElement.query(By.css('zephyros-profile'));

    expect(element).toBeTruthy();
  });

  describe('if backend responds', () => {
    beforeEach(() => {
      userProfile$.next(profile);
      fixture.detectChanges();
    });

    it('should request the profile of the user with the given id', () => {
      expect(usersApiServiceMock.getUserProfile).toHaveBeenCalledWith('someUserId');
    });

    it('should request the profile once', () => {
      expect(usersApiServiceMock.getUserProfile).toHaveBeenCalledTimes(1);
    });

    it('should display the correct profile', () => {
      const profileComponent = fixture.debugElement.query(By.css('zephyros-profile')).componentInstance;

      expect(profileComponent.profile).toBe(profile);
    });

    it('should not show that the profile was not found', () => {
      const element = fixture.debugElement.query(By.css('zephyros-profile-not-found'));

      expect(element).toBeFalsy();
    });
  });

  describe('if backend error', () => {
    beforeEach(() => {
      const apiErrorResponse: ApiErrorResponse = {
        error: {} as ApiError,
        status: 404
      };

      userProfile$.error(apiErrorResponse);
      fixture.detectChanges();
    });

    it('should show that the profile was not found', () => {
      const element = fixture.debugElement.query(By.css('zephyros-profile-not-found'));

      expect(element).toBeTruthy();
    });

    it('should not show the profile', () => {
      const element = fixture.debugElement.query(By.css('zephyros-profile'));

      expect(element).toBeFalsy();
    });
  });
});
