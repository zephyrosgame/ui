import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {first, map, switchMap, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {UsersApiService} from '@api/services';
import {ApiErrorResponse, UserProfile} from '@api/models';

@Component({
  selector: 'zephyros-profile-screen',
  templateUrl: './profile-screen.component.html',
  styleUrls: ['./profile-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileScreenComponent implements OnInit {

  profile: UserProfile | null = null;

  errorOccurred = false;

  constructor(private route: ActivatedRoute,
              private usersApiService: UsersApiService,
              private logger: NGXLogger,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.route.params.pipe(
      first(),
      map(data => data['userId']),
      tap((userId: string) => this.logger.trace(`Requesting profile of user with id '${userId}'`)),
      switchMap((userId: string) => this.usersApiService.getUserProfile(userId))
    ).subscribe((profile: UserProfile) => {
      this.profile = profile;
      this.cdr.markForCheck();
    }, (res: ApiErrorResponse) => {
      this.logger.warn('Error while requesting profile', res.error);
      this.errorOccurred = true;
      this.cdr.markForCheck();
    });
  }

}
