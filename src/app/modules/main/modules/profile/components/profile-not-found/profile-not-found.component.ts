import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'zephyros-profile-not-found',
  templateUrl: './profile-not-found.component.html',
  styleUrls: ['./profile-not-found.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileNotFoundComponent {
}
