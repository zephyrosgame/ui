import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {UserProfile} from '@api/models';

@Component({
  selector: 'zephyros-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent {

  @Input() profile: UserProfile | null;

}
