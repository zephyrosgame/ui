import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {ProfileScreenComponent} from './components/profile-screen/profile-screen.component';
import {ProfileNotFoundComponent} from './components/profile-not-found/profile-not-found.component';
import {ProfileComponent} from './components/profile/profile.component';
import {MainWidgetsModule} from '../main-widgets/main-widgets.module';
import {ProfileRoutingModule} from './profile-routing.module';
import {WidgetsModule} from '../../../widgets/widgets.module';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    TranslateModule,
    MainWidgetsModule,
    WidgetsModule
  ],
  declarations: [
    ProfileScreenComponent,
    ProfileNotFoundComponent,
    ProfileComponent
  ]
})
export class ProfileModule {
}
