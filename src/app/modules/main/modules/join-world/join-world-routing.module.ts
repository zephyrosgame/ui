import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {JoinWorldScreenComponent} from './components/join-world-screen/join-world-screen.component';

const routes: Routes = [
  {
    path: '',
    component: JoinWorldScreenComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JoinWorldRoutingModule {
}
