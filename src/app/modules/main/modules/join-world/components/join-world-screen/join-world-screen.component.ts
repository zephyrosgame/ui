import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {RoutingService} from '../../../../../../services';
import {ActivatedRoute} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {catchError, finalize, first, map, takeUntil} from 'rxjs/operators';
import {EMPTY, Subject} from 'rxjs';
import {Icon} from '../../../../../widgets/components/icon-widget/icon.enum';
import {PlayersApiService} from '@api/services';
import {ApiErrorResponse, World} from '@api/models';

@Component({
  selector: 'zephyros-join-world-screen',
  templateUrl: './join-world-screen.component.html',
  styleUrls: ['./join-world-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JoinWorldScreenComponent implements OnInit, OnDestroy {

  readonly Icon = Icon;

  world: World;

  buttonDisabled = true;

  waitingForResponse = true;

  success = false;

  private readonly destroyed$: Subject<void> = new Subject();

  constructor(private logger: NGXLogger,
              private activatedRoute: ActivatedRoute,
              private route: RoutingService,
              private playersApiService: PlayersApiService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.pipe(
      takeUntil(this.destroyed$),
      map(data => data['world'])
    ).subscribe((world: World) => {
      this.logger.debug('Showing join world screen for world', world);
      this.world = world;
      this.buttonDisabled = false;
      this.waitingForResponse = false;
      this.cdr.markForCheck();
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  join(): void {
    this.logger.debug('Trying to join world', this.world);
    this.buttonDisabled = true;
    this.waitingForResponse = true;

    if (!this.world) {
      this.logger.error('Trying to join world but world has not been fetched');
    }

    this.playersApiService.createPlayer({worldId: this.world.id}).pipe(
      first(),
      catchError((res: ApiErrorResponse) => {
        switch (res.error.errorName) {
          case 'PlayerExists':
            this.userAlreadyHasPlayerOnWorld();
            break;
          case 'MaxPlayersReached':
            this.worldIsFull();
            break;
        }
        return EMPTY;
      }),
      finalize(() => {
        this.waitingForResponse = false;
        this.cdr.markForCheck();
      })
    ).subscribe(() => {
      this.logger.info('player created on world', this.world);

      this.success = true;
      this.cdr.markForCheck();

      this.route.toWorld(this.world.id);
    });
  }

  private userAlreadyHasPlayerOnWorld(): void {
    this.logger.info('user already has a player on the world');
    this.route.toWorld(this.world.id);
  }

  private worldIsFull(): void {
    this.logger.debug(`can't join world because it is full`);
    // todo
  }

}
