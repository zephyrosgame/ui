import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {JoinWorldScreenComponent} from './components/join-world-screen/join-world-screen.component';
import {MainWidgetsModule} from '../main-widgets/main-widgets.module';
import {JoinWorldRoutingModule} from './join-world-routing.module';
import {WidgetsModule} from '../../../widgets/widgets.module';

@NgModule({
  imports: [
    CommonModule,
    JoinWorldRoutingModule,
    TranslateModule,
    WidgetsModule,
    MainWidgetsModule
  ],
  declarations: [
    JoinWorldScreenComponent
  ]
})
export class JoinWorldModule {
}
