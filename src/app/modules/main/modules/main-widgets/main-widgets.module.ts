import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {MainSplitPageComponent} from './components/main-split-page/main-split-page.component';
import {MainFooterComponent} from './components/main-footer/main-footer.component';
import {MainFullPageComponent} from './components/main-full-page/main-full-page.component';
import {WidgetsModule} from '../../../widgets/widgets.module';
import {MainJumbotronComponent} from './components/main-jumbotron/main-jumbotron.component';
import {MainTimeoutErrorComponent} from './components/main-timeout-error/main-timeout-error.component';
import {MainSidebarChallengesComponent} from './components/main-sidebar-challenges/main-sidebar-challenges.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    WidgetsModule
  ],
  declarations: [
    MainSplitPageComponent,
    MainFooterComponent,
    MainFullPageComponent,
    MainJumbotronComponent,
    MainTimeoutErrorComponent,
    MainSidebarChallengesComponent
  ],
  exports: [
    MainSplitPageComponent,
    MainFooterComponent,
    MainFullPageComponent,
    MainJumbotronComponent,
    MainTimeoutErrorComponent,
    MainSidebarChallengesComponent
  ]
})
export class MainWidgetsModule {
}
