import {ChangeDetectionStrategy, Component} from '@angular/core';
import {footerMenuItems} from '../../../../models';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'zephyros-main-split-page',
  templateUrl: './main-split-page.component.html',
  styleUrls: ['./main-split-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainSplitPageComponent {

  readonly footerMenuItems = footerMenuItems;

  page: string;

  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.data.pipe(
      map(data => data['page'])
    ).subscribe(page => this.page = page);
  }

}
