import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {MainFooterMenuItem} from '../../../../models';

@Component({
  selector: 'zephyros-main-footer',
  templateUrl: './main-footer.component.html',
  styleUrls: ['./main-footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainFooterComponent {

  @Input() menuItems: MainFooterMenuItem[];

}
