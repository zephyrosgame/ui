import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {MainFooterComponent} from './main-footer.component';

const translations = {
  ['de']: {
    legal: {
      'imprint.nav-label': 'Impressum',
      'privacy-policy.nav-label': 'Datenschutzerklärung'
    }
  }
};

describe('MainFooterComponent', () => {
  let component: MainFooterComponent;
  let fixture: ComponentFixture<MainFooterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MainFooterComponent],
      imports: [
        RouterTestingModule,
        TranslateTestingModule.withTranslations(translations)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MainFooterComponent);
    component = fixture.componentInstance;
  });

  it('should correctly display a single menu item', () => {
    component.menuItems = [
      {translationKey: 'legal.imprint.nav-label', routerLink: '/imprint'},
    ];
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.menu-item'));

    expect(elements.length).toBe(1);

    expect(elements[0].nativeElement.textContent).toBe('Impressum');
    expect(elements[0].nativeElement.href).toContain('/imprint');
  });

  it('should correctly display multiple menu items', () => {
    component.menuItems = [
      {translationKey: 'legal.imprint.nav-label', routerLink: '/imprint'},
      {translationKey: 'legal.privacy-policy.nav-label', routerLink: '/privacy-policy'}
    ];
    fixture.detectChanges();

    const elements = fixture.debugElement.queryAll(By.css('.menu-item'));

    expect(elements.length).toBe(2);

    expect(elements[0].nativeElement.textContent).toBe('Impressum');
    expect(elements[0].nativeElement.href).toContain('/imprint');

    expect(elements[1].nativeElement.textContent).toBe('Datenschutzerklärung');
    expect(elements[1].nativeElement.href).toContain('/privacy-policy');
  });
});
