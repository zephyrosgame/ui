import {Component} from '@angular/core';

@Component({
  selector: 'zephyros-main-full-page',
  template: `<ng-content></ng-content>`
})
export class MainFullPageMockComponent {
}
