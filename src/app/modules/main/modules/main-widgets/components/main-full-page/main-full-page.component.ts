import {ChangeDetectionStrategy, Component} from '@angular/core';
import {footerMenuItems} from '../../../../models';
import {ActivatedRoute} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'zephyros-main-full-page',
  templateUrl: './main-full-page.component.html',
  styleUrls: ['./main-full-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainFullPageComponent {

  readonly footerMenuItems = footerMenuItems;

  page: string;

  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.data.pipe(
      map(data => data['page'])
    ).subscribe(page => this.page = page);
  }

}
