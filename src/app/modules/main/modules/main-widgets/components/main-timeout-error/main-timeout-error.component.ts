import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'zephyros-main-timeout-error',
  templateUrl: './main-timeout-error.component.html',
  styleUrls: ['./main-timeout-error.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainTimeoutErrorComponent {

  @Output() retry: EventEmitter<void> = new EventEmitter();

}
