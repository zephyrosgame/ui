import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'zephyros-main-jumbotron',
  templateUrl: './main-jumbotron.component.html',
  styleUrls: ['./main-jumbotron.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainJumbotronComponent {

  @Input() title: string;

  @Input() body: string;

}
