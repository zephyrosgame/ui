import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Icon} from '../../../../../widgets/components/icon-widget/icon.enum';

@Component({
  selector: 'zephyros-main-sidebar-challenges',
  templateUrl: './main-sidebar-challenges.component.html',
  styleUrls: ['./main-sidebar-challenges.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainSidebarChallengesComponent {

  readonly Icon = Icon;

}
