import {MainHeaderMenuItem} from './main-header-menu-item.model';

export const guestHeaderMenuItems: MainHeaderMenuItem[] = [
  new MainHeaderMenuItem('worlds.nav-label', '/'),
  new MainHeaderMenuItem('extensions.nav-label', '/extensions'),
  new MainHeaderMenuItem('about.nav-label', '/about'),
  new MainHeaderMenuItem('blog.nav-label', '/blog')
];

export const userHeaderMenuItems: MainHeaderMenuItem[] = [
  new MainHeaderMenuItem('worlds.nav-label', '/worlds'),
  new MainHeaderMenuItem('extensions.nav-label', '/extensions'),
  new MainHeaderMenuItem('about.nav-label', '/about'),
  new MainHeaderMenuItem('blog.nav-label', '/blog')
];
