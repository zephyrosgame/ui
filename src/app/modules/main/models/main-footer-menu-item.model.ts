export class MainFooterMenuItem {

  constructor(public readonly translationKey: string,
              public readonly routerLink: string) {
  }

}
