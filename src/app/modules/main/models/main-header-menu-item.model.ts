export class MainHeaderMenuItem {

  constructor(public readonly translationKey: string,
              public readonly routerLink: string) {
  }

}
