export interface TranslationWithParams {
  translationKey: string;
  params?: Object;
}
