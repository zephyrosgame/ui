import {MainFooterMenuItem} from './main-footer-menu-item.model';

export const footerMenuItems: MainFooterMenuItem[] = [
  new MainFooterMenuItem('legal.privacy-policy.nav-label', '/legal/privacy-policy'),
  new MainFooterMenuItem('legal.terms-of-use.nav-label', '/legal/terms-of-use'),
  new MainFooterMenuItem('legal.imprint.nav-label', '/legal/imprint'),
];
