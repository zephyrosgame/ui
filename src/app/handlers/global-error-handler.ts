import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {DialogManagementService} from '../modules/dialog/services/dialog-management.service';
import {DialogRouteService} from '../modules/dialog/services/dialog-route.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector) {
  }

  async handleError(error: any) {
    const dialogManager = this.injector.get<DialogManagementService>(DialogManagementService);
    const dialogRouteService = this.injector.get<DialogRouteService>(DialogRouteService);
    const route = dialogRouteService.getRouteById('exception'); // todo: duplicate code
    if (route) {
      dialogManager.requestDialog(route);
    }

    // const logger = this.injector.get<NGXLogger>(NGXLogger);
    // const trace = await StackTrace.fromError(error).then((stackframes: StackTrace.StackFrame[]) => stackframes.splice(0, 20).join('\n'));
    // logger.error(trace); // todo: send stack trace to bl

    throw error;
  }

}
