export enum StorageKeys {
  TokenType = 'tokenType',
  AccessToken = 'accessToken',
  CookiesAccepted = 'cookiesAccepted'
}
