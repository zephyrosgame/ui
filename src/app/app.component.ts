import {ChangeDetectionStrategy, Component} from '@angular/core';
import {NavigationEnd, Router, RouterOutlet} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {filter} from 'rxjs/operators';
import {DialogManagementService} from './modules/dialog/services/dialog-management.service';
import {App} from './models';
import {CookiesNotificationService} from './services/cookies-notification.service';
import {appRoutingAnimation} from './animations/app-routing.animation';
import {bottomShadowAnimation} from './animations/bottom-shadow.animation';
import {cookiesNotificationAnimation} from './animations/cookies-notification.animation';
import {environment} from '../environments/environment';
import {Store} from '@ngrx/store';
import {GetUserAction} from './store/auth';
import {selectApp} from './store/app/app.selectors';
import {AppState} from './store/app.state';
import {appLoadingAnimation} from './animations/app-loading.animation';

@Component({
  selector: 'zephyros-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.components.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    appRoutingAnimation,
    appLoadingAnimation,
    bottomShadowAnimation,
    cookiesNotificationAnimation
  ]
})
export class AppComponent {

  readonly enableAnimations: boolean;

  readonly selectionDisabled$: Observable<boolean>;

  readonly activeApp$: Observable<App>;

  isAppLoading = true;

  showCookiesNotification: boolean;

  constructor(private translateService: TranslateService,
              private router: Router,
              private dialogManager: DialogManagementService,
              private cookiesNotificationService: CookiesNotificationService,
              private store: Store<AppState>) {
    this.translateService.addLangs(['de']);
    this.translateService.setDefaultLang('de');

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      window.scrollTo(0, 0);
      this.isAppLoading = false;
    });

    this.store.dispatch(new GetUserAction());

    this.enableAnimations = environment.enableAnimations;

    this.selectionDisabled$ = this.dialogManager.isDragging$;

    this.activeApp$ = this.store.select(selectApp);

    this.showCookiesNotification = !this.cookiesNotificationService.hasBeenAccepted;
  }

  onAcceptCookiesNotification() {
    this.cookiesNotificationService.accept();
    this.showCookiesNotification = false;
  }

  getRouteApp(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['app'];
  }

}
