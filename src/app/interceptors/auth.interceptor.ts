import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {NGXLogger} from 'ngx-logger';
import {StorageService} from '../services';
import {StorageKeys} from '../models/enum/storage-keys.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

  constructor(private logger: NGXLogger,
              private storageService: StorageService,
              @Inject(PLATFORM_ID) private platformId: Object) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.logger.trace('intercepting http request');

    const tokenType = this.storageService.getItem(StorageKeys.TokenType);
    const accessToken = this.storageService.getItem(StorageKeys.AccessToken);

    if (isPlatformBrowser(this.platformId) && tokenType !== null && accessToken !== null) {
      this.logger.trace(`using token type '${tokenType}' and access token '${accessToken}' for authorization`);

      return next.handle(req.clone({
        headers: req.headers.set('Authorization', `${tokenType} ${accessToken}`)
      }));
    }

    this.logger.trace('no authorization header added');

    return next.handle(req);
  }

}
