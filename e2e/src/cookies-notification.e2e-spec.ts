import {CookiesNotificationPo} from './cookies-notification.po';
import {browser} from 'protractor';

describe('CookiesNotification', () => {
  let page: CookiesNotificationPo;

  beforeEach(() => {
    page = new CookiesNotificationPo();
    page.navigateTo();
  });

  it('should be displayed by default', () => {
    page.getCookiesNotifications().then(elements => {
      expect(elements.length).toBe(1);
    });
  });

  it('should display title', () => {
    expect(page.getTitleText()).toEqual('Zephyros verwendet Cookies');
  });

  it('should display paragraph', () => {
    expect(page.getParagraphText()).toEqual('Wir verwenden Cookies, um Inhalte zu personalisieren und die Zugriffe auf unsere ' +
      'Webseite zu analysieren. Sie akzeptieren unsere Cookies, wenn Sie fortfahren diese Webseite zu nutzen.');
  });

  it('should route to policies page if policy link is clicked', () => {
    expect(browser.getCurrentUrl()).not.toEqual(`${browser.baseUrl}policies/cookies-policy`);
    page.getPolicyLink().click();
    expect(browser.getCurrentUrl()).toEqual(`${browser.baseUrl}policies/cookies-policy`);
  });

  it('should hide notification if accepted', () => {
    page.getAcceptButton().click();

    page.getCookiesNotifications().then(elements => {
      expect(elements.length).toBe(0);
    });
  });
});
