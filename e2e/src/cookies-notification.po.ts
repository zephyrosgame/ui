import {browser, by, element} from 'protractor';

export class CookiesNotificationPo {
  navigateTo() {
    return browser.get('/');
  }

  getCookiesNotifications() {
    return element.all(by.css('zephyros-cookies-notification'));
  }

  getTitleText() {
    return element(by.css('.cookies-notification__title')).getText();
  }

  getParagraphText() {
    return element(by.css('.cookies-notification__paragraph')).getText();
  }

  getPolicyLink() {
    return element(by.cssContainingText('zephyros-cookies-notification a', 'Cookie-Richtlinien'));
  }

  getAcceptButton() {
    return element(by.cssContainingText('zephyros-cookies-notification button', 'Akzeptieren'));
  }
}
